# Changelog

## v.1.0.1
fix bugs

## v.1.0.0

ohos移植首次版本

### support feature
- koin-core
- koin-core-ext

### not support feature
- ktor
- koin-androidx
- koin-android-compat
- koin-androidx-workmanager
- android-compose