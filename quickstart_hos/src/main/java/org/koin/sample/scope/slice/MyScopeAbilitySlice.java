package org.koin.sample.scope.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Text;
import org.koin.hos.ext.hos.ContextExt;
import org.koin.sample.ResourceTable;
import org.koin.sample.scope.MyScopePresenter;

public class MyScopeAbilitySlice extends AbilitySlice {

    private MyScopePresenter presenter;
    private Text text;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_my_scope);

        presenter = ContextExt.inject(getAbility(), MyScopePresenter.class);

        System.out.println("--- hello presenter : " + presenter);

        text = (Text) findComponentById(ResourceTable.Id_text_helloworld);
        text.setText(presenter.sayHello());
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
