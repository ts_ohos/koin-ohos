package org.koin.sample.scope;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import org.koin.core.scope.Scope;
import org.koin.hos.ext.hos.AbilityExt;
import org.koin.hos.scope.HosScopeComponent;
import org.koin.sample.scope.slice.MyScopeAbilitySlice;

public class MyScopeAbility extends Ability implements HosScopeComponent {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MyScopeAbilitySlice.class.getName());
    }

    @Override
    public Scope getScope() {
        return AbilityExt.abilityScope(this);
    }
}
