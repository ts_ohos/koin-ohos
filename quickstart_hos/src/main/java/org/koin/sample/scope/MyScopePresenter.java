package org.koin.sample.scope;

import org.koin.sample.HelloRepository;

public class MyScopePresenter {

    private HelloRepository repo;

    public MyScopePresenter(HelloRepository repo) {
        this.repo = repo;
    }

    public String sayHello() {
        return repo.giveHello() + " from MyScopePresenter";
    }
}
