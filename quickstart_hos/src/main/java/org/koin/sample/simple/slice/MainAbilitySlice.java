package org.koin.sample.simple.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import org.koin.java.KoinJavaComponent;
import org.koin.sample.ResourceTable;
import org.koin.sample.simple.MySimplePresenter;

public class MainAbilitySlice extends AbilitySlice {

    private MySimplePresenter firstPresenter;
    private Text text;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        text = (Text) findComponentById(ResourceTable.Id_text_helloworld);

        firstPresenter = KoinJavaComponent.inject(MySimplePresenter.class);
        text.setText(firstPresenter.sayHello());

        findComponentById(ResourceTable.Id_background)
                .setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent intent = new Intent();

                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName(getBundleName())
                        .withAbilityName("org.koin.sample.java.JavaAbility")
                        .build();

                intent.setOperation(operation);
                startAbility(intent);
            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
