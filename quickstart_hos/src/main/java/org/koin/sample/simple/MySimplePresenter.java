package org.koin.sample.simple;

import org.koin.sample.HelloRepository;

public class MySimplePresenter {

    private HelloRepository repo;

    public MySimplePresenter(HelloRepository repo) {
        this.repo = repo;
    }

    public String sayHello() {
        return repo.giveHello() + " from MySimplePresenter";
    }
}
