package org.koin.sample.simple;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import org.koin.sample.simple.slice.MainAbilitySlice;

public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());
    }
}
