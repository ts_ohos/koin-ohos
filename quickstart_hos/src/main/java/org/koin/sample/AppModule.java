package org.koin.sample;

import org.koin.core.definition.Definition;
import org.koin.core.error.DefinitionOverrideException;
import org.koin.core.module.Module;
import org.koin.core.parameter.DefinitionParameters;
import org.koin.core.scope.Scope;
import org.koin.dsl.ModuleDeclaration;
import org.koin.dsl.ScopeDSL;
import org.koin.sample.java.MyJavaPresenter;
import org.koin.sample.scope.MyScopeAbility;
import org.koin.sample.scope.MyScopePresenter;
import org.koin.sample.simple.MySimplePresenter;

public class AppModule {

    public static final Module appModule = org.koin.dsl.Module.module(new ModuleDeclaration() {
        @Override
        public void invoke(Module module) {
            try {
                module.single(HelloRepositoryImpl.class, new Definition<HelloRepositoryImpl>() {
                    @Override
                    public HelloRepositoryImpl invoke(Scope scope,
                                                      DefinitionParameters parameters) {
                        return new HelloRepositoryImpl();
                    }
                });

                module.factory(MySimplePresenter.class, new Definition<MySimplePresenter>() {
                    @Override
                    public MySimplePresenter invoke(Scope scope, DefinitionParameters parameters) {
                        try {
                            return new MySimplePresenter(scope.get(HelloRepositoryImpl.class));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return null;
                    }
                });

                module.factory(MyJavaPresenter.class, new Definition<MyJavaPresenter>() {
                    @Override
                    public MyJavaPresenter invoke(Scope scope, DefinitionParameters parameters) {
                        try {
                            return new MyJavaPresenter(scope.get(HelloRepositoryImpl.class));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return null;
                    }
                });

                module.scope(MyScopeAbility.class, new Module.ScopeSet() {
                    @Override
                    public void invoke(ScopeDSL scopeDSL) {
                        try {
                            scopeDSL.scoped(MyScopePresenter.class,
                                    new Definition<MyScopePresenter>() {
                                        @Override
                                        public MyScopePresenter invoke(Scope scope,
                                           DefinitionParameters parameters) {
                                            try {
                                                return new MyScopePresenter(scope
                                                        .get(HelloRepositoryImpl.class));
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            return null;
                                        }
                                    });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });

            } catch (DefinitionOverrideException e) {
                e.printStackTrace();
            }
        }
    });
}
