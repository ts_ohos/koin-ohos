package org.koin.sample;

public class HelloRepositoryImpl implements HelloRepository {

    @Override
    public String giveHello() {
        return "Hello Koin";
    }
}
