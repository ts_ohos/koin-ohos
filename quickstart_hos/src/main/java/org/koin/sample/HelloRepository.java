package org.koin.sample;

public interface HelloRepository {
    String giveHello();
}
