package org.koin.sample.java;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import org.koin.sample.java.slice.JavaAbilitySlice;

public class JavaAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(JavaAbilitySlice.class.getName());
    }
}
