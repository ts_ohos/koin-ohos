package org.koin.sample.java.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import org.koin.java.KoinJavaComponent;
import org.koin.sample.ResourceTable;
import org.koin.sample.java.MyJavaPresenter;
import org.koin.sample.simple.MySimplePresenter;

public class JavaAbilitySlice extends AbilitySlice {

    private MySimplePresenter presenter;
    private MyJavaPresenter javaPresenter;
    private Text text1;
    private Text text2;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_java);

        initInfo();
    }

    private void initInfo() {
        presenter = KoinJavaComponent.inject(MySimplePresenter.class);
        javaPresenter = KoinJavaComponent.inject(MyJavaPresenter.class);

        text1 = (Text) findComponentById(ResourceTable.Id_text_helloworld1);
        text2 = (Text) findComponentById(ResourceTable.Id_text_helloworld2);
        text1.setText(presenter.sayHello());
        text2.setText(javaPresenter.sayHello());

        findComponentById(ResourceTable.Id_background)
                .setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent intent = new Intent();

                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName(getBundleName())
                        .withAbilityName("org.koin.sample.scope.MyScopeAbility")
                        .build();

                intent.setOperation(operation);
                startAbility(intent);
            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
