package org.koin.sample;

import ohos.aafwk.ability.AbilityPackage;
import org.koin.core.KoinApplication;
import org.koin.core.context.DefaultContextExt;
import org.koin.core.logger.Level;
import org.koin.hos.java.KoinHosApplication;

public class MyApplication extends AbilityPackage {

    @Override
    public void onInitialize() {
        super.onInitialize();

        try {

            KoinApplication koinApplication = KoinHosApplication
                    .create(this, Level.INFO)
                    .modules(AppModule.appModule);
            DefaultContextExt.startKoin(koinApplication);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
