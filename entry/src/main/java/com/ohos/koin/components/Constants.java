package com.ohos.koin.components;

public class Constants {

    public static final String APP_TITLE = "Hos Samples Demo";
    public static final String ID = "42";
    public static final String SCOPE_ID = "SCOPE_ID";
    public static final String SCOPE_SESSION = "SCOPE_SESSION";
    public static final String SESSION_1 = "session1";
    public static final String SESSION_2 = "session2";

    public static int released = 0;
}
