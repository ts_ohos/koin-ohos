package com.example.hos_samples.mvp;

import com.example.hos_samples.mvp.slice.MVPAbilitySlice;
import ohos.aafwk.content.Intent;
import org.koin.hos.scope.ScopeAbility;

public class MVPAbility extends ScopeAbility {

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MVPAbilitySlice.class.getName());
    }
}
