package com.example.hos_samples.mvp.slice;

import com.example.hos_samples.ResourceTable;
import com.example.hos_samples.components.Constants;
import com.example.hos_samples.components.mvp.FactoryPresenter;
import com.example.hos_samples.components.mvp.ScopedPresenter;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import org.junit.Assert;
import org.koin.core.error.DefinitionParameterException;
import org.koin.core.parameter.DefinitionParameters;
import org.koin.core.parameter.ParametersDefinition;
import org.koin.hos.ext.hos.ContextExt;

public class MVPAbilitySlice extends AbilitySlice {

    // Inject presenter as Factory
    private FactoryPresenter factoryPresenter;

    // Inject presenter from MVPActivity's scope
    private ScopedPresenter scopedPresenter;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);

        initInfo();

        super.setUIContent(ResourceTable.Layout_ability_mvp);

        setEvent();
    }

    private void setEvent() {

        findComponentById(ResourceTable.Id_mvp_button)
                .setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent intent = new Intent();

                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName(getBundleName())
                        .withAbilityName("com.example.hos_samples.scoped.ScopedAbilityA")
                        .build();

                intent.setOperation(operation);
                startAbility(intent);
            }
        });

    }

    private void initInfo() {

        factoryPresenter = ContextExt.inject(this,
                FactoryPresenter.class,
                new ParametersDefinition() {
            @Override
            public DefinitionParameters invoke() {
                try {
                    return DefinitionParameters.parametersOf(Constants.ID);
                } catch (DefinitionParameterException e) {
                    e.printStackTrace();
                }
                return null;
            }
        });

        scopedPresenter = ContextExt.inject(getAbility(),
                ScopedPresenter.class,
                new ParametersDefinition() {
            @Override
            public DefinitionParameters invoke() {
                try {
                    return DefinitionParameters.parametersOf(Constants.ID);
                } catch (DefinitionParameterException e) {
                    e.printStackTrace();
                }
                return null;
            }
        });

        Assert.assertEquals(factoryPresenter.getId(), scopedPresenter.getId());
        Assert.assertEquals(factoryPresenter.getService(), scopedPresenter.getService());

        FactoryPresenter factoryPresenter1 = ContextExt.<FactoryPresenter>get(this,
                FactoryPresenter.class,
                new ParametersDefinition() {
            @Override
            public DefinitionParameters invoke() {
                try {
                    return DefinitionParameters.parametersOf(Constants.ID);
                } catch (DefinitionParameterException e) {
                    e.printStackTrace();
                }
                return null;
            }
        });

        Assert.assertNotEquals(factoryPresenter1, factoryPresenter);

        ScopedPresenter scopedPresenter1 = ContextExt.<ScopedPresenter>get(getAbility(),
                ScopedPresenter.class);

        Assert.assertEquals(scopedPresenter1, scopedPresenter);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
