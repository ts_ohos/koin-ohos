package com.example.hos_samples.scoped.slice;

import com.example.hos_samples.ResourceTable;
import com.example.hos_samples.components.Constants;
import com.example.hos_samples.components.scope.Session;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import org.koin.core.qualifier.Qualifier;
import org.koin.hos.ext.hos.ContextExt;

import static org.junit.Assert.assertEquals;

public class ScopedAbilityBSlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_scoped_b);

        initInfo();

        findComponentById(ResourceTable.Id_ability_b_button)
                .setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent intent = new Intent();

                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName(getBundleName())
                        .withAbilityName("com.example.hos_samples.dynamic.DynamicAbility")
                        .build();

                intent.setOperation(operation);
                startAbility(intent);
            }
        });
    }

    private void initInfo() {
        Session session = null;
        try {
            session = ContextExt.getKoin(this)
                    .getScope(Constants.SCOPE_ID)
                    .get(Session.class, Qualifier.named(Constants.SCOPE_SESSION));
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (session != null) {
            assertEquals(Constants.ID, session.getId());
        }
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
