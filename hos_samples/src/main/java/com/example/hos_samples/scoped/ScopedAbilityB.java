package com.example.hos_samples.scoped;

import com.example.hos_samples.scoped.slice.ScopedAbilityBSlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class ScopedAbilityB extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(ScopedAbilityBSlice.class.getName());
    }
}
