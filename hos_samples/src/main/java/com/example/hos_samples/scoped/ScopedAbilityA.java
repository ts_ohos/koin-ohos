package com.example.hos_samples.scoped;

import com.example.hos_samples.components.Constants;
import com.example.hos_samples.components.scope.Session;
import com.example.hos_samples.components.scope.SessionAbility;
import com.example.hos_samples.scoped.slice.ScopedAbilityASlice;
import ohos.aafwk.content.Intent;
import org.koin.core.qualifier.Qualifier;
import org.koin.core.scope.Scope;
import org.koin.hos.scope.ScopeAbility;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class ScopedAbilityA extends ScopeAbility {

    // Inject from current scope
    private Session currentSession;
    private SessionAbility currentAbilitySession;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(ScopedAbilityASlice.class.getName());

        initInfo();
    }

    private void initInfo() {

        Session session1 = null;
        Session session2 = null;
        Session session3 = null;
        Session session4 = null;
        Scope scopeSession1 = null;
        Scope scopeSession2 = null;

        try {
            currentSession = getScope().inject(Session.class);
            session1 = getScope().get(Session.class);

            // Compare different scope instances
            scopeSession1 = getScope().getKoin().createScope(Constants.SESSION_1,
                    Qualifier.named(Constants.SCOPE_ID));
            scopeSession2 = getScope().getKoin().createScope(Constants.SESSION_2,
                    Qualifier.named(Constants.SCOPE_ID));

            session2 = scopeSession1.<Session>get(Session.class,
                    Qualifier.qualifier(Constants.SCOPE_SESSION));
            session3 = scopeSession2.<Session>get(Session.class,
                    Qualifier.qualifier(Constants.SCOPE_SESSION));

            session4 = getScope().getKoin()
                    .createScope(Constants.SCOPE_ID, Qualifier.named(Constants.SCOPE_ID))
                    .get(Session.class, Qualifier.named(Constants.SCOPE_SESSION));

            session4.setId(Constants.ID);

            currentAbilitySession = getScope().inject(SessionAbility.class);

            System.out.println("my ability current : " + currentAbilitySession);

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("my ability a error: " + e.getMessage());
        }

        assertEquals(session1, currentSession);
        assertNotEquals(currentSession, session2);
        assertNotEquals(session2, session3);

    }
}
