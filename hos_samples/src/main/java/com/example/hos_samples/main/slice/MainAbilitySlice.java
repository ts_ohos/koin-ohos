package com.example.hos_samples.main.slice;

import com.example.hos_samples.ResourceTable;
import com.example.hos_samples.components.Constants;
import com.example.hos_samples.components.main.DumbServiceImpl;
import com.example.hos_samples.components.main.RandomId;
import com.example.hos_samples.components.main.Service;
import com.example.hos_samples.components.main.ServiceImpl;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import org.junit.Assert;
import org.koin.core.qualifier.Qualifier;
import org.koin.hos.ext.hos.ContextExt;

public class MainAbilitySlice extends AbilitySlice {

    // Inject by Interface - default definition
    private Service service;

    // Inject by Interface - qualified definition with a String name
    private Service dumbService;

    // Inject factory
    private RandomId randomId;

    // Inject property from koin.properties
    private String propertyTitle;

    private Button mBtnMain;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        initInfo();
        super.setUIContent(ResourceTable.Layout_ability_main);

        setEvent();
    }

    private void setEvent() {
        mBtnMain = (Button) findComponentById(ResourceTable.Id_main_button);

        mBtnMain.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent intent = new Intent();

                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName(getBundleName())
                        .withAbilityName("com.example.hos_samples.mvp.MVPAbility")
                        .build();

                intent.setOperation(operation);
                startAbility(intent);
            }
        });
    }

    private void initInfo() {

        service = ContextExt.inject(this, Service.class);
        dumbService = ContextExt.inject(this, Service.class, Qualifier.named("dumb"));
        randomId = ContextExt.inject(this, RandomId.class);
        propertyTitle = ContextExt.getKoin(this)
                .getProperty("app.title");

        Assert.assertEquals(ServiceImpl.SERVICE_IMPL, service.getId());
        Assert.assertEquals(DumbServiceImpl.DUMB_SERVICE, dumbService.getId());
        Assert.assertNotEquals(ContextExt.<RandomId>get(this, RandomId.class).id, randomId.id);
        Assert.assertEquals(Constants.APP_TITLE, propertyTitle);

    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
