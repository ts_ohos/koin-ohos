package com.example.hos_samples.components.mvp;

import com.example.hos_samples.components.main.Service;

public class ScopedPresenter {

    private String id;
    private Service service;

    public ScopedPresenter(String id, Service service) {
        this.id = id;
        this.service = service;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
