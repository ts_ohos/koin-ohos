package com.example.hos_samples.components;

public class Counter {

    private static int released = 0;

    public static int getReleased() {
        return released;
    }

    public static void setReleased(int released) {
        Counter.released = released;
    }
}
