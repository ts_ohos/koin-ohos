package com.example.hos_samples.components.scope;

import ohos.aafwk.ability.Ability;
import java.util.UUID;

public class SessionAbility {

    private Ability ability;
    private String id;

    public SessionAbility(Ability ability, String id) {
        this.ability = ability;
        this.id = id;
    }

    public SessionAbility(Ability ability) {
        this(ability, UUID.randomUUID().toString());
    }

    public Ability getAbility() {
        return ability;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
