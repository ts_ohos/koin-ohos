package com.example.hos_samples.components.main;

public class ServiceImpl implements Service {

    public static final String SERVICE_IMPL = "DefaultService";

    @Override
    public String getId() {
        return SERVICE_IMPL;
    }
}
