package com.example.hos_samples.components.main;

import java.util.UUID;

public class RandomId {

    public final String id = UUID.randomUUID().toString();

}
