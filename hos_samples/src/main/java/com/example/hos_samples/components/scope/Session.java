package com.example.hos_samples.components.scope;

import java.util.UUID;

public class Session {

    private String id;

    public Session(String id) {
        this.id = id;
    }

    public Session() {
        this(UUID.randomUUID().toString());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
