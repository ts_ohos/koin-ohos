package com.example.hos_samples.components.main;

public class DumbServiceImpl implements Service {

    public static final String DUMB_SERVICE = "Dumb Service";

    @Override
    public String getId() {
        return DUMB_SERVICE;
    }
}
