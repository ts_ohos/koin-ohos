package com.example.hos_samples;

import com.example.hos_samples.di.AppModule;
import ohos.aafwk.ability.AbilityPackage;
import org.koin.core.KoinApplication;
import org.koin.core.context.DefaultContextExt;
import org.koin.core.error.DefinitionOverrideException;
import org.koin.core.error.KoinAppAlreadyStartedException;
import org.koin.core.error.NoScopeDefFoundException;
import org.koin.core.error.ScopeAlreadyCreatedException;
import org.koin.core.logger.Level;
import org.koin.hos.ext.koin.KoinExt;
import org.koin.hos.logger.HosLogger;

public class MyApplication extends AbilityPackage {
    @Override
    public void onInitialize() {
        super.onInitialize();

        try {
            DefaultContextExt.startKoin(new org.koin.dsl.KoinAppDeclaration() {
                @Override
                public void invoke(KoinApplication koinApplication) {
                    koinApplication.logger(new HosLogger(Level.DEBUG));
                    KoinExt.hosContext(koinApplication, getContext());
                    KoinExt.hosFileProperties(koinApplication);
                    try {
                        koinApplication.modules(AppModule.allModules);
                    } catch (DefinitionOverrideException e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (NoScopeDefFoundException e) {
            e.printStackTrace();
        } catch (ScopeAlreadyCreatedException e) {
            e.printStackTrace();
        } catch (KoinAppAlreadyStartedException e) {
            e.printStackTrace();
        }
    }
}
