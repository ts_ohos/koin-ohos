package com.example.hos_samples.dynamic;

import com.example.hos_samples.dynamic.slice.DynamicAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class DynamicAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(DynamicAbilitySlice.class.getName());
    }
}
