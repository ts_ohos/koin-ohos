# KOIN

本项目是基于开源项目InsertKoinIO/koin进行适用harmonyos的移植和开发的，
可通过github地址https://github.com/InsertKoinIO/koin 追踪到原项目

## 项目介绍
koin是一个实用型轻量级依赖注入框架

#### 项目名称：KOIN
#### 所属系列：harmonyos的第三方组件适配移植
#### 功能：koin是一个实用型轻量级依赖注入框架
#### 项目移植状态：未移植部分请参考[changelog](./CHANGELOG.md)
#### 调用差异：无
#### 原项目GitHub地址：https://github.com/InsertKoinIO/koin

## 安装教程

在根目录的`build.gradle`文件中添加`mavenCentral()`：
```groovy
   // Top-level build file where you can add configuration options common to all sub-projects/modules.
   ...
   buildscript {
       repositories {
           ...
           mavenCentral()
       }
       ...
   }
   
   allprojects {
       repositories {
           ...
           mavenCentral()
       }
   }
```

在你的项目中添加依赖

1.如果只使用koin相关api，则可以只引入核心库
```xml
dependencies {
    implementation 'com.gitee.ts_ohos:koin_core:1.0.1'
}
```
2.如果要使用hos的相关特性，则可以引入以下内容
```xml
dependencies {
    implementation 'com.gitee.ts_ohos:koin_hos:1.0.1'
}
```

## 使用说明

### Java
下面的示例展示了通过koin注册和解析模块

在该示例中：
- `HelloMessageData` - 数据持有者
- `HelloService` - 使用和展示来自`HelloMessageData`中的数据
- `HelloApplication` - 解析和使用`HelloService`


#### Data holder
先创建`HelloMessageData`用于存储需要展示的数据
```java
public class HelloMessageData {

    public String message;

    public HelloMessageData(String message) {
        this.message = message;
    }

    public HelloMessageData() {
        this.message = "Hello Koin!";
    }
}
```

#### Service
创建用于展示`HelloMessageData`数据的类。`HelloServiceImpl`类实现了接口`HelloService`:
```java
/**
 * Hello Service - interface
 */
public interface HelloService {

    String hello();

}

/**
 * Hello Service Impl
 * Will use HelloMessageData data
 */
public class HelloServiceImpl implements HelloService {

    private HelloMessageData helloMessageData;

    public HelloServiceImpl(HelloMessageData helloMessageData) {
        this.helloMessageData = helloMessageData;
    }

    @Override
    public String hello() {
        return "Hey, " + helloMessageData.message;
    }
}

```

#### Application
要运行`HelloService`组件，需要创建一个运行时组件。
现在创建`HelloApplication`实现了`KoinComponent`接口。
稍后可以使用`inject`来解析需要的组件
```java

/**
 * HelloApplication - Application Class
 * use HelloService
 */
public class HelloApplication implements KoinComponent {

    public HelloService helloService;

    public HelloApplication() {
        try {
            // Inject HelloService
            helloService = inject(HelloService.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    // display our data
    public void sayHello() {
        System.out.println(helloService.hello());
    }
}
```

#### 依赖声明
现在，通过Koin的module，我们将`HelloMessageData`和`HelloService`关联起来:

```java
public class HelloModule {

    public static final Module helloModule = org.koin.dsl.Module.module(new ModuleDeclaration() {
        @Override
        public void invoke(Module module) {
            try {
                module.single(HelloMessageData.class, new Definition<HelloMessageData>() {
                    @Override
                    public HelloMessageData invoke(Scope scope, DefinitionParameters parameters) {
                        return new HelloMessageData();
                    }
                });

                module.single(HelloService.class, new Definition<HelloService>() {
                    @Override
                    public HelloService invoke(Scope scope, DefinitionParameters parameters) {
                        try {
                            return new HelloServiceImpl(scope.get(HelloMessageData.class));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return null;
                    }
                });

            } catch (DefinitionOverrideException e) {
                e.printStackTrace();
            }
        }
    });

}
```

这个示例中的两个组件都是以`single`模式声明，即两个组件都是单例模式。
- `module.single(HelloMessageData.class,...)`：声明一个`HelloMessageData`的单例模式的实例
- `module.single(HelloService.class,...)`：声明一个`HelloService`的单例模式，其实例为`HelloServiceImpl`
，并将`HelloMessageData`注入其中

#### 验证结果
我们可以在main中去使用刚才注册的模块:
```java
public static void main(String[] args) {
        try {

            //start koin
            DefaultContextExt.startKoin(new org.koin.dsl.KoinAppDeclaration() {
                @Override
                public void invoke(KoinApplication koinApplication) {

                    // use Koin logger
                    koinApplication.printLogger();

                    try {
                        // declare modules
                        koinApplication.modules(HelloModule.helloModule);
                    } catch (DefinitionOverrideException e) {
                        e.printStackTrace();
                    }
                }
            });

            //use it
            new HelloApplication().sayHello();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
```

#### 更多示例可以参考
- `examples`
- `quickstart_hos`
- `hos_samples`

## 移植版本
Tag 3.0.2

## 版本迭代
- v1.0.1 fix bugs
- v1.0.0 ohos首次移植版本

## License

```
/*
 * Copyright 2017-2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
```
