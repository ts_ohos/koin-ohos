package org.koin.test;

import org.koin.core.qualifier.Qualifier;
import org.koin.mp.KoinPlatformTools;

public class Components {
    public static class Simple {

        public static class ComponentA {
        }

        public static class ComponentB {
            ComponentA a;

        }

        public static class ComponentC {
            ComponentB b;
        }

        public static class MyString {
            String s;

            public MyString(String s) {
                this.s = s;
            }
        }

        public static class UUIDComponent {
            private void getUUID() {
                KoinPlatformTools.INSTANCE().generateId();
            }
        }
    }
}

class UpperCase extends Qualifier {

    public static volatile UpperCase INSTANCE;
    private String value = "UpperCase";

    public static UpperCase INSTANCE() {
        if (INSTANCE == null) {
            synchronized (UpperCase.class) {
                if (INSTANCE == null) {
                    INSTANCE = new UpperCase();
                }
            }
        }
        return INSTANCE;
    }
}
