package org.koin.test;

import org.junit.Assert;
import org.junit.Test;
import org.koin.core.KoinApplication;
import org.koin.core.definition.Definition;
import org.koin.core.error.*;
import org.koin.core.module.Module;
import org.koin.core.parameter.DefinitionParameters;
import org.koin.core.qualifier.Qualifier;
import org.koin.core.scope.Scope;
import org.koin.dsl.KoinAppDeclaration;
import org.koin.dsl.ModuleDeclaration;

import static org.koin.core.context.DefaultContextExt.startKoin;
import static org.koin.core.context.DefaultContextExt.stopKoin;

public class KoinQualifierTest implements KoinTest {

    private Qualifier qualifier = Qualifier.qualifier("42");
    //TODO 2021/6/11 java 不支持 lazy，后面直接赋值。val myString42: Simple.MyString by inject(qualifier)
    private Components.Simple.MyString myString42;

    @Test
    public void use_0qualifier() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, KoinAppAlreadyStartedException {

        startKoin(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {
                Module module = org.koin.dsl.Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(Module module) {
                        try {
                            module.single(Components.Simple.MyString.class, qualifier, new Definition<Components.Simple.MyString>() {
                                @Override
                                public Components.Simple.MyString invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Components.Simple.MyString("42");
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        try {
            myString42 = get(Components.Simple.MyString.class, qualifier, null);
            Assert.assertNotNull(myString42);
        } catch (DefinitionParameterException e) {
            e.printStackTrace();
        } catch (NoBeanDefFoundException e) {
            e.printStackTrace();
        } catch (ClosedScopeException e) {
            e.printStackTrace();
        }

        stopKoin();
    }
}
