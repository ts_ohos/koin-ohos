package org.koin.test;

import org.junit.Test;
import org.koin.core.KoinApplication;
import org.koin.core.definition.Definition;
import org.koin.core.error.*;
import org.koin.core.logger.Level;
import org.koin.core.parameter.DefinitionParameters;
import org.koin.core.scope.Scope;
import org.koin.dsl.KoinAppDeclaration;
import org.koin.dsl.Module;
import org.koin.dsl.ModuleDeclaration;

import static org.junit.Assert.fail;
import static org.koin.core.context.DefaultContextExt.*;

public class DeclareTest implements KoinTest {

    @Test
    public void declare_on_the_fly_with_KoinTest() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, KoinAppAlreadyStartedException {
        startKoin(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
            }
        });

        try {
            get(Components.Simple.ComponentA.class);
            fail();
        } catch (Exception e) {
            e.printStackTrace();
        }

        loadKoinModules(Module.module(new ModuleDeclaration() {
            @Override
            public void invoke(org.koin.core.module.Module module) {
                try {
                    module.single(Components.Simple.ComponentA.class, new Definition<Components.Simple.ComponentA>() {
                        @Override
                        public Components.Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                            return new Components.Simple.ComponentA();
                        }
                    });
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }));

        try {
            get(Components.Simple.ComponentA.class);
        } catch (DefinitionParameterException e) {
            e.printStackTrace();
        } catch (NoBeanDefFoundException e) {
            e.printStackTrace();
        } catch (ClosedScopeException e) {
            e.printStackTrace();
        }

        stopKoin();
    }
}
