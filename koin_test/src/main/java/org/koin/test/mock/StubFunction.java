package org.koin.test.mock;

public interface StubFunction<T> {
    void invoke(T t);
}
