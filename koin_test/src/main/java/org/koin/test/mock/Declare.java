package org.koin.test.mock;

import org.koin.core.Koin;
import org.koin.core.error.DefinitionOverrideException;
import org.koin.core.qualifier.Qualifier;
import org.koin.mp.KoinPlatformTools;
import org.koin.test.KoinTest;
import java.util.ArrayList;

public class Declare {

    public static <T> T declare(Class<T> clazz,
                                KoinTest koinTest,
                                Qualifier qualifier,
                                T instance) {
        Koin koin = KoinPlatformTools.INSTANCE().defaultContext().get();
        try {
            koin.declare(clazz, instance, qualifier, new ArrayList<>(), true);
        } catch (DefinitionOverrideException e) {
            e.printStackTrace();
        }
        return koinTest.get(clazz, qualifier, null);
    }
}
