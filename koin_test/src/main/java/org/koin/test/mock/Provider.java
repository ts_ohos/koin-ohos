package org.koin.test.mock;

public interface Provider<T> {
    T invoke(Class<T> clazz);
}
