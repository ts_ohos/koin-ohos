package org.koin.test.mock;

public class MockProvider {

    public static volatile MockProvider INSTANCE;
    private static Provider _provider;

    public MockProvider() {
    }

    public static Provider getProvider() {
        Provider provider = _provider;
        if (provider == null) {
            throw new IllegalStateException("Missing MockProvider. Please use MockProvider.register() to register a new mock provider");
        }
        return provider;
    }

    public static MockProvider INSTANCE() {
        if (INSTANCE == null) {
            synchronized (MockProvider.class) {
                if (INSTANCE == null) {
                    INSTANCE = new MockProvider();
                }
            }
        }
        return INSTANCE;
    }

    public static void register(Provider provider) {
        MockProvider._provider = provider;
    }

    public <T> Object makeMock() {
        return getProvider().invoke(Object.class);
    }

    public <T> Object makeMock(Class clazz) {
        return getProvider().invoke(clazz);
    }

}
