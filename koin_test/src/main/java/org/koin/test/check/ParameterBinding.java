package org.koin.test.check;

import org.koin.core.Koin;
import org.koin.core.qualifier.Qualifier;
import org.koin.mp.KoinPlatformTools;
import org.koin.test.mock.MockProvider;
import java.util.LinkedHashMap;
import java.util.Map;

public class ParameterBinding {

    private Koin koin;

    private Map parametersCreators = new LinkedHashMap<CheckedComponent, ParametersCreator>();

    private Map defaultValues = new LinkedHashMap<String, Object>();

    public ParameterBinding(Koin koin) {
        super();
        this.koin = koin;
    }

    public Koin getKoin() {
        return koin;
    }

    public Map getParametersCreators() {
        return this.parametersCreators;
    }

    public Map getDefaultValues() {
        return this.defaultValues;
    }

    public <T> void create(ParametersCreator creator) {
        create((Qualifier) null, creator);
    }

    //TODO 2021/6/10 Object.class 不确定，同下
    public <T> void create(Qualifier qualifier, ParametersCreator creator) {
        this.getParametersCreators().put(new CheckedComponent(qualifier, Object.class), creator);
    }

    public void create(Class clazz, ParametersCreator creator) {
        create(clazz, null, creator);
    }

    public void create(Class clazz, Qualifier qualifier, ParametersCreator creator) {
        this.getParametersCreators().put(new CheckedComponent(qualifier, clazz), creator);
    }

    //TODO 2021/6/10
    public <T> void defaultValue(Class<T> clazz, Object t) {
        this.getDefaultValues().put(KoinPlatformTools.INSTANCE().getClassName(clazz), t);
    }

    public <T> void defaultValue(Class<T> clazz) {
        this.getDefaultValues().put(KoinPlatformTools.INSTANCE().getClassName(clazz),
                MockProvider.INSTANCE().makeMock());
    }
}
