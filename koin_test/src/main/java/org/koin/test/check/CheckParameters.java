package org.koin.test.check;

public interface CheckParameters {
    void invoke(ParameterBinding parameterBinding);
}
