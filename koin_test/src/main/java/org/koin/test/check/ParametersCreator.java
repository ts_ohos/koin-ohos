package org.koin.test.check;

import org.koin.core.parameter.DefinitionParameters;
import org.koin.core.qualifier.Qualifier;

public interface ParametersCreator {
    DefinitionParameters invoke(Qualifier qualifier);
}
