package org.koin.test.check;

import org.koin.core.qualifier.Qualifier;

public class CheckedComponent {
    private Qualifier qualifier;
    private Class type;

    public CheckedComponent(Qualifier qualifier, Class type) {
        super();
        this.qualifier = qualifier;
        this.type = type;
    }

    public Qualifier getQualifier() {
        return qualifier;
    }

    public void setQualifier(Qualifier qualifier) {
        this.qualifier = qualifier;
    }

    public Class getType() {
        return type;
    }

    public void setType(Class type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "CheckComPonent{}";
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}
