package org.koin.test;

import org.koin.core.component.KoinComponent;
import org.koin.core.error.ClosedScopeException;
import org.koin.core.error.DefinitionParameterException;
import org.koin.core.error.NoBeanDefFoundException;
import org.koin.core.parameter.ParametersDefinition;
import org.koin.core.qualifier.Qualifier;

public interface KoinTest extends KoinComponent {

    /**
     * Koin Test Component
     */
    public default <T> T get(Class<T> clazz,
                             Qualifier qualifier,
                             ParametersDefinition parameters) {
        try {
            return getKoin().get(clazz, qualifier, parameters);
        } catch (DefinitionParameterException e) {
            e.printStackTrace();
        } catch (NoBeanDefFoundException e) {
            e.printStackTrace();
        } catch (ClosedScopeException e) {
            e.printStackTrace();
        }
        return null;
    }

    default <T> T get(Class<T> clazz) {
        return get(clazz, null, null);
    }

    default <T> T get(Class<T> clazz,
                      Qualifier qualifier) {
        return get(clazz, qualifier, null);
    }

    default <T> T get(Class<T> clazz,
                      ParametersDefinition parameters) {
        return get(clazz, null, parameters);
    }
}
