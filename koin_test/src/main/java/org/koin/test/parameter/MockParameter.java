package org.koin.test.parameter;

import org.koin.core.error.DefinitionParameterException;
import org.koin.core.error.NoParameterFoundException;
import org.koin.core.parameter.DefinitionParameters;
import org.koin.core.scope.Scope;
import org.koin.mp.KoinPlatformTools;
import org.koin.test.mock.MockProvider;
import java.util.*;

public class MockParameter extends DefinitionParameters {

    private Scope scope;

    private Map defaultValues = new LinkedHashMap<String, Object>();

    public MockParameter(Scope scope, Map defaultValues) {
        super((List) new ArrayList<>());
        this.scope = scope;
        this.defaultValues = defaultValues;
    }

    @Override
    public Object elementAt(int i, Class clazz) throws NoParameterFoundException {
        Object object = this.defaultValues.get(KoinPlatformTools.INSTANCE().getClassName(clazz));
        if (!(object instanceof Object)) {
            object = null;
        }
        if (object == null) {
            object = this.getDefaultPrimaryValue(clazz);
        }
        if (object == null) {
            object = MockProvider.INSTANCE().makeMock(clazz);
        }
        return object;
    }

    private <T> T getDefaultPrimaryValue(Class clazz) {
        T result;
        String className = KoinPlatformTools.INSTANCE().getClassName(clazz);
        if (className.equals(KoinPlatformTools.INSTANCE().getClassName(String.class))) {
            result = (T) "";
        } else if (className.equals(KoinPlatformTools.INSTANCE().getClassName(Integer.class))) {
            result = (T) Integer.valueOf(0);
        } else if (className.equals(KoinPlatformTools.INSTANCE().getClassName(Double.class))) {
            result = (T) Double.valueOf(0.0);
        } else if (className.equals(KoinPlatformTools.INSTANCE().getClassName(Float.class))) {
            result = (T) Float.valueOf(0.0f);
        } else {
            result = null;
        }
        return result;
    }

    @Override
    public Object getOrNull(Class clazz) throws DefinitionParameterException {
        Iterator iterator = this.defaultValues.values().iterator();
        Object t;
        while (true) {
            if (iterator.hasNext()) {
                Object it = iterator.next();
                if (!(it.getClass() == clazz)) {
                    continue;
                }
                t = it;
                break;
            }
            t = null;
            break;
        }

        if (t instanceof Object) {
            t = null;
        }

        if (t == null) {
            t = this.getDefaultPrimaryValue(clazz);
        }
        return t;
    }

    public Scope getScope() {
        return scope;
    }
}
