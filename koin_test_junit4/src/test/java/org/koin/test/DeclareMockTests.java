package org.koin.test;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.koin.core.Koin;
import org.koin.core.definition.Definition;
import org.koin.core.error.*;
import org.koin.core.logger.Level;
import org.koin.core.module.Module;
import org.koin.core.parameter.DefinitionParameters;
import org.koin.core.qualifier.Qualifier;
import org.koin.core.scope.Scope;
import org.koin.dsl.KoinAppDeclaration;
import org.koin.dsl.KoinApplication;
import org.koin.dsl.ModuleDeclaration;
import org.koin.dsl.ScopeDSL;
import org.koin.test.mock.DeclareMock;
import org.koin.test.mock.MockProviderRule;
import org.koin.test.mock.Provider;
import org.koin.test.mock.StubFunction;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;

public class DeclareMockTests extends AutoCloseKoinTest {

    @Rule
    public MockProviderRule mockProvider = MockProviderRule.create(new Provider() {
        @Override
        public Object invoke(Class clazz) {
            return Mockito.mock(clazz);
        }
    });

    @Test
    public void declare_and_stub_an_existing_definition() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        Koin koin = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                Module module = org.koin.dsl.Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(Module module) {
                        try {
                            module.single(Components.Simple.UUIDComponent.class, new Definition<Components.Simple.UUIDComponent>() {
                                @Override
                                public Components.Simple.UUIDComponent invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Components.Simple.UUIDComponent();
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        try {
            Components.Simple.UUIDComponent instance = koin.get(Components.Simple.UUIDComponent.class);
            String uuidValue = "UUID";

            DeclareMock.declareMock(koin, Components.Simple.UUIDComponent.class, null, new ArrayList<>(), new StubFunction<Components.Simple.UUIDComponent>() {
                @Override
                public void invoke(Components.Simple.UUIDComponent uuidComponent) {
                    BDDMockito.given(uuidComponent.getUUID()).will(new Answer<String>() {
                        @Override
                        public String answer(InvocationOnMock invocation) throws Throwable {
                            return uuidValue;
                        }
                    });
                }
            });

            Components.Simple.UUIDComponent mock = koin.get(Components.Simple.UUIDComponent.class);

            Assert.assertNotEquals(instance, mock);
            Assert.assertEquals(uuidValue, mock.getUUID());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void declare_and_stub_an_existing_scoped_definition() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        Koin koin = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                Module module = org.koin.dsl.Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(Module module) {
                        module.scope(Qualifier.named(Components.Simple.class), new Module.ScopeSet() {
                            @Override
                            public void invoke(ScopeDSL scopeDSL) {
                                try {
                                    scopeDSL.scoped(Components.Simple.UUIDComponent.class, new Definition<Components.Simple.UUIDComponent>() {
                                        @Override
                                        public Components.Simple.UUIDComponent invoke(Scope scope, DefinitionParameters parameters) {
                                            return new Components.Simple.UUIDComponent();
                                        }
                                    });
                                } catch (DefinitionOverrideException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                });
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        Scope scope = koin.getOrCreateScope("scope_id", Qualifier.named(Components.Simple.class));

        try {
            Object instance = scope.get(Components.Simple.UUIDComponent.class);
            String uuidValue = "UUID";

            DeclareMock.declareMock(scope, Components.Simple.UUIDComponent.class, null, new ArrayList<>(), new StubFunction<Components.Simple.UUIDComponent>() {
                @Override
                public void invoke(Components.Simple.UUIDComponent uuidComponent) {
                    BDDMockito.given(uuidComponent.getUUID()).will(new Answer<String>() {
                        @Override
                        public String answer(InvocationOnMock invocation) throws Throwable {
                            return uuidValue;
                        }
                    });
                }
            });

            Components.Simple.UUIDComponent mock = scope.get(Components.Simple.UUIDComponent.class);

            Assert.assertNotEquals(instance, mock);
            Assert.assertEquals(uuidValue, mock.getUUID());
        } catch (ClosedScopeException e) {
            e.printStackTrace();
        } catch (DefinitionParameterException e) {
            e.printStackTrace();
        } catch (NoBeanDefFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void declare_a_Mock_of_an_existing_definition_for_given_scope() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        Koin koin = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                Module module = org.koin.dsl.Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(Module module) {
                        module.scope(Qualifier.named(Components.Simple.class), new Module.ScopeSet() {
                            @Override
                            public void invoke(ScopeDSL scopeDSL) {
                                try {
                                    scopeDSL.scoped(Components.Simple.ComponentA.class, new Definition<Components.Simple.ComponentA>() {
                                        @Override
                                        public Components.Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                            return new Components.Simple.ComponentA();
                                        }
                                    });
                                } catch (DefinitionOverrideException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                });
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        Scope scope = koin.getOrCreateScope("scope_id", Qualifier.named(Components.Simple.class));

        try {
            Object instance = scope.get(Components.Simple.ComponentA.class);

            DeclareMock.declareMock(scope, Components.Simple.ComponentA.class, null, new ArrayList<>(), new StubFunction<Components.Simple.ComponentA>() {
                @Override
                public void invoke(Components.Simple.ComponentA componentA) {

                }
            });

            Object mock = scope.get(Components.Simple.ComponentA.class);

            Assert.assertNotEquals(instance, mock);
        } catch (ClosedScopeException e) {
            e.printStackTrace();
        } catch (DefinitionParameterException e) {
            e.printStackTrace();
        } catch (NoBeanDefFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void declare_and_mock_an_existing_definition_for_given_scope() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        Koin koin = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                Module module = org.koin.dsl.Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(Module module) {
                        module.scope(Qualifier.named(Components.Simple.class), new Module.ScopeSet() {
                            @Override
                            public void invoke(ScopeDSL scopeDSL) {
                                try {
                                    scopeDSL.scoped(Components.Simple.UUIDComponent.class, new Definition<Components.Simple.UUIDComponent>() {
                                        @Override
                                        public Components.Simple.UUIDComponent invoke(Scope scope, DefinitionParameters parameters) {
                                            return new Components.Simple.UUIDComponent();
                                        }
                                    });
                                } catch (DefinitionOverrideException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                });
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        Scope scope = koin.getOrCreateScope("scope_id", Qualifier.named(Components.Simple.class));

        try {
            Object instance = scope.get(Components.Simple.UUIDComponent.class);
            String uuidValue = "UUID";

            DeclareMock.declareMock(scope, Components.Simple.UUIDComponent.class, null, new ArrayList<>(), new StubFunction<Components.Simple.UUIDComponent>() {
                @Override
                public void invoke(Components.Simple.UUIDComponent uuidComponent) {
                    BDDMockito.given(uuidComponent.getUUID()).will(new Answer<String>() {
                        @Override
                        public String answer(InvocationOnMock invocation) throws Throwable {
                            return uuidValue;
                        }
                    });
                }
            });

            Components.Simple.UUIDComponent mock = scope.get(Components.Simple.UUIDComponent.class);

            Assert.assertNotEquals(instance, mock);
            Assert.assertEquals(uuidValue, mock.getUUID());
        } catch (ClosedScopeException e) {
            e.printStackTrace();
        } catch (DefinitionParameterException e) {
            e.printStackTrace();
        } catch (NoBeanDefFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void declare_a_Mock_of_an_existing_definition() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        Koin koin = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                Module module = org.koin.dsl.Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(Module module) {
                        try {
                            module.single(Components.Simple.ComponentA.class, new Definition<Components.Simple.ComponentA>() {
                                @Override
                                public Components.Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Components.Simple.ComponentA();
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        try {
            Components.Simple.ComponentA instance = koin.get(Components.Simple.ComponentA.class);

            DeclareMock.declareMock(koin, Components.Simple.ComponentA.class, null, new ArrayList<>(), new StubFunction<Components.Simple.ComponentA>() {
                @Override
                public void invoke(Components.Simple.ComponentA componentA) {

                }
            });

            Components.Simple.ComponentA mock = koin.get(Components.Simple.ComponentA.class);

            Assert.assertNotEquals(instance, mock);
        } catch (DefinitionParameterException e) {
            e.printStackTrace();
        } catch (NoBeanDefFoundException e) {
            e.printStackTrace();
        } catch (ClosedScopeException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void declare_and_mock_an_existing_definition() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        Koin koin = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                Module module = org.koin.dsl.Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(Module module) {
                        try {
                            module.single(Components.Simple.UUIDComponent.class, new Definition<Components.Simple.UUIDComponent>() {
                                @Override
                                public Components.Simple.UUIDComponent invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Components.Simple.UUIDComponent();
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        try {
            Components.Simple.UUIDComponent instance = koin.get(Components.Simple.UUIDComponent.class);
            String uuidValue = "UUID";

            DeclareMock.declareMock(koin, Components.Simple.UUIDComponent.class, null, new ArrayList<>(), new StubFunction<Components.Simple.UUIDComponent>() {
                @Override
                public void invoke(Components.Simple.UUIDComponent uuidComponent) {
                    BDDMockito.given(uuidComponent.getUUID()).will(new Answer<String>() {
                        @Override
                        public String answer(InvocationOnMock invocation) throws Throwable {
                            return uuidValue;
                        }
                    });
                }
            });

            Components.Simple.UUIDComponent mock = koin.get(Components.Simple.UUIDComponent.class);

            Assert.assertNotEquals(instance, mock);
            Assert.assertEquals(uuidValue, mock.getUUID());
        } catch (DefinitionParameterException e) {
            e.printStackTrace();
        } catch (NoBeanDefFoundException e) {
            e.printStackTrace();
        } catch (ClosedScopeException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void mock_with_qualifier() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        Koin koin = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                Module module = org.koin.dsl.Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(Module module) {
                        try {
                            module.single(Components.Simple.ComponentA.class, Qualifier.named("test"), new Definition<Components.Simple.ComponentA>() {
                                @Override
                                public Components.Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Components.Simple.ComponentA();
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        DeclareMock.declareMock(koin, Components.Simple.ComponentA.class, Qualifier.named("test"), new ArrayList<>(), new StubFunction<Components.Simple.ComponentA>() {
            @Override
            public void invoke(Components.Simple.ComponentA componentA) {

            }
        });
    }

}
