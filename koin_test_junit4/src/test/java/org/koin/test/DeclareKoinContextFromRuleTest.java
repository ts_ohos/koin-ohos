package org.koin.test;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.koin.core.KoinApplication;
import org.koin.core.definition.Definition;
import org.koin.core.error.ClosedScopeException;
import org.koin.core.error.DefinitionOverrideException;
import org.koin.core.error.DefinitionParameterException;
import org.koin.core.error.NoBeanDefFoundException;
import org.koin.core.logger.Level;
import org.koin.core.module.Module;
import org.koin.core.parameter.DefinitionParameters;
import org.koin.core.scope.Scope;
import org.koin.dsl.KoinAppDeclaration;
import org.koin.dsl.ModuleDeclaration;
import org.koin.java.KoinJavaComponent;

public class DeclareKoinContextFromRuleTest extends AutoCloseKoinTest {

    @Rule
    public KoinTestRule koinTestRule = KoinTestRule.create(new KoinAppDeclaration() {
        @Override
        public void invoke(KoinApplication koinApplication) {
            koinApplication.printLogger(Level.DEBUG);
            Module module = org.koin.dsl.Module.module(new ModuleDeclaration() {
                @Override
                public void invoke(Module module) {
                    try {
                        module.single(Components.Simple.ComponentA.class, new Definition<Components.Simple.ComponentA>() {
                            @Override
                            public Components.Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                return new Components.Simple.ComponentA();
                            }
                        });

                        module.single(Components.Simple.MyString.class, new Definition<Components.Simple.MyString>() {
                            @Override
                            public Components.Simple.MyString invoke(Scope scope, DefinitionParameters parameters) {
                                return new Components.Simple.MyString("simple test");
                            }
                        });
                    } catch (DefinitionOverrideException e) {
                        e.printStackTrace();
                    }
                }
            });
            try {
                koinApplication.modules(module);
            } catch (DefinitionOverrideException e) {
                e.printStackTrace();
            }
        }
    });

    private Components.Simple.ComponentA componentA;

    @Test
    public void component_is_injected_to_class_variable() {
        componentA = KoinJavaComponent.inject(Components.Simple.ComponentA.class);
        Assert.assertNotNull(componentA);
        Assert.assertEquals(Components.Simple.ComponentA.class, componentA.getClass());
    }

    @Test
    public void whenMockWithQualifier() throws DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        Components.Simple.MyString myString = koinTestRule.getKoin().get(Components.Simple.MyString.class);
        Assert.assertEquals(myString.s, "simple test");
    }
}
