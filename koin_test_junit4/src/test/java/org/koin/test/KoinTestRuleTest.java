package org.koin.test;

import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.koin.core.KoinApplication;
import org.koin.core.parameter.ParametersDefinition;
import org.koin.core.qualifier.Qualifier;
import org.koin.dsl.KoinAppDeclaration;
import org.koin.test.mock.Declare;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class KoinTestRuleTest implements KoinTest {

    @Rule
    public KoinTestRule koinTestRule = KoinTestRule.create(new KoinAppDeclaration() {
        @Override
        public void invoke(KoinApplication koinApplication) {

        }
    });

    @Test
    public void A_can_declare_dependency_because_Koin_was_started() {
        Declare.declare(TestType.class, new KoinTest() {
            @Override
            public <T> T get(Class<T> clazz, KoinTest koinTest, Qualifier qualifier, ParametersDefinition parameters) {
                return null;
            }
        }, null, new TestType());
    }

    @Test
    public void B_can_declare_dependency_again_because_Koin_was_stopped_after_first_test() {
        Declare.declare(TestType.class, new KoinTest() {
            @Override
            public <T> T get(Class<T> clazz, KoinTest koinTest, Qualifier qualifier, ParametersDefinition parameters) {
                return null;
            }
        }, null, new TestType());
    }
}

class TestType {
    public TestType() {
    }
}
