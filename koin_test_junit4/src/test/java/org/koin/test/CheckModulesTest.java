package org.koin.test;

import org.junit.Assert;
import org.junit.Test;
import org.koin.core.definition.Definition;
import org.koin.core.error.*;
import org.koin.core.logger.Level;
import org.koin.core.module.Module;
import org.koin.core.parameter.DefinitionParameters;
import org.koin.core.qualifier.Qualifier;
import org.koin.core.scope.Scope;
import org.koin.dsl.KoinAppDeclaration;
import org.koin.dsl.KoinApplication;
import org.koin.dsl.ModuleDeclaration;
import org.koin.dsl.ScopeDSL;
import org.koin.test.check.CheckParameters;
import org.koin.test.check.ParameterBinding;
import org.koin.test.check.ParametersCreator;
import org.koin.test.mock.MockProviderRule;
import org.koin.test.mock.Provider;
import org.mockito.Mockito;

import java.util.HashMap;

import static org.junit.Assert.fail;
import static org.koin.core.parameter.DefinitionParameters.parametersOf;
import static org.koin.test.check.CheckModules.checkModules;

public class CheckModulesTest {

    private MockProviderRule mockProvider = MockProviderRule.create(new Provider() {
        @Override
        public Object invoke(Class clazz) {
            return Mockito.mock(clazz);
        }
    });

    @Test
    public void check_a_scoped_module() {
        try {
            org.koin.core.KoinApplication koinApplication = KoinApplication.koinApplication(new KoinAppDeclaration() {
                @Override
                public void invoke(org.koin.core.KoinApplication koinApplication) {
                    koinApplication.printLogger(Level.DEBUG);
                    Module module = org.koin.dsl.Module.module(new ModuleDeclaration() {
                        @Override
                        public void invoke(Module module) {
                            module.scope(Qualifier.named("scope"), new Module.ScopeSet() {
                                @Override
                                public void invoke(ScopeDSL scopeDSL) {
                                    try {
                                        scopeDSL.scoped(Components.Simple.ComponentA.class, new Definition<Components.Simple.ComponentA>() {
                                            @Override
                                            public Components.Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                                return new Components.Simple.ComponentA();
                                            }
                                        });

                                        scopeDSL.scoped(Components.Simple.ComponentB.class, new Definition<Components.Simple.ComponentB>() {
                                            @Override
                                            public Components.Simple.ComponentB invoke(Scope scope, DefinitionParameters parameters) {
                                                try {
                                                    return new Components.Simple.ComponentB(scope.get(Components.Simple.ComponentA.class));
                                                } catch (ClosedScopeException e) {
                                                    e.printStackTrace();
                                                } catch (DefinitionParameterException e) {
                                                    e.printStackTrace();
                                                } catch (NoBeanDefFoundException e) {
                                                    e.printStackTrace();
                                                }
                                                return null;
                                            }
                                        });
                                    } catch (DefinitionOverrideException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        }
                    });
                    try {
                        koinApplication.modules(module);
                    } catch (DefinitionOverrideException e) {
                        e.printStackTrace();
                    }
                }
            });

            checkModules(koinApplication, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void check_a_scoped_module_and_ext_deps_failed() {
        try {
            org.koin.core.KoinApplication koinApplication = KoinApplication.koinApplication(new KoinAppDeclaration() {
                @Override
                public void invoke(org.koin.core.KoinApplication koinApplication) {
                    koinApplication.printLogger(Level.DEBUG);
                    Module module = org.koin.dsl.Module.module(new ModuleDeclaration() {
                        @Override
                        public void invoke(Module module) {
                            try {
                                module.single(Components.Simple.ComponentB.class, new Definition<Components.Simple.ComponentB>() {
                                    @Override
                                    public Components.Simple.ComponentB invoke(Scope scope, DefinitionParameters parameters) {
                                        try {
                                            return new Components.Simple.ComponentB(scope.get(Components.Simple.ComponentA.class));
                                        } catch (ClosedScopeException e) {
                                            e.printStackTrace();
                                        } catch (DefinitionParameterException e) {
                                            e.printStackTrace();
                                        } catch (NoBeanDefFoundException e) {
                                            e.printStackTrace();
                                        }
                                        return null;
                                    }
                                });
                            } catch (DefinitionOverrideException e) {
                                e.printStackTrace();
                            }

                            module.scope(Qualifier.named("scope"), new Module.ScopeSet() {
                                @Override
                                public void invoke(ScopeDSL scopeDSL) {
                                    try {
                                        scopeDSL.scoped(Components.Simple.ComponentA.class, new Definition<Components.Simple.ComponentA>() {
                                            @Override
                                            public Components.Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                                return new Components.Simple.ComponentA();
                                            }
                                        });
                                    } catch (DefinitionOverrideException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        }
                    });
                    try {
                        koinApplication.modules(module);
                    } catch (DefinitionOverrideException e) {
                        e.printStackTrace();
                    }
                }
            });

            checkModules(koinApplication, null);
            fail();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void check_a_scoped_module_and_ext_scope_failed() {
        try {
            org.koin.core.KoinApplication koinApplication = KoinApplication.koinApplication(new KoinAppDeclaration() {
                @Override
                public void invoke(org.koin.core.KoinApplication koinApplication) {
                    koinApplication.printLogger(Level.DEBUG);
                    Module module = org.koin.dsl.Module.module(new ModuleDeclaration() {
                        @Override
                        public void invoke(Module module) {
                            module.scope(Qualifier.named("scope2"), new Module.ScopeSet() {
                                @Override
                                public void invoke(ScopeDSL scopeDSL) {
                                    try {
                                        scopeDSL.scoped(Components.Simple.ComponentB.class, new Definition<Components.Simple.ComponentB>() {
                                            @Override
                                            public Components.Simple.ComponentB invoke(Scope scope, DefinitionParameters parameters) {
                                                try {
                                                    return new Components.Simple.ComponentB(scope.get(Components.Simple.ComponentA.class));
                                                } catch (ClosedScopeException e) {
                                                    e.printStackTrace();
                                                } catch (DefinitionParameterException e) {
                                                    e.printStackTrace();
                                                } catch (NoBeanDefFoundException e) {
                                                    e.printStackTrace();
                                                }
                                                return null;
                                            }
                                        });
                                    } catch (DefinitionOverrideException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });

                            module.scope(Qualifier.named("scope1"), new Module.ScopeSet() {
                                @Override
                                public void invoke(ScopeDSL scopeDSL) {
                                    try {
                                        scopeDSL.scoped(Components.Simple.ComponentA.class, new Definition<Components.Simple.ComponentA>() {
                                            @Override
                                            public Components.Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                                return new Components.Simple.ComponentA();
                                            }
                                        });
                                    } catch (DefinitionOverrideException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        }
                    });
                    try {
                        koinApplication.modules(module);
                    } catch (DefinitionOverrideException e) {
                        e.printStackTrace();
                    }
                }
            });
            checkModules(koinApplication, null);
            fail();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void check_a_scoped_module_and_ext_scope_create_scope() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        org.koin.core.KoinApplication koinApplication = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                Module module = org.koin.dsl.Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(Module module) {
                        module.scope(Qualifier.named("scope2"), new Module.ScopeSet() {
                            @Override
                            public void invoke(ScopeDSL scopeDSL) {
                                try {
                                    scopeDSL.scoped(Components.Simple.ComponentB.class, new Definition<Components.Simple.ComponentB>() {
                                        @Override
                                        public Components.Simple.ComponentB invoke(Scope scope, DefinitionParameters parameters) {
                                            try {
                                                Components.Simple.ComponentA a = scope.getScope("scopei1").get(Components.Simple.ComponentA.class);
                                                return new Components.Simple.ComponentB(a);
                                            } catch (ClosedScopeException e) {
                                                e.printStackTrace();
                                            } catch (DefinitionParameterException e) {
                                                e.printStackTrace();
                                            } catch (NoBeanDefFoundException e) {
                                                e.printStackTrace();
                                            } catch (ScopeNotCreatedException e) {
                                                e.printStackTrace();
                                            }
                                            return null;
                                        }
                                    });
                                } catch (DefinitionOverrideException e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                        module.scope(Qualifier.named("scope1"), new Module.ScopeSet() {
                            @Override
                            public void invoke(ScopeDSL scopeDSL) {
                                try {
                                    scopeDSL.scoped(Components.Simple.ComponentA.class, new Definition<Components.Simple.ComponentA>() {
                                        @Override
                                        public Components.Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                            return new Components.Simple.ComponentA();
                                        }
                                    });
                                } catch (DefinitionOverrideException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                });
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        checkModules(koinApplication, new CheckParameters() {
            @Override
            public void invoke(ParameterBinding parameterBinding) {
                try {
                    koinApplication.getKoin().createScope("scopei1", Qualifier.named("scope1"));
                } catch (ScopeAlreadyCreatedException e) {
                    e.printStackTrace();
                } catch (NoScopeDefFoundException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Test
    public void check_a_scoped_module_and_ext_scope_inject_scope() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        org.koin.core.KoinApplication koinApplication = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                Module module = org.koin.dsl.Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(Module module) {
                        module.scope(Qualifier.named("scope2"), new Module.ScopeSet() {
                            @Override
                            public void invoke(ScopeDSL scopeDSL) {
                                try {
                                    scopeDSL.scoped(Components.Simple.ComponentB.class, new Definition<Components.Simple.ComponentB>() {
                                        @Override
                                        public Components.Simple.ComponentB invoke(Scope scope, DefinitionParameters parameters) {
                                            try {
                                                //TODO 2021/6/15 不确定
                                                Scope scope1 = scope.getScope("scope2");
                                                try {
                                                    return new Components.Simple.ComponentB(scope1.get(Components.Simple.ComponentA.class));
                                                } catch (ClosedScopeException e) {
                                                    e.printStackTrace();
                                                } catch (DefinitionParameterException e) {
                                                    e.printStackTrace();
                                                } catch (NoBeanDefFoundException e) {
                                                    e.printStackTrace();
                                                }
                                            } catch (ScopeNotCreatedException e) {
                                                e.printStackTrace();
                                            }
                                            return null;
                                        }
                                    });
                                } catch (DefinitionOverrideException e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                        module.scope(Qualifier.named("scope1"), new Module.ScopeSet() {
                            @Override
                            public void invoke(ScopeDSL scopeDSL) {
                                try {
                                    scopeDSL.scoped(Components.Simple.ComponentA.class, new Definition<Components.Simple.ComponentA>() {
                                        @Override
                                        public Components.Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                            return new Components.Simple.ComponentA();
                                        }
                                    });
                                } catch (DefinitionOverrideException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                });
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        checkModules(koinApplication, new CheckParameters() {
            @Override
            public void invoke(ParameterBinding parameterBinding) {
                parameterBinding.create(Components.Simple.ComponentB.class, new ParametersCreator() {
                    @Override
                    public DefinitionParameters invoke(Qualifier qualifier) {
                        try {
                            return parametersOf(koinApplication.getKoin().createScope("scopei1", Qualifier.named("scope1")));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return null;
                    }
                });
            }
        });
    }

    @Test
    public void check_a_simple_module() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        org.koin.core.KoinApplication koinApplication = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                Module module = org.koin.dsl.Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(Module module) {
                        try {
                            module.single(Components.Simple.ComponentA.class, new Definition<Components.Simple.ComponentA>() {
                                @Override
                                public Components.Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Components.Simple.ComponentA();
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });
        checkModules(koinApplication, null);
    }

    @Test
    public void check_a_module_with_link() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        org.koin.core.KoinApplication koinApplication = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                Module module = org.koin.dsl.Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(Module module) {
                        try {
                            module.single(Components.Simple.ComponentA.class, new Definition<Components.Simple.ComponentA>() {
                                @Override
                                public Components.Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Components.Simple.ComponentA();
                                }
                            });

                            module.single(Components.Simple.ComponentB.class, new Definition<Components.Simple.ComponentB>() {
                                @Override
                                public Components.Simple.ComponentB invoke(Scope scope, DefinitionParameters parameters) {
                                    try {
                                        return new Components.Simple.ComponentB(scope.get(Components.Simple.ComponentA.class));
                                    } catch (ClosedScopeException e) {
                                        e.printStackTrace();
                                    } catch (DefinitionParameterException e) {
                                        e.printStackTrace();
                                    } catch (NoBeanDefFoundException e) {
                                        e.printStackTrace();
                                    }
                                    return null;
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });
        checkModules(koinApplication, null);
    }

    @Test
    public void check_a_broken_module() {
        try {
            org.koin.core.KoinApplication koinApplication = KoinApplication.koinApplication(new KoinAppDeclaration() {
                @Override
                public void invoke(org.koin.core.KoinApplication koinApplication) {
                    koinApplication.printLogger(Level.DEBUG);
                    Module module = org.koin.dsl.Module.module(new ModuleDeclaration() {
                        @Override
                        public void invoke(Module module) {
                            try {
                                module.single(Components.Simple.ComponentB.class, new Definition<Components.Simple.ComponentB>() {
                                    @Override
                                    public Components.Simple.ComponentB invoke(Scope scope, DefinitionParameters parameters) {
                                        try {
                                            return new Components.Simple.ComponentB(scope.get(Components.Simple.ComponentA.class));
                                        } catch (ClosedScopeException e) {
                                            e.printStackTrace();
                                        } catch (DefinitionParameterException e) {
                                            e.printStackTrace();
                                        } catch (NoBeanDefFoundException e) {
                                            e.printStackTrace();
                                        }
                                        return null;
                                    }
                                });
                            } catch (DefinitionOverrideException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    try {
                        koinApplication.modules(module);
                    } catch (DefinitionOverrideException e) {
                        e.printStackTrace();
                    }
                }
            });
            checkModules(koinApplication, null);
            fail("should not pass with borken definitions");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void check_a_module_with_params() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        org.koin.core.KoinApplication koinApplication = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                Module module = org.koin.dsl.Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(Module module) {
                        try {
                            module.single(Components.Simple.MyString.class, new Definition<Components.Simple.MyString>() {
                                @Override
                                public Components.Simple.MyString invoke(Scope scope, DefinitionParameters parameters) {
                                    String s = null;
                                    return new Components.Simple.MyString(s);
                                }
                            });

                            module.single(Components.Simple.MyString.class, UpperCase.INSTANCE(), new Definition<Components.Simple.MyString>() {
                                @Override
                                public Components.Simple.MyString invoke(Scope scope, DefinitionParameters parameters) {
                                    String s = null;
                                    return new Components.Simple.MyString(s.toUpperCase());
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });
        checkModules(koinApplication, new CheckParameters() {
            @Override
            public void invoke(ParameterBinding parameterBinding) {
                parameterBinding.create(Components.Simple.MyString.class, new ParametersCreator() {
                    @Override
                    public DefinitionParameters invoke(Qualifier qualifier) {
                        try {
                            return parametersOf("param");
                        } catch (DefinitionParameterException e) {
                            e.printStackTrace();
                        }
                        return null;
                    }
                });

                parameterBinding.create(Components.Simple.MyString.class, UpperCase.INSTANCE(), new ParametersCreator() {
                    @Override
                    public DefinitionParameters invoke(Qualifier qualifier) {
                        try {
                            return parametersOf(qualifier.toString());
                        } catch (DefinitionParameterException e) {
                            e.printStackTrace();
                        }
                        return null;
                    }
                });
            }
        });
    }

    @Test
    public void check_a_module_with_params_using_create_method_with_KClass() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        org.koin.core.KoinApplication koinApplication = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                Module module = org.koin.dsl.Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(Module module) {
                        try {
                            module.single(Components.Simple.MyString.class, new Definition<Components.Simple.MyString>() {
                                @Override
                                public Components.Simple.MyString invoke(Scope scope, DefinitionParameters parameters) {
                                    String s = null;
                                    return new Components.Simple.MyString(s);
                                }
                            });

                            module.single(Components.Simple.MyString.class, UpperCase.INSTANCE(), new Definition<Components.Simple.MyString>() {
                                @Override
                                public Components.Simple.MyString invoke(Scope scope, DefinitionParameters parameters) {
                                    String s = null;
                                    return new Components.Simple.MyString(s.toUpperCase());
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });
        checkModules(koinApplication, new CheckParameters() {
            @Override
            public void invoke(ParameterBinding parameterBinding) {
                parameterBinding.create(Components.Simple.MyString.class, new ParametersCreator() {
                    @Override
                    public DefinitionParameters invoke(Qualifier qualifier) {
                        try {
                            return parametersOf("param");
                        } catch (DefinitionParameterException e) {
                            e.printStackTrace();
                        }
                        return null;
                    }
                });

                parameterBinding.create(Components.Simple.MyString.class, UpperCase.INSTANCE(), new ParametersCreator() {
                    @Override
                    public DefinitionParameters invoke(Qualifier qualifier) {
                        try {
                            return parametersOf(qualifier.toString());
                        } catch (DefinitionParameterException e) {
                            e.printStackTrace();
                        }
                        return null;
                    }
                });
            }
        });
    }

    @Test
    public void check_a_module_with_params_auto() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        org.koin.core.KoinApplication koinApplication = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                Module module = org.koin.dsl.Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(Module module) {
                        try {
                            module.single(Components.Simple.MyString.class, new Definition<Components.Simple.MyString>() {
                                @Override
                                public Components.Simple.MyString invoke(Scope scope, DefinitionParameters parameters) {
                                    String s = null;
                                    return new Components.Simple.MyString(s);
                                }
                            });

                            module.single(Components.Simple.ComponentB.class, new Definition<Components.Simple.ComponentB>() {
                                @Override
                                public Components.Simple.ComponentB invoke(Scope scope, DefinitionParameters parameters) {
                                    Components.Simple.ComponentA a = new Components.Simple.ComponentA();
                                    return new Components.Simple.ComponentB(a);
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });
        checkModules(koinApplication, null);
    }

    @Test
    public void check_a_module_with_params_default_value() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        String id = "_ID_";
        final String[] injectedValue = {null};
        org.koin.core.KoinApplication koinApplication = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                Module module = org.koin.dsl.Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(Module module) {
                        try {
                            module.single(Components.Simple.MyString.class, new Definition<Components.Simple.MyString>() {
                                @Override
                                public Components.Simple.MyString invoke(Scope scope, DefinitionParameters parameters) {
                                    String s = null;
                                    injectedValue[0] = s;
                                    return new Components.Simple.MyString(s);
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });
        checkModules(koinApplication, new CheckParameters() {
            @Override
            public void invoke(ParameterBinding parameterBinding) {
                parameterBinding.defaultValue(String.class, id);
            }
        });

        Assert.assertNotEquals(injectedValue[0], id);
    }

    @Test
    public void check_a_module_with_params_added_default_value_in_graph() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        String id = "_ID_";
        final String[] _id = {""};
        org.koin.core.KoinApplication app = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                Module module = org.koin.dsl.Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(Module module) {
                        try {
                            module.single(Components.Simple.MyString.class, new Definition<Components.Simple.MyString>() {
                                @Override
                                public Components.Simple.MyString invoke(Scope scope, DefinitionParameters parameters) {
                                    try {
                                        _id[0] = scope.get(Components.Simple.MyString.class);
                                        return new Components.Simple.MyString(_id[0]);
                                    } catch (ClosedScopeException e) {
                                        e.printStackTrace();
                                    } catch (DefinitionParameterException e) {
                                        e.printStackTrace();
                                    } catch (NoBeanDefFoundException e) {
                                        e.printStackTrace();
                                    }
                                    return null;
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });
        checkModules(app, new CheckParameters() {
            @Override
            public void invoke(ParameterBinding parameterBinding) {
                parameterBinding.defaultValue(String.class, id);
            }
        });
        Assert.assertNotEquals(id, _id[0]);
    }

    @Test
    public void check_a_module_with_params_default_value_in_graph() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        final String[] _value = {null};
        org.koin.core.KoinApplication app = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                Module module = org.koin.dsl.Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(Module module) {
                        try {
                            module.single(Components.Simple.MyString.class, new Definition<Components.Simple.MyString>() {
                                @Override
                                public Components.Simple.MyString invoke(Scope scope, DefinitionParameters parameters) {
                                    try {
                                        _value[0] = scope.get(String.class);
                                    } catch (ClosedScopeException e) {
                                        e.printStackTrace();
                                    } catch (DefinitionParameterException e) {
                                        e.printStackTrace();
                                    } catch (NoBeanDefFoundException e) {
                                        e.printStackTrace();
                                    }
                                    return new Components.Simple.MyString(_value[0]);
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });
        checkModules(app, null);

        Assert.assertNotEquals(_value[0], "");
    }

    @Test
    public void check_a_module_with_complex_params_default_mocked_value_in_graph() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        final Components.Simple.ComponentA[] _a = {null};
        org.koin.core.KoinApplication app = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                Module module = org.koin.dsl.Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(Module module) {
                        try {
                            module.single(Components.Simple.ComponentB.class, new Definition<Components.Simple.ComponentB>() {
                                @Override
                                public Components.Simple.ComponentB invoke(Scope scope, DefinitionParameters parameters) {
                                    try {
                                        _a[0] = scope.get(String.class);
                                        if (_a[0] == null) {
                                            throw new NullPointerException();
                                        }
                                    } catch (ClosedScopeException e) {
                                        e.printStackTrace();
                                    } catch (DefinitionParameterException e) {
                                        e.printStackTrace();
                                    } catch (NoBeanDefFoundException e) {
                                        e.printStackTrace();
                                    }
                                    return new Components.Simple.ComponentB(_a[0]);
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });
        checkModules(app, new CheckParameters() {
            @Override
            public void invoke(ParameterBinding parameterBinding) {
                parameterBinding.defaultValue(Components.Simple.ComponentA.class, new Components.Simple.ComponentA());
            }
        });

        Assert.assertNotNull(_a);
    }

    @Test
    public void check_a_module_with_params_default_value_object() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        Components.Simple.ComponentA a = new Components.Simple.ComponentA();
        final Components.Simple.ComponentA[] injectedValue = {null};
        org.koin.core.KoinApplication koinApplication = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                Module module = org.koin.dsl.Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(Module module) {
                        try {
                            module.single(Components.Simple.ComponentB.class, new Definition<Components.Simple.ComponentB>() {
                                @Override
                                public Components.Simple.ComponentB invoke(Scope scope, DefinitionParameters parameters) {
                                    injectedValue[0] = a;
                                    return new Components.Simple.ComponentB(a);
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });
        checkModules(koinApplication, new CheckParameters() {
            @Override
            public void invoke(ParameterBinding parameterBinding) {
                parameterBinding.defaultValue(a.getClass(), a);
            }
        });

        Assert.assertNotEquals(injectedValue[0], a);
    }

    @Test
    public void check_a_module_with_params_auto_scope() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        org.koin.core.KoinApplication koinApplication = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                Module module = org.koin.dsl.Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(Module module) {
                        module.scope(Components.Simple.ComponentA.class, new Module.ScopeSet() {
                            @Override
                            public void invoke(ScopeDSL scopeDSL) {
                                try {
                                    scopeDSL.scoped(Components.Simple.ComponentB.class, new Definition<Components.Simple.ComponentB>() {
                                        @Override
                                        public Components.Simple.ComponentB invoke(Scope scope, DefinitionParameters parameters) {
                                            try {
                                                return new Components.Simple.ComponentB(scope.get(Components.Simple.ComponentA.class));
                                            } catch (ClosedScopeException e) {
                                                e.printStackTrace();
                                            } catch (DefinitionParameterException e) {
                                                e.printStackTrace();
                                            } catch (NoBeanDefFoundException e) {
                                                e.printStackTrace();
                                            }
                                            return null;
                                        }
                                    });
                                } catch (DefinitionOverrideException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                });
            }
        });
        checkModules(koinApplication, null);
    }

    @Test
    public void check_a_module_with_params_auto_scope_error() {
        try {
            org.koin.core.KoinApplication koinApplication = KoinApplication.koinApplication(new KoinAppDeclaration() {
                @Override
                public void invoke(org.koin.core.KoinApplication koinApplication) {
                    koinApplication.printLogger(Level.DEBUG);
                    Module module = org.koin.dsl.Module.module(new ModuleDeclaration() {
                        @Override
                        public void invoke(Module module) {
                            module.scope(Components.Simple.ComponentA.class, new Module.ScopeSet() {
                                @Override
                                public void invoke(ScopeDSL scopeDSL) {
                                    try {
                                        scopeDSL.scoped(Components.Simple.ComponentC.class, new Definition<Components.Simple.ComponentC>() {
                                            @Override
                                            public Components.Simple.ComponentC invoke(Scope scope, DefinitionParameters parameters) {
                                                try {
                                                    return new Components.Simple.ComponentC(scope.get(Components.Simple.ComponentB.class));
                                                } catch (ClosedScopeException e) {
                                                    e.printStackTrace();
                                                } catch (DefinitionParameterException e) {
                                                    e.printStackTrace();
                                                } catch (NoBeanDefFoundException e) {
                                                    e.printStackTrace();
                                                }
                                                return null;
                                            }
                                        });
                                    } catch (DefinitionOverrideException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        }
                    });
                    try {
                        koinApplication.modules(module);
                    } catch (DefinitionOverrideException e) {
                        e.printStackTrace();
                    }
                }
            });
            checkModules(koinApplication, null);
            fail();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void check_with_qualifier() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        org.koin.core.KoinApplication koinApplication = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                Module module = org.koin.dsl.Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(Module module) {
                        try {
                            module.single(Components.Simple.ComponentA.class, Qualifier.named("test"), new Definition<Components.Simple.ComponentA>() {
                                @Override
                                public Components.Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Components.Simple.ComponentA();
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });
        checkModules(koinApplication, null);
    }

    @Test
    public void check_a_module_with_property() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        org.koin.core.KoinApplication koinApplication = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                HashMap<String, String> map = new HashMap<>();
                map.put("aValue", "value");
                koinApplication.properties(map);
                Module module = org.koin.dsl.Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(Module module) {
                        try {
                            module.single(Components.Simple.MyString.class, new Definition<Components.Simple.MyString>() {
                                @Override
                                public Components.Simple.MyString invoke(Scope scope, DefinitionParameters parameters) {
                                    try {
                                        return new Components.Simple.MyString(scope.getProperty("aValue"));
                                    } catch (MissingPropertyException e) {
                                        e.printStackTrace();
                                    }
                                    return null;
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });
        checkModules(koinApplication, null);
    }
}
