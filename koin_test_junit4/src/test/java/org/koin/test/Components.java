package org.koin.test;

import org.koin.core.qualifier.Qualifier;

import java.util.UUID;

public class Components {

    public static class Simple {

        public static class ComponentA {
        }

        public static class ComponentB {
            private Simple.ComponentA a;

            public ComponentB(Simple.ComponentA a) {
                this.a = a;
            }
        }

        public static class ComponentC {
            private Simple.ComponentB b;

            public ComponentC(Simple.ComponentB b) {
                this.b = b;
            }
        }

        public static class MyString {
            public String s;

            public MyString(String s) {
                this.s = s;
            }
        }

        public static class UUIDComponent {
            public String getUUID() {
                return UUID.randomUUID().toString();
            }
        }
    }

}


class UpperCase extends Qualifier {

    public static volatile UpperCase INSTANCE;
    private String value = "UpperCase";

    public static UpperCase INSTANCE() {
        if (INSTANCE == null) {
            synchronized (UpperCase.class) {
                if (INSTANCE == null) {
                    INSTANCE = new UpperCase();
                }
            }
        }
        return INSTANCE;
    }
}