package org.koin.test;

import org.junit.Assert;
import org.junit.Test;
import org.koin.core.KoinApplication;
import org.koin.core.definition.Definition;
import org.koin.core.error.*;
import org.koin.core.logger.Level;
import org.koin.core.parameter.DefinitionParameters;
import org.koin.core.parameter.ParametersDefinition;
import org.koin.core.qualifier.Qualifier;
import org.koin.core.scope.Scope;
import org.koin.dsl.KoinAppDeclaration;
import org.koin.dsl.Module;
import org.koin.dsl.ModuleDeclaration;
import org.koin.java.KoinJavaComponent;
import org.koin.test.mock.DeclareMock;
import org.koin.test.mock.MockProviderRule;
import org.koin.test.mock.Provider;
import org.koin.test.mock.StubFunction;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;

import static org.koin.core.context.DefaultContextExt.startKoin;

public class DeclareMockFromKoinTest extends AutoCloseKoinTest {

    private MockProviderRule mockProvider = MockProviderRule.create(new Provider() {
        @Override
        public Object invoke(Class clazz) {
            return Mockito.mock(clazz);
        }
    });

    private Components.Simple.UUIDComponent mock;

    @Test
    public void declareMock_with_KoinTest() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, KoinAppAlreadyStartedException {
        startKoin(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                try {
                    koinApplication.modules(Module.module(new ModuleDeclaration() {
                        @Override
                        public void invoke(org.koin.core.module.Module module) {
                            try {
                                module.single(Components.Simple.UUIDComponent.class, new Definition<Components.Simple.UUIDComponent>() {
                                    @Override
                                    public Components.Simple.UUIDComponent invoke(Scope scope, DefinitionParameters parameters) {
                                        return new Components.Simple.UUIDComponent();
                                    }
                                });
                            } catch (DefinitionOverrideException e) {
                                e.printStackTrace();
                            }
                        }
                    }));
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        String uuidValue = "UUID";

        //TODO 2021/6/17 java.lang.IllegalStateException: Missing MockProvider. Please use MockProvider.register() to register a new mock provider
        DeclareMock.declareMock(new KoinTest() {
            @Override
            public <T> T get(Class<T> clazz, KoinTest koinTest, Qualifier qualifier, ParametersDefinition parameters) {
                return null;
            }
        }, Components.Simple.UUIDComponent.class, null, new ArrayList<>(), new StubFunction<Components.Simple.UUIDComponent>() {
            @Override
            public void invoke(Components.Simple.UUIDComponent uuidComponent) {
                BDDMockito.given(uuidComponent.getUUID()).will(new Answer<String>() {
                    @Override
                    public String answer(InvocationOnMock invocation) throws Throwable {
                        return uuidValue;
                    }
                });
            }
        });

        mock = KoinJavaComponent.inject(Components.Simple.UUIDComponent.class);
        Assert.assertEquals(uuidValue, mock.getUUID());

    }

    @Test
    public void declareMock_factory_with_KoinTest() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, KoinAppAlreadyStartedException {
        startKoin(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.factory(Components.Simple.UUIDComponent.class, new Definition<Components.Simple.UUIDComponent>() {
                                @Override
                                public Components.Simple.UUIDComponent invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Components.Simple.UUIDComponent();
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });


        try {
            String val1 = getKoin().get(Components.Simple.UUIDComponent.class).getUUID();
            String val2 = getKoin().get(Components.Simple.UUIDComponent.class).getUUID();

            Assert.assertNotEquals(val1, val2);
        } catch (DefinitionParameterException e) {
            e.printStackTrace();
        } catch (NoBeanDefFoundException e) {
            e.printStackTrace();
        } catch (ClosedScopeException e) {
            e.printStackTrace();
        }
    }

}
