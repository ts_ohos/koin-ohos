package org.koin.test;

import org.junit.After;
import org.koin.core.context.DefaultContextExt;

public interface ClosingKoinTest extends KoinTest {

    @After
    public static void autoClose() {
        DefaultContextExt.stopKoin();
    }
}
