package org.koin.test;

import org.junit.After;
import org.koin.core.context.DefaultContextExt;

public abstract class AutoCloseKoinTest implements KoinTest {

    @After
    public void autoClose() {
        DefaultContextExt.stopKoin();
    }
}
