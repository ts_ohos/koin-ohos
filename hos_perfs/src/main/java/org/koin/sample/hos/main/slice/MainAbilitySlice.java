package org.koin.sample.hos.main.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import org.koin.core.Koin;
import org.koin.core.KoinApplication;
import org.koin.core.error.DefinitionOverrideException;
import org.koin.dsl.KoinAppDeclaration;
import org.koin.ext.Pair;
import org.koin.hos.logger.HosLogger;
import org.koin.sample.hos.main.Module400;
import org.koin.sample.hos.main.Perfs;
import org.koin.sample.hos.main.ResourceTable;
import java.text.DecimalFormat;

public class MainAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        new Thread(new Runnable() {
            @Override
            public void run() {
                int size = 10;
                long start = 0;
                long exec = 0;
                for (int i = 0; i < size; i++) {
                    Pair<Long, Long> longLongPair = runPerf(i);
                    start += longLongPair.getFirst();
                    exec += longLongPair.getSecond();
                }
                DecimalFormat to = new DecimalFormat("0.0000");
                String avgExec = to.format(exec / size / 1000000.0d);
                String avgStart = to.format(start / size / 1000000.0d);
                System.out.println("Avg start time: " + avgStart);
                System.out.println("Avg execution time: " + avgExec);
            }
        }).start();
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    private Pair<Long, Long> runPerf(int count) {

        KoinApplication app = null;

        try {
            long start = System.nanoTime();
            app = org.koin.dsl.KoinApplication.koinApplication(new KoinAppDeclaration() {
                @Override
                public void invoke(KoinApplication koinApplication) {
                    try {
                        koinApplication.logger(new HosLogger());
                        koinApplication.modules(Module400.perfModule400);
                    } catch (DefinitionOverrideException e) {
                        e.printStackTrace();
                    }
                }
            });
            long end = System.nanoTime();
            DecimalFormat to = new DecimalFormat("0.0000");
            String duration = to.format((end - start) / 1000000.0);
            System.out.println("[" + count + "] started in " + duration + "ms");

            Koin koin = app.getKoin();

            long start1 = System.nanoTime();
            koin.get(Perfs.A27.class);
            koin.get(Perfs.A31.class);
            koin.get(Perfs.A12.class);
            koin.get(Perfs.A42.class);
            long end1 = System.nanoTime();
            String executionDuration = to.format((end1 - start1) / 1000000.0);
            System.out.println("[" + count + "] measured executed in " + executionDuration + "ms");

            return new Pair<>(end - start, end1 - start1);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (app != null) {
                app.close();
            }
        }

        return new Pair(-1, -1);
    }
}
