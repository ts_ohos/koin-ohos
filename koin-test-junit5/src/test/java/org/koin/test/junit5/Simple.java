package org.koin.test.junit5;

import java.util.UUID;

public class Simple {

    public static class ComponentA {

    }

    public static class ComponentB {
        public ComponentA a;

        public ComponentB(ComponentA componentA) {
            this.a = componentA;
        }
    }

    public static class MyString {
        public String s;

        public MyString(String str) {
            s = str;
        }
    }

    public static class UUIDComponent {
        public String getUUID() {
            return UUID.randomUUID().toString();
        }
    }
}
