package org.koin.test.junit5;

import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.koin.core.Koin;
import org.koin.core.definition.Definition;
import org.koin.core.error.DefinitionOverrideException;
import org.koin.core.error.KoinAppAlreadyStartedException;
import org.koin.core.error.NoScopeDefFoundException;
import org.koin.core.error.ScopeAlreadyCreatedException;
import org.koin.core.logger.Level;
import org.koin.core.parameter.DefinitionParameters;
import org.koin.core.scope.Scope;
import org.koin.dsl.Module;
import org.koin.dsl.ModuleDeclaration;
import org.koin.test.junit5.mock.MockProviderExtension;
import org.koin.test.mock.DeclareMock;
import org.koin.test.mock.Provider;
import org.koin.test.mock.StubFunction;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.UUID;

import static org.koin.core.context.DefaultContextExt.startKoin;

public class DeclareMockFromKoinTest extends AutoCloseKoinTest {

    @RegisterExtension
    public MockProviderExtension mockProviderExtension = MockProviderExtension.INSTANCE(new Provider() {
        @Override
        public Object invoke(Class clazz) {
            return Mockito.mock(Provider.class);

        }
    });

    private Simple.UUIDComponent mock = new Simple.UUIDComponent();

    @Test
    @DisplayName("declareMock with koinTest")
    public void declaresMockForTestingContext() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, KoinAppAlreadyStartedException {
        org.koin.core.KoinApplication app = startKoin(new org.koin.dsl.KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                try {
                    koinApplication.modules(Module.module(new ModuleDeclaration() {
                        @Override
                        public void invoke(org.koin.core.module.Module module) {
                            try {
                                module.single(Simple.UUIDComponent.class, new Definition<Simple.UUIDComponent>() {
                                    @Override
                                    public Simple.UUIDComponent invoke(Scope scope, DefinitionParameters parameters) {
                                        return new Simple.UUIDComponent();
                                    }
                                });
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }));
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        String uuidValue = "UUID";
        Koin koin = app.getKoin();
        DeclareMock declareMock = new DeclareMock();
        //TODO 提示错误：java.lang.IllegalStateException: Missing MockProvider. Please use MockProvider.register() to register a new mock provider
        declareMock.declareMock(koin, Simple.UUIDComponent.class, null, new ArrayList<>(), new StubFunction<Simple.UUIDComponent>() {
            @Override
            public void invoke(Simple.UUIDComponent uuidComponent) {
                BDDMockito.given(new Simple.UUIDComponent().getUUID()).will(new Answer<String>() {
                    @Override
                    public String answer(InvocationOnMock invocation) throws Throwable {
                        return uuidValue;
                    }
                });
            }
        });
        Assert.assertEquals(uuidValue, mock.getUUID());
    }

    @Test
    @DisplayName("declareMock factory with KoinTest")
    public void declareMockWhenUsingFactory() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, KoinAppAlreadyStartedException {
        org.koin.core.KoinApplication app = startKoin(new org.koin.dsl.KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                try {
                    koinApplication.modules(Module.module(new ModuleDeclaration() {
                        @Override
                        public void invoke(org.koin.core.module.Module module) {
                            try {
                                module.factory(Simple.UUIDComponent.class, new Definition<Simple.UUIDComponent>() {
                                    @Override
                                    public Simple.UUIDComponent invoke(Scope scope, DefinitionParameters parameters) {
                                        return new Simple.UUIDComponent();
                                    }
                                });
                            } catch (DefinitionOverrideException e) {
                                e.printStackTrace();
                            }
                        }
                    }));
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        DeclareMock declareMock = new DeclareMock();
        //TODO 提示错误：java.lang.IllegalStateException: Missing MockProvider. Please use MockProvider.register() to register a new mock provider
        declareMock.declareMock(app.getKoin(), Simple.UUIDComponent.class, null, new ArrayList<>(), new StubFunction<Simple.UUIDComponent>() {
            @Override
            public void invoke(Simple.UUIDComponent uuidComponent) {
                BDDMockito.given(new Simple.UUIDComponent()).will(new Answer<Object>() {
                    @Override
                    public String answer(InvocationOnMock invocation) throws Throwable {
                        return UUID.randomUUID().toString();
                    }
                });
            }
        });
        try {
            Simple.UUIDComponent uuidComponent1 = app.getKoin().get(Simple.UUIDComponent.class);
            String str1 = uuidComponent1.getUUID();
            Simple.UUIDComponent uuidComponent2 = app.getKoin().get(Simple.UUIDComponent.class);
            String str2 = uuidComponent2.getUUID();
            Assert.assertEquals(str1, str2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
