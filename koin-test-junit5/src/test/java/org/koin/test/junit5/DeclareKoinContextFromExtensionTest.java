package org.koin.test.junit5;

import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.koin.core.Koin;
import org.koin.core.KoinAppDeclaration;
import org.koin.core.KoinApplication;
import org.koin.core.definition.Definition;
import org.koin.core.error.DefinitionOverrideException;
import org.koin.core.logger.Level;
import org.koin.core.parameter.DefinitionParameters;
import org.koin.core.scope.Scope;
import org.koin.dsl.Module;
import org.koin.dsl.ModuleDeclaration;
import org.koin.test.KoinTest;

public class DeclareKoinContextFromExtensionTest implements KoinTest {

    KoinAppDeclaration koinAppDeclaration = new KoinAppDeclaration() {
        @Override
        public void invoke(KoinApplication koinApplication) {
            koinApplication.printLogger(Level.DEBUG);
            try {
                koinApplication.modules(Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.single(Simple.ComponentA.class, new Definition<Simple.ComponentA>() {
                                @Override
                                public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Simple.ComponentA();
                                }
                            });
                            module.single(Simple.MyString.class, new Definition<Simple.MyString>() {
                                @Override
                                public Simple.MyString invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Simple.MyString("simple test");
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                }));
            } catch (DefinitionOverrideException e) {
                e.printStackTrace();
            }
        }
    };
    @RegisterExtension
    KoinTestExtension koinTestExtension = KoinTestExtension.INSTANCE(koinAppDeclaration);

    @Test
    @DisplayName("component can be fetched from koin context")
    public void whenMockWithQualifier(Koin koin) {
        Simple.MyString myString = null;
        try {
            myString = koin.get(Simple.MyString.class);
            Assert.assertEquals(myString.s, "simple test");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //TODO 因org.koin.test.KoinTest 类中inject，方法移植未完成（hos无法使用kotlin库Lazy;）所以 whenMockWithQualifier（）方法未移植

}
