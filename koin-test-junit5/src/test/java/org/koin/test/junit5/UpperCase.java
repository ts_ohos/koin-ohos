package org.koin.test.junit5;

import org.koin.core.qualifier.Qualifier;

public class UpperCase extends Qualifier {

    public final static String value = "UpperCase";

}
