package org.koin.test.junit5;

import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.koin.core.Koin;
import org.koin.core.KoinApplication;
import org.koin.core.definition.Definition;
import org.koin.core.error.DefinitionOverrideException;
import org.koin.core.error.NoScopeDefFoundException;
import org.koin.core.error.ScopeAlreadyCreatedException;
import org.koin.core.logger.Level;
import org.koin.core.parameter.DefinitionParameters;
import org.koin.core.qualifier.Qualifier;
import org.koin.core.scope.Scope;
import org.koin.dsl.KoinAppDeclaration;
import org.koin.dsl.Module;
import org.koin.dsl.ModuleDeclaration;
import org.koin.dsl.ScopeDSL;
import org.koin.test.junit5.Simple.ComponentA;
import org.koin.test.junit5.Simple.UUIDComponent;
import org.koin.test.junit5.mock.MockProviderExtension;
import org.koin.test.mock.DeclareMock;
import org.koin.test.mock.Provider;
import org.koin.test.mock.StubFunction;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;

public class DeclareMockTests extends AutoCloseKoinTest {

    @RegisterExtension
    MockProviderExtension provider = MockProviderExtension.INSTANCE(new Provider() {
        @Override
        public Object invoke(Class clazz) {
            return Mockito.mock(Provider.class);
        }
    });

    @Test
    @DisplayName("declare and stub an existing definition")
    public void whenStubbingExistingDefinition() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        org.koin.core.KoinApplication app = org.koin.dsl.KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                try {
                    koinApplication.modules(Module.module(new ModuleDeclaration() {
                        @Override
                        public void invoke(org.koin.core.module.Module module) {
                            try {
                                module.single(UUIDComponent.class, new Definition<UUIDComponent>() {
                                    @Override
                                    public UUIDComponent invoke(Scope scope, DefinitionParameters parameters) {
                                        return new UUIDComponent();
                                    }
                                });
                            } catch (DefinitionOverrideException e) {
                                e.printStackTrace();
                            }
                        }
                    }));
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });
        try {
            Koin koin = app.getKoin();
            UUIDComponent instance = koin.get(UUIDComponent.class);
            String uuidValue = "UUID";
            DeclareMock declareMock = new DeclareMock();
            declareMock.declareMock(koin, UUIDComponent.class, null, new ArrayList<>(), new StubFunction<UUIDComponent>() {
                @Override
                public void invoke(UUIDComponent uuidComponent) {
                    BDDMockito.given(new UUIDComponent().getUUID()).will(new Answer<String>() {
                        @Override
                        public String answer(InvocationOnMock invocation) throws Throwable {
                            return uuidValue;
                        }
                    });
                }
            });
            UUIDComponent mock = koin.get(UUIDComponent.class);
            Assert.assertEquals(instance, mock);
            Assert.assertEquals(uuidValue, mock.getUUID());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    @DisplayName("declare and stub an existing scoped definition")
    public void whenStubbingExistingScopedDefinition() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        org.koin.core.KoinApplication app = org.koin.dsl.KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                try {
                    koinApplication.modules(Module.module(new ModuleDeclaration() {
                        @Override
                        public void invoke(org.koin.core.module.Module module) {
                            try {
                                module.scope(Qualifier.named(Simple.class), new org.koin.core.module.Module.ScopeSet() {
                                    @Override
                                    public void invoke(ScopeDSL scopeDSL) {
                                        try {
                                            scopeDSL.scoped(UUIDComponent.class, new Definition<UUIDComponent>() {
                                                @Override
                                                public UUIDComponent invoke(Scope scope, DefinitionParameters parameters) {
                                                    return new UUIDComponent();
                                                }
                                            });
                                        } catch (DefinitionOverrideException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }));
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });
        try {
            Koin koin = app.getKoin();
            Scope scope = koin.getOrCreateScope("scope_id", Qualifier.named(Simple.class));
            UUIDComponent instance = scope.get(UUIDComponent.class);
            String uuidValue = "UUID";
            DeclareMock declareMock = new DeclareMock();
            declareMock.declareMock(koin, UUIDComponent.class, null, new ArrayList<>(), new StubFunction<UUIDComponent>() {
                @Override
                public void invoke(UUIDComponent uuidComponent) {
                    BDDMockito.given(new UUIDComponent().getUUID()).will(new Answer<String>() {
                        @Override
                        public String answer(InvocationOnMock invocation) throws Throwable {
                            return uuidValue;
                        }
                    });
                }
            });
            UUIDComponent mock = scope.get(UUIDComponent.class);
            Assert.assertEquals(instance, mock);
            Assert.assertEquals(uuidValue, mock.getUUID());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    @DisplayName("declare a Mock of an existing definition for given scope")
    public void whenDeclaringMockForExistingDefinitionForGivenScope() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {

        org.koin.core.KoinApplication app = org.koin.dsl.KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {
                try {
                    koinApplication.modules(Module.module(new ModuleDeclaration() {
                        @Override
                        public void invoke(org.koin.core.module.Module module) {
                            module.scope(Qualifier.named(Simple.class), new org.koin.core.module.Module.ScopeSet() {
                                @Override
                                public void invoke(ScopeDSL scopeDSL) {
                                    try {
                                        scopeDSL.scoped(ComponentA.class, new Definition<ComponentA>() {
                                            @Override
                                            public ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                                return new ComponentA();
                                            }
                                        });
                                    } catch (DefinitionOverrideException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        }
                    }));
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });
        try {
            Koin koin = app.getKoin();
            Scope scope = koin.getOrCreateScope("scope_id", Qualifier.named(Simple.class));
            ComponentA instance = scope.get(ComponentA.class);
            scope.declare(ComponentA.class, null, null);
            ComponentA mock = scope.get(ComponentA.class);
            Assert.assertNotEquals(instance, mock);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    @DisplayName("declare and mock an existing definition for given scope")
    public void whenDeclaringAndMockingAnExistingDefinitionForGivenScope() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        Koin koin = org.koin.dsl.KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {
                try {
                    koinApplication.modules(Module.module(new ModuleDeclaration() {
                        @Override
                        public void invoke(org.koin.core.module.Module module) {
                            module.scope(Simple.class, new org.koin.core.module.Module.ScopeSet() {
                                @Override
                                public void invoke(ScopeDSL scopeDSL) {
                                    try {
                                        scopeDSL.scoped(UUIDComponent.class, new Definition<UUIDComponent>() {
                                            @Override
                                            public UUIDComponent invoke(Scope scope, DefinitionParameters parameters) {
                                                return new UUIDComponent();
                                            }
                                        });
                                    } catch (DefinitionOverrideException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        }
                    }));
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();
        try {
            Scope scope = koin.getOrCreateScope("scope_id", Qualifier.named(Simple.class));
            UUIDComponent instance = scope.get(UUIDComponent.class);
            String uuidValue = "UUID";
            DeclareMock declareMock = new DeclareMock();
            declareMock.declareMock(koin, UUIDComponent.class, null, new ArrayList<>(), new StubFunction<UUIDComponent>() {
                @Override
                public void invoke(UUIDComponent uuidComponent) {
                    BDDMockito.given(new UUIDComponent().getUUID()).will(new Answer<String>() {
                        @Override
                        public String answer(InvocationOnMock invocation) throws Throwable {
                            return uuidValue;
                        }
                    });
                }
            });
            UUIDComponent mock = scope.get(UUIDComponent.class);
            Assert.assertNotEquals(instance, mock);
            Assert.assertEquals(uuidValue, mock.getUUID());

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    @DisplayName("declare a Mock of an existing definition")
    public void whenDecralingMockOfAnExistingDefinition() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        Koin koin = org.koin.dsl.KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                try {
                    koinApplication.modules(Module.module(new ModuleDeclaration() {
                        @Override
                        public void invoke(org.koin.core.module.Module module) {
                            try {
                                module.single(ComponentA.class, new Definition<ComponentA>() {
                                    @Override
                                    public ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                        return new ComponentA();
                                    }
                                });
                            } catch (DefinitionOverrideException e) {
                                e.printStackTrace();
                            }
                        }
                    }));
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();
        try {
            ComponentA instance = koin.get(ComponentA.class);
            koin.declare(ComponentA.class, null);
            ComponentA mock = koin.get(ComponentA.class);
            Assert.assertNotEquals(instance, mock);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    @DisplayName("declare and mock an existing definition")
    public void wheDeclaringAndMockingExistingDefinition() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        Koin koin = org.koin.dsl.KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                try {
                    koinApplication.modules(Module.module(new ModuleDeclaration() {
                        @Override
                        public void invoke(org.koin.core.module.Module module) {
                            try {
                                module.single(UUIDComponent.class, new Definition<UUIDComponent>() {
                                    @Override
                                    public UUIDComponent invoke(Scope scope, DefinitionParameters parameters) {
                                        return new UUIDComponent();
                                    }
                                });
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();
        try {
            UUIDComponent instance = koin.get(UUIDComponent.class);
            String uuidValue = "UUID";
            DeclareMock declareMock = new DeclareMock();
            declareMock.declareMock(koin, UUIDComponent.class, null, new ArrayList<>(), new StubFunction<UUIDComponent>() {
                @Override
                public void invoke(UUIDComponent uuidComponent) {
                    BDDMockito.given(new UUIDComponent().getUUID()).will(new Answer<String>() {
                        @Override
                        public String answer(InvocationOnMock invocation) throws Throwable {
                            return uuidValue;
                        }
                    });
                }
            });

            UUIDComponent mock = koin.get(UUIDComponent.class);
            Assert.assertNotEquals(instance, mock);
            Assert.assertEquals(uuidValue, mock.getUUID());

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    @DisplayName("mock with qualifier")
    public void whenMockWithQualifier() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        Koin koin = org.koin.dsl.KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                try {
                    koinApplication.modules(Module.module(new ModuleDeclaration() {
                        @Override
                        public void invoke(org.koin.core.module.Module module) {
                            try {
                                module.single(ComponentA.class, Qualifier.named("test"), new Definition<ComponentA>() {
                                    @Override
                                    public ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                        return new ComponentA();
                                    }
                                });
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();
        try {
            koin.declare(ComponentA.class, null, Qualifier.named("test"), null, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
