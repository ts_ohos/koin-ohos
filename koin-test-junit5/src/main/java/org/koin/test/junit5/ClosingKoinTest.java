package org.koin.test.junit5;

import org.junit.jupiter.api.AfterEach;
import org.koin.core.context.DefaultContextExt;
import org.koin.test.KoinTest;

public interface ClosingKoinTest extends KoinTest {
    @AfterEach
    public default void autoClose() {
        DefaultContextExt.stopKoin();
    }
}
