package org.koin.test.junit5.mock;

import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.koin.test.mock.MockProvider;
import org.koin.test.mock.Provider;


public class MockProviderExtension implements BeforeEachCallback {

    public static volatile MockProviderExtension MOCKINSTANCE;

    private Provider mockProvider;

    private MockProviderExtension(Provider provider) {
        this.mockProvider = provider;
    }

    public static MockProviderExtension INSTANCE(Provider provider) {
        if (null == MOCKINSTANCE) {
            synchronized (MockProviderExtension.class) {
                if (null == MOCKINSTANCE) {
                    MOCKINSTANCE = new MockProviderExtension(provider);
                }
            }
        }
        return MOCKINSTANCE;
    }


    @Override
    public void beforeEach(ExtensionContext context) throws Exception {
        MockProvider.INSTANCE().register(mockProvider);
    }
}
