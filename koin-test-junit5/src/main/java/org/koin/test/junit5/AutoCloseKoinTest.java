package org.koin.test.junit5;

import org.junit.jupiter.api.AfterEach;
import org.koin.core.context.DefaultContextExt;
import org.koin.test.KoinTest;

public abstract class AutoCloseKoinTest implements KoinTest {

    @AfterEach
    public void autoClose() {
        DefaultContextExt.stopKoin();
    }
}
