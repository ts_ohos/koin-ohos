package org.koin.hos.java;

import ohos.app.Context;
import org.koin.core.KoinApplication;
import org.koin.core.error.NoScopeDefFoundException;
import org.koin.core.error.ScopeAlreadyCreatedException;
import org.koin.core.logger.Level;
import org.koin.hos.ext.koin.KoinExt;

/**
 * Java Compat utility
 */
public class KoinHosApplication {

    /**
     * Create Koin Application with Hos context - For Java compat
     */
    public static KoinApplication create(Context context, Level HosLoggerLevel) {
        try {
            KoinApplication app = KoinApplication.init();
            KoinExt.hosContext(app, context);
            KoinExt.HosLogger(app, HosLoggerLevel);
            return app;
        } catch (ScopeAlreadyCreatedException e) {
            e.printStackTrace();
        } catch (NoScopeDefFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Create Koin Application with Hos context - For Java compat
     */
    public static KoinApplication create(Context context) {
        return create(context, Level.INFO);
    }
}
