package org.koin.hos.logger;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import org.koin.core.logger.Level;
import org.koin.core.logger.Logger;

public class HosLogger extends Logger {

    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, KOIN_TAG);

    public HosLogger(Level level) {
        super(level);
    }

    public HosLogger() {
        super(Level.INFO);
    }

    @Override
    public void log(Level level, String msg) {
        if (getLevel().ordinal() <= level.ordinal()) {
            logOnLevel(msg, level);
        }
    }

    private void logOnLevel(String msg, Level level) {
        switch (level) {
            case DEBUG:
                HiLog.debug(LABEL, msg);
                break;
            case INFO:
                HiLog.info(LABEL, msg);
                break;
            case ERROR:
                HiLog.error(LABEL, msg);
                break;
            default:
                HiLog.error(LABEL, msg);
                break;
        }
    }
}
