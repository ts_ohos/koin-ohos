package org.koin.hos.scope;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import org.koin.core.scope.Scope;
import org.koin.hos.ext.hos.AbilityExt;

public class ScopeAbility extends Ability implements
        HosScopeComponent {

    private boolean initialiseScope = true;

    @Override
    public Scope getScope() {
        return AbilityExt.abilityScope(this);
    }

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        if (initialiseScope) {
            getScope().getLogger().debug("Open Ability Scope: " + getScope());
        }
    }

    public void setInitialiseScope(boolean initialiseScope) {
        this.initialiseScope = initialiseScope;
    }
}
