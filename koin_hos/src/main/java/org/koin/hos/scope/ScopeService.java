package org.koin.hos.scope;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import org.koin.core.scope.Scope;

public class ScopeService extends Ability implements
        HosScopeComponent {

    private boolean initialiseScope = false;

    @Override
    public Scope getScope() {
        return ServiceExt.serviceScope(this);
    }

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        if (initialiseScope) {
            getScope().getLogger().debug("Open Service Scope: " + getScope());
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (initialiseScope) {
            getScope().getLogger().debug("Close Service Scope: " + getScope());
        }
    }


    public void setInitialiseScope(boolean initialiseScope) {
        this.initialiseScope = initialiseScope;
    }
}
