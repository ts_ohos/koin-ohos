package org.koin.hos.scope;

import org.koin.core.scope.Scope;

public interface HosScopeComponent {

    Scope getScope();
}
