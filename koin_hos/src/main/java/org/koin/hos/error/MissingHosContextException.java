package org.koin.hos.error;

public class MissingHosContextException extends Throwable {

    public MissingHosContextException(String message) {
        super(message);
    }
}
