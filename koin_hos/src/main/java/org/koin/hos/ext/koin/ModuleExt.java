package org.koin.hos.ext.koin;

import ohos.aafwk.ability.AbilityPackage;
import ohos.app.Context;
import org.koin.core.scope.Scope;
import org.koin.hos.error.MissingHosContextException;

public class ModuleExt {

    public static final String ERROR_MSG
            = "Please use HosContext() function in your KoinApplication configuration.";

    /**
     * DSL extension - Resolve Hos Context instance
     */
    public static Context hosContext(Scope scope) throws MissingHosContextException {
        Context context = null;
        try {
            context = scope.get(Context.class);
        } catch (Exception e) {
            throw new MissingHosContextException("Can't resolve Context instance. "
                    + ERROR_MSG);
        }
        return context;
    }

    /**
     * DSL extension - Resolve Hos Context instance
     */
    public static AbilityPackage hosApplication(Scope scope)
            throws MissingHosContextException {
        AbilityPackage abilityPackage = null;
        try {
            abilityPackage = scope.get(AbilityPackage.class);
        } catch (Exception e) {
            throw new MissingHosContextException("Can't resolve Application instance. "
                    + ERROR_MSG);
        }
        return abilityPackage;
    }
}
