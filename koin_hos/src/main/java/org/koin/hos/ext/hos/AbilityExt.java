package org.koin.hos.ext.hos;

import ohos.aafwk.ability.Ability;
import org.koin.core.component.KoinScopeComponent;
import org.koin.core.qualifier.TypeQualifier;
import org.koin.core.scope.Scope;
import org.koin.hos.scope.LifecycleScopeDelegate;

public class AbilityExt {

    public static Scope abilityScope(Ability ability) {
        return new LifecycleScopeDelegate(ability).getValue();
    }

    public static Scope createScope(Ability ability, Object source) {
        String scopeId = KoinScopeComponent.getScopeId(ability);
        TypeQualifier scopeName = KoinScopeComponent.getScopeName(ability);
        try {
            Scope scope = ContextExt.getKoin(ability).createScope(scopeId, scopeName, source);
            return scope;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Scope createScope(Ability ability) {
        return createScope(ability, null);
    }

    public static Scope getScopeOrNull(Ability ability) {
        String scopeId = KoinScopeComponent.getScopeId(ability);
        return ContextExt.getKoin(ability).getScopeOrNull(scopeId);
    }
}
