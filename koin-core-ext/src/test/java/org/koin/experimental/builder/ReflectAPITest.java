package org.koin.experimental.builder;

import org.junit.Test;
import org.koin.core.KoinApplication;
import org.koin.core.definition.Definition;
import org.koin.core.error.DefinitionOverrideException;
import org.koin.core.error.NoScopeDefFoundException;
import org.koin.core.error.ScopeAlreadyCreatedException;
import org.koin.core.parameter.DefinitionParameters;
import org.koin.core.parameter.ParametersDefinition;
import org.koin.core.scope.Scope;
import org.koin.dsl.KoinAppDeclaration;
import org.koin.dsl.Module;
import org.koin.dsl.ModuleDeclaration;
import org.koin.ext.ClassUtil;

import java.text.DecimalFormat;

public class ReflectAPITest {

    public static <T> void makeJavaInstance(Class<T> clazz) throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        double startTime = System.nanoTime() / 1000000.0;
        KoinApplication app = org.koin.dsl.KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {
                try {
                    koinApplication.modules(Module.module(new ModuleDeclaration() {
                        @Override
                        public void invoke(org.koin.core.module.Module module) {
                            try {
                                module.single(clazz, new Definition<T>() {
                                    @Override
                                    public T invoke(Scope scope, DefinitionParameters parameters) {
                                        try {
                                            return (T) scope.get(clazz);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        return null;
                                    }
                                });
                            } catch (DefinitionOverrideException e) {
                                e.printStackTrace();
                            }
                        }
                    }));
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });
        double endTime = System.nanoTime() / 1000000.0;
        DecimalFormat to = new DecimalFormat("0.0000");
        String duration = to.format(endTime - startTime);
        app.getKoin().getLogger().debug(ClassUtil.getFullName(clazz) + "' in " + duration + " ms");

    }

    public static <T, S> void makeInstance(Class<T> tClassB, Class<S> sClassA) throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        double startTime = System.nanoTime() / 1000000.0;
        KoinApplication app = org.koin.dsl.KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {
                try {
                    koinApplication.modules(Module.module(new ModuleDeclaration() {
                        @Override
                        public void invoke(org.koin.core.module.Module module) {
                            try {
                                module.single(tClassB, new Definition<T>() {
                                    @Override
                                    public T invoke(Scope scope, DefinitionParameters parameters) {
                                        try {
                                            return (T) scope.bind(sClassA, tClassB, (ParametersDefinition) parameters);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        return null;
                                    }
                                });
                            } catch (DefinitionOverrideException e) {
                                e.printStackTrace();
                            }
                        }
                    }));
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });
        double endTime = System.nanoTime() / 1000000.0;
        DecimalFormat to = new DecimalFormat("0.0000");
        String duration = to.format(endTime - startTime);
        app.getKoin().getLogger().debug(ClassUtil.getFullName(sClassA) + "' in " + duration + " ms");
    }

    public static <T> void makeInstance(Class<T> clazz) throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        double startTime = System.nanoTime() / 1000000.0;
        KoinApplication app = org.koin.dsl.KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {
                try {
                    koinApplication.modules(Module.module(new ModuleDeclaration() {
                        @Override
                        public void invoke(org.koin.core.module.Module module) {
                            try {
                                module.single(clazz, new Definition<T>() {
                                    @Override
                                    public T invoke(Scope scope, DefinitionParameters parameters) {
                                        try {
                                            return (T) scope.get(clazz);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        return null;
                                    }
                                });
                            } catch (DefinitionOverrideException e) {
                                e.printStackTrace();
                            }
                        }
                    }));
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });
        double endTime = System.nanoTime() / 1000000.0;
        DecimalFormat to = new DecimalFormat("0.0000");
        String duration = to.format(endTime - startTime);
        app.getKoin().getLogger().debug(ClassUtil.getFullName(clazz) + "' in " + duration + " ms");
    }

    @Test
    public void reflect_api_to_construct_an_instance() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        makeJavaInstance(ComponentA.class);
        makeInstance(ComponentA.class);
        makeJavaInstance(ComponentA2.class);
        makeInstance(ComponentA2.class);
        makeJavaInstance(ComponentB.class);
        makeInstance(ComponentB.class, ComponentA.class);

    }
}
