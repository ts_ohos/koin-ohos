package org.koin.experimental.builder;

public class Presenter {

    public Repository repository;

    public Presenter(Repository repository) {
        this.repository = repository;
    }
}
