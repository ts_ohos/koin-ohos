package org.koin.experimental.builder;

import org.junit.Assert;
import org.junit.Test;
import org.koin.core.Koin;
import org.koin.core.definition.Definition;
import org.koin.core.error.DefinitionOverrideException;
import org.koin.core.error.NoScopeDefFoundException;
import org.koin.core.error.ScopeAlreadyCreatedException;
import org.koin.core.logger.Level;
import org.koin.core.parameter.DefinitionParameters;
import org.koin.core.qualifier.Qualifier;
import org.koin.core.scope.Scope;
import org.koin.dsl.*;

public class ScopeDelcarationTest {

    @Test
    public void can_t_get_scoped_dependency_without_scop_from_single() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        Qualifier scopeName = Qualifier.named("MY_SCOPE");
        Koin koin = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                try {
                    koinApplication.modules(Module.module(new ModuleDeclaration() {
                        @Override
                        public void invoke(org.koin.core.module.Module module) {
                            try {
                                module.single(ComponentA.class, new Definition<ComponentA>() {
                                    @Override
                                    public ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                        return new ComponentA();
                                    }
                                });
                                module.scope(ComponentB.class, new org.koin.core.module.Module.ScopeSet() {
                                    @Override
                                    public void invoke(ScopeDSL scopeDSL) {
                                        scopeDSL.single(scopeName, false, new Definition<ComponentB>() {
                                            @Override
                                            public ComponentB invoke(Scope scope, DefinitionParameters parameters) {
                                                try {
                                                    return scope.get(ComponentB.class, scopeName);
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                                return null;
                                            }
                                        });
                                    }
                                });
                            } catch (DefinitionOverrideException e) {
                                e.printStackTrace();
                            }
                        }
                    }));
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        try {
            Scope scope = koin.createScope("scope", scopeName);
            ComponentB componentB = scope.get(ComponentB.class);
            Assert.assertEquals(koin.get(ComponentA.class), componentB.a);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
