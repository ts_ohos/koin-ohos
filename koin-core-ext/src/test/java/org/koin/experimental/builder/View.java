package org.koin.experimental.builder;

import org.koin.core.component.KoinComponent;
import org.koin.core.error.ClosedScopeException;
import org.koin.core.error.DefinitionParameterException;
import org.koin.core.error.NoBeanDefFoundException;

public class View implements KoinComponent {

    public Presenter presenter;

    public View() {
        try {
            presenter = get(Presenter.class);
        } catch (DefinitionParameterException e) {
            e.printStackTrace();
        } catch (NoBeanDefFoundException e) {
            e.printStackTrace();
        } catch (ClosedScopeException e) {
            e.printStackTrace();
        }
    }
}
