package org.koin.experimental.builder;

public class ComponentB {

    public ComponentA a;

    public ComponentB(ComponentA a) {
        this.a = a;
    }
}
