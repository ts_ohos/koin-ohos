package org.koin.experimental.builder;

import org.junit.Assert;
import org.junit.Test;
import org.koin.core.Koin;
import org.koin.core.KoinApplication;
import org.koin.core.component.KoinComponent;
import org.koin.core.definition.Definition;
import org.koin.core.error.*;
import org.koin.core.logger.Level;
import org.koin.core.parameter.DefinitionParameters;
import org.koin.core.scope.Scope;
import org.koin.dsl.KoinAppDeclaration;
import org.koin.dsl.Module;
import org.koin.dsl.ModuleDeclaration;

import static org.junit.Assert.fail;
import static org.koin.dsl.KoinApplication.koinApplication;

public class CreateAPITest implements KoinComponent {

    @Test
    public void should_find_1st_constructor_and_build() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        Koin koin = koinApplication(
                new KoinAppDeclaration() {
                    @Override
                    public void invoke(KoinApplication koinApplication) {
                        koinApplication.printLogger(Level.DEBUG);

                        org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                            @Override
                            public void invoke(org.koin.core.module.Module module) {
                                try {
                                    module.single(ComponentA.class, new Definition<ComponentA>() {
                                        @Override
                                        public ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                            return new ComponentA();
                                        }
                                    });

                                    module.single(ComponentB.class, new Definition<ComponentB>() {
                                        @Override
                                        public ComponentB invoke(Scope scope, DefinitionParameters parameters) {
                                            try {
                                                return new ComponentB(scope.get(ComponentA.class));
                                            } catch (ClosedScopeException e) {
                                                e.printStackTrace();
                                            } catch (DefinitionParameterException e) {
                                                e.printStackTrace();
                                            } catch (NoBeanDefFoundException e) {
                                                e.printStackTrace();
                                            }
                                            return null;
                                        }
                                    });

                                } catch (DefinitionOverrideException e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                        try {
                            koinApplication.modules(module);
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                }
        ).getKoin();

        koin.get(ComponentB.class);
    }

    @Test()
    public void create_with_missing_dependency() {

        try {

            org.koin.core.KoinApplication app = org.koin.dsl.KoinApplication.koinApplication(new KoinAppDeclaration() {
                @Override
                public void invoke(KoinApplication koinApplication) {
                    koinApplication.printLogger(Level.DEBUG);
                    try {
                        koinApplication.modules(Module.module(new ModuleDeclaration() {
                            @Override
                            public void invoke(org.koin.core.module.Module module) {
                                try {
                                    module.single(ComponentB.class, null, new Definition<ComponentB>() {
                                        @Override
                                        public ComponentB invoke(Scope scope, DefinitionParameters parameters) {
                                            try {
                                                return InstanceBuilder.create(scope, ComponentB.class);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            return null;
                                        }
                                    });
                                } catch (DefinitionOverrideException e) {
                                    e.printStackTrace();
                                }
                            }
                        }));
                    } catch (DefinitionOverrideException e) {
                        e.printStackTrace();
                    }
                }
            });

            Koin koin = app.getKoin();
            koin.get(ComponentB.class);
            fail("should not get ComponentB");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void create_with_empty_ctor() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        org.koin.core.KoinApplication app = org.koin.dsl.KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                try {
                    koinApplication.modules(Module.module(new ModuleDeclaration() {
                        @Override
                        public void invoke(org.koin.core.module.Module module) {
                            try {
                                module.single(ComponentA.class, new Definition<ComponentA>() {
                                    @Override
                                    public ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                        try {
                                            return InstanceBuilder.create(scope, ComponentA.class);
                                        } catch (DefinitionParameterException e) {
                                            e.printStackTrace();
                                        } catch (NoBeanDefFoundException e) {
                                            e.printStackTrace();
                                        } catch (ClosedScopeException e) {
                                            e.printStackTrace();
                                        }
                                        return null;
                                    }
                                });
                            } catch (DefinitionOverrideException e) {
                                e.printStackTrace();
                            }
                        }
                    }));
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        Koin koin = app.getKoin();
        try {
            koin.get(ComponentA.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void create_for_interface() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        org.koin.core.KoinApplication app = org.koin.dsl.KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                try {
                    koinApplication.modules(Module.module(new ModuleDeclaration() {
                        @Override
                        public void invoke(org.koin.core.module.Module module) {
                            try {
                                module.single(ComponentA.class, new Definition<ComponentA>() {
                                    @Override
                                    public ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                        return new ComponentA();
                                    }
                                });

                                module.single(Component.class, new Definition<Component>() {
                                    @Override
                                    public Component invoke(Scope scope, DefinitionParameters parameters) {
                                        try {
                                            return InstanceBuilder.create(scope, ComponentD.class);
                                        } catch (DefinitionParameterException e) {
                                            e.printStackTrace();
                                        } catch (NoBeanDefFoundException e) {
                                            e.printStackTrace();
                                        } catch (ClosedScopeException e) {
                                            e.printStackTrace();
                                        }
                                        return null;
                                    }
                                });
                            } catch (DefinitionOverrideException e) {
                                e.printStackTrace();
                            }
                        }
                    }));
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });
        Koin koin = app.getKoin();
        try {
            koin.get(Component.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void create_factory_for_interface() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {

        org.koin.core.KoinApplication app = org.koin.dsl.KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                try {
                    koinApplication.modules(Module.module(new ModuleDeclaration() {
                        @Override
                        public void invoke(org.koin.core.module.Module module) {
                            try {
                                module.single(ComponentA.class, new Definition<ComponentA>() {
                                    @Override
                                    public ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                        return new ComponentA();
                                    }
                                });
                                module.factory(Component.class, new Definition<Component>() {
                                    @Override
                                    public Component invoke(Scope scope, DefinitionParameters parameters) {
                                        try {
                                            return InstanceBuilder.create(scope, ComponentD.class);
                                        } catch (DefinitionParameterException e) {
                                            e.printStackTrace();
                                        } catch (NoBeanDefFoundException e) {
                                            e.printStackTrace();
                                        } catch (ClosedScopeException e) {
                                            e.printStackTrace();
                                        }
                                        return null;
                                    }
                                });
                            } catch (DefinitionOverrideException e) {
                                e.printStackTrace();
                            }
                        }
                    }));
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });
        try {
            Koin koin = app.getKoin();
            Component f1 = koin.get(Component.class);
            Component f2 = koin.get(Component.class);
            Assert.assertNotEquals(f1, f2);
            Assert.assertEquals(f1.a, f2.a);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void create_API_overhead() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        org.koin.core.KoinApplication app = org.koin.dsl.KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                try {
                    koinApplication.modules(Module.module(new ModuleDeclaration() {
                        @Override
                        public void invoke(org.koin.core.module.Module module) {
                            try {
                                module.single(ComponentA.class, new Definition<ComponentA>() {
                                    @Override
                                    public ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                        try {
                                            return InstanceBuilder.create(scope, ComponentA.class);
                                        } catch (DefinitionParameterException e) {
                                            e.printStackTrace();
                                        } catch (NoBeanDefFoundException e) {
                                            e.printStackTrace();
                                        } catch (ClosedScopeException e) {
                                            e.printStackTrace();
                                        }
                                        return null;
                                    }
                                });
                                module.factory(ComponentD.class, new Definition<ComponentD>() {
                                    @Override
                                    public ComponentD invoke(Scope scope, DefinitionParameters parameters) {
                                        try {
                                            return InstanceBuilder.create(scope, ComponentD.class);
                                        } catch (DefinitionParameterException e) {
                                            e.printStackTrace();
                                        } catch (NoBeanDefFoundException e) {
                                            e.printStackTrace();
                                        } catch (ClosedScopeException e) {
                                            e.printStackTrace();
                                        }
                                        return null;
                                    }
                                });
                            } catch (DefinitionOverrideException e) {
                                e.printStackTrace();
                            }
                        }
                    }));
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });
        Koin koin = app.getKoin();
        for (int i = 0; i < 3; i++) {
            try {
                koin.get(Component.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
}
