package org.koin.experimental.builder;

import org.junit.*;
import org.koin.core.definition.Definition;
import org.koin.core.error.DefinitionOverrideException;
import org.koin.core.error.NoBeanDefFoundException;
import org.koin.core.error.NoScopeDefFoundException;
import org.koin.core.error.ScopeAlreadyCreatedException;
import org.koin.core.logger.Level;
import org.koin.core.module.Module;
import org.koin.core.parameter.DefinitionParameters;
import org.koin.core.parameter.ParametersDefinition;
import org.koin.core.scope.Scope;
import org.koin.dsl.KoinAppDeclaration;
import org.koin.dsl.KoinApplication;
import org.koin.dsl.ModuleDeclaration;
import org.koin.test.KoinTest;
import org.koin.test.KoinTestRule;
import org.koin.test.check.CheckModules;

public class MVPArchitectureTest implements KoinTest {

    @Rule
    public KoinTestRule rule = KoinTestRule.create(new KoinAppDeclaration() {
        @Override
        public void invoke(org.koin.core.KoinApplication koinApplication) {
            koinApplication.printLogger(Level.DEBUG);
            try {
                koinApplication.modules(MVPModule, DataSourceModule);
            } catch (DefinitionOverrideException e) {
                e.printStackTrace();
            }
        }
    });

    org.koin.core.module.Module MVPModule = org.koin.dsl.Module.module(new ModuleDeclaration() {
        @Override
        public void invoke(Module module) {
            try {
                module.single(Repository.class, new Definition<Repository>() {
                    @Override
                    public Repository invoke(Scope scope, DefinitionParameters parameters) {
                        return new Repository(scope.getSource(DebugDatasource.class));
                    }
                });
                module.single(View.class, new Definition<View>() {
                    @Override
                    public View invoke(Scope scope, DefinitionParameters parameters) {
                        return new View();
                    }
                });
                module.single(Presenter.class, new Definition<Presenter>() {
                    @Override
                    public Presenter invoke(Scope scope, DefinitionParameters parameters) {
                        try {
                            return new Presenter(scope.bind(Repository.class, Datasource.class, new ParametersDefinition() {
                                @Override
                                public DefinitionParameters invoke() {
                                    return new DefinitionParameters();
                                }
                            }));
                        } catch (NoBeanDefFoundException e) {
                            e.printStackTrace();
                        }
                        return null;
                    }
                });
            } catch (DefinitionOverrideException e) {
                e.printStackTrace();
            }
        }
    });

    org.koin.core.module.Module DataSourceModule = org.koin.dsl.Module.module(new ModuleDeclaration() {
        @Override
        public void invoke(Module module) {
            try {
                ModuleExt.singleBy(module, Datasource.class, DebugDatasource.class);
            } catch (DefinitionOverrideException e) {
                e.printStackTrace();
            }
        }
    });


    @Test
    public void should_create_all_MVP_hierarchy() {
        //TODO 测试未通过：报错信息：java.lang.IllegalStateException: KoinApplication has not been started
        try {
            View view = get(View.class);
            Presenter presenter = get(Presenter.class);
            Repository repository = get(Repository.class);
            Datasource datasource = get(Datasource.class);

            Assert.assertEquals(presenter, view.presenter);
            Assert.assertEquals(repository, presenter.repository);
            Assert.assertEquals(repository, view.presenter.repository);
            Assert.assertEquals(datasource, repository.datasource);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void check_MVP_hierarchy() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {

        CheckModules checkModules = new CheckModules();
        checkModules.checkModules(KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                try {
                    koinApplication.modules(MVPModule, DataSourceModule);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin());
    }
}
