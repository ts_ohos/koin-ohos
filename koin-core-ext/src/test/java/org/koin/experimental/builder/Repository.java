package org.koin.experimental.builder;

public class Repository {

    public Datasource datasource;

    public Repository(Datasource datasource) {
        this.datasource = datasource;
    }
}
