package org.koin.samples.coffee_maker;

public interface Pump {

    void pump();
}
