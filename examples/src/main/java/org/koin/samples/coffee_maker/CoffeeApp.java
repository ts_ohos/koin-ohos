package org.koin.samples.coffee_maker;

import org.koin.core.KoinApplication;
import org.koin.core.component.KoinComponent;
import org.koin.core.context.DefaultContextExt;
import org.koin.core.error.*;

public class CoffeeApp implements KoinComponent {

    private CoffeeMaker maker;

    public CoffeeApp() {
        try {
            maker = get(CoffeeMaker.class);
        } catch (DefinitionParameterException e) {
            e.printStackTrace();
        } catch (NoBeanDefFoundException e) {
            e.printStackTrace();
        } catch (ClosedScopeException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        try {
            DefaultContextExt.startKoin(new org.koin.dsl.KoinAppDeclaration() {
                @Override
                public void invoke(KoinApplication koinApplication) {
                    koinApplication.printLogger();
                    try {
                        koinApplication.modules(CoffeeAppModule.coffeeAppModule);
                    } catch (DefinitionOverrideException e) {
                        e.printStackTrace();
                    }
                }
            });

            CoffeeApp coffeeShop = new CoffeeApp();
            coffeeShop.maker.brew();

        } catch (NoScopeDefFoundException e) {
            e.printStackTrace();
        } catch (ScopeAlreadyCreatedException e) {
            e.printStackTrace();
        } catch (KoinAppAlreadyStartedException e) {
            e.printStackTrace();
        }
    }
}
