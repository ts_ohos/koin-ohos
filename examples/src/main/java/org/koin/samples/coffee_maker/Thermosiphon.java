package org.koin.samples.coffee_maker;

public class Thermosiphon implements Pump {

    private Heater heater;

    public Thermosiphon(Heater heater) {
        this.heater = heater;
    }

    @Override
    public void pump() {
        if (heater.isHot()) {
            System.out.println("=> => pumping => =>");
        }
    }
}
