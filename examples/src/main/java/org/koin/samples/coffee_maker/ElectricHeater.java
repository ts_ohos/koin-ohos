package org.koin.samples.coffee_maker;

public class ElectricHeater implements Heater {

    private boolean heating = false;

    @Override
    public void on() {
        System.out.println("~ ~ ~ heating ~ ~ ~");
        heating = true;
    }

    @Override
    public void off() {
        heating = false;
    }

    @Override
    public boolean isHot() {
        return heating;
    }
}
