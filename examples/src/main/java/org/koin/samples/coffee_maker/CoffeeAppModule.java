package org.koin.samples.coffee_maker;

import org.koin.core.definition.Definition;
import org.koin.core.error.DefinitionOverrideException;
import org.koin.core.module.Module;
import org.koin.core.parameter.DefinitionParameters;
import org.koin.core.scope.Scope;
import org.koin.dsl.ModuleDeclaration;

public class CoffeeAppModule {

    public static final Module coffeeAppModule = org.koin.dsl.Module.module(new ModuleDeclaration() {
        @Override
        public void invoke(Module module) {
            try {
                module.single(CoffeeMaker.class, new Definition<CoffeeMaker>() {
                    @Override
                    public CoffeeMaker invoke(Scope scope, DefinitionParameters parameters) {
                        try {
                            return new CoffeeMaker(scope.get(Pump.class), scope.get(Heater.class));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return null;
                    }
                });

                module.single(Pump.class, new Definition<Pump>() {
                    @Override
                    public Pump invoke(Scope scope, DefinitionParameters parameters) {
                        try {
                            return new Thermosiphon(scope.get(Heater.class));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return null;
                    }
                });

                module.single(Heater.class, new Definition<Heater>() {
                    @Override
                    public Heater invoke(Scope scope, DefinitionParameters parameters) {
                        return new ElectricHeater();
                    }
                });

            } catch (DefinitionOverrideException e) {
                e.printStackTrace();
            }
        }
    });

}
