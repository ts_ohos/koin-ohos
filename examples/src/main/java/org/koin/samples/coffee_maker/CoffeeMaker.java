package org.koin.samples.coffee_maker;

public class CoffeeMaker {

    private Pump pump;
    private Heater heater;

    public CoffeeMaker(Pump pump, Heater heater) {
        this.pump = pump;
        this.heater = heater;
    }

    public void brew() {
        heater.on();
        pump.pump();
        System.out.println(" [_]P coffee! [_]P ");
        heater.off();
    }
}
