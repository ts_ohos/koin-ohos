package org.koin.samples.coffee_maker;

public interface Heater {

    void on();

    void off();

    boolean isHot();
}
