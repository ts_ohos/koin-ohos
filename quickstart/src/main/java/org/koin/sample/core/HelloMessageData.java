package org.koin.sample.core;

public class HelloMessageData {

    public String message;

    public HelloMessageData(String message) {
        this.message = message;
    }

    public HelloMessageData() {
        this.message = "Hello Koin!";
    }
}
