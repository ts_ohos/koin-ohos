package org.koin.sample.core;

import org.koin.core.KoinApplication;
import org.koin.core.component.KoinComponent;
import org.koin.core.context.DefaultContextExt;
import org.koin.core.error.*;

import static org.koin.java.KoinJavaComponent.inject;

/**
 * HelloApplication - Application Class
 * use HelloService
 */
public class HelloApplication implements KoinComponent {

    public HelloService helloService;

    public HelloApplication() {
        try {
            helloService = inject(HelloService.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sayHello() {
        System.out.println(helloService.hello());
    }

    public static void main(String[] args) {
        try {

            //start koin
            DefaultContextExt.startKoin(new org.koin.dsl.KoinAppDeclaration() {
                @Override
                public void invoke(KoinApplication koinApplication) {

                    // use Koin logger
                    koinApplication.printLogger();

                    try {
                        // declare modules
                        koinApplication.modules(HelloModule.helloModule);
                    } catch (DefinitionOverrideException e) {
                        e.printStackTrace();
                    }
                }
            });

            //use it
            new HelloApplication().sayHello();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
