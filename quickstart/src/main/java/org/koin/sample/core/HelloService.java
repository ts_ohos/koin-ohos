package org.koin.sample.core;

/**
 * Hello Service - interface
 */
public interface HelloService {

    String hello();

}
