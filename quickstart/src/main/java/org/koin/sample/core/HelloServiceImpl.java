package org.koin.sample.core;

/**
 * Hello Service Impl
 * Will use HelloMessageData data
 */
public class HelloServiceImpl implements HelloService {

    private HelloMessageData helloMessageData;

    public HelloServiceImpl(HelloMessageData helloMessageData) {
        this.helloMessageData = helloMessageData;
    }

    @Override
    public String hello() {
        return "Hey, " + helloMessageData.message;
    }
}
