package org.koin.sample.core;

import org.koin.core.definition.Definition;
import org.koin.core.error.ClosedScopeException;
import org.koin.core.error.DefinitionOverrideException;
import org.koin.core.error.DefinitionParameterException;
import org.koin.core.error.NoBeanDefFoundException;
import org.koin.core.module.Module;
import org.koin.core.parameter.DefinitionParameters;
import org.koin.core.scope.Scope;
import org.koin.dsl.ModuleDeclaration;

public class HelloModule {

    public static final Module helloModule = org.koin.dsl.Module.module(new ModuleDeclaration() {
        @Override
        public void invoke(Module module) {
            try {
                module.single(HelloMessageData.class, new Definition<HelloMessageData>() {
                    @Override
                    public HelloMessageData invoke(Scope scope, DefinitionParameters parameters) {
                        return new HelloMessageData();
                    }
                });

                module.single(HelloService.class, new Definition<HelloService>() {
                    @Override
                    public HelloService invoke(Scope scope, DefinitionParameters parameters) {
                        try {
                            return new HelloServiceImpl(scope.get(HelloMessageData.class));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return null;
                    }
                });

            } catch (DefinitionOverrideException e) {
                e.printStackTrace();
            }
        }
    });

}
