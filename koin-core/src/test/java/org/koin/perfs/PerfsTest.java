package org.koin.perfs;

import org.junit.Test;
import org.koin.core.Koin;
import org.koin.core.KoinApplication;
import org.koin.core.definition.Definition;
import org.koin.core.definition.Definitions;
import org.koin.core.definition.Options;
import org.koin.core.error.*;
import org.koin.core.parameter.DefinitionParameters;
import org.koin.core.scope.Scope;
import org.koin.core.scope.ScopeDefinition;
import org.koin.core.setterinjecttest.A;
import org.koin.dsl.KoinAppDeclaration;
import org.koin.dsl.Module;
import org.koin.dsl.ModuleDeclaration;
import org.koin.test.KoinApplicationExt;

import java.text.DecimalFormat;
import java.util.ArrayList;

import static org.koin.dsl.KoinApplication.koinApplication;

public class PerfsTest {

    @Test
    public void empty_module_perfs() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        double start = System.nanoTime() / 1000000.0;
        KoinApplication app = koinApplication();
        double end = System.nanoTime() / 1000000.0;
        DecimalFormat to = new DecimalFormat("0.0000");
        System.out.println("mepty - start " + to.format(end - start) + " ms");
        KoinApplicationExt.assertDefinitionsCount(app.getKoin(), 0);
        app.close();
    }

    @Test
    public void module_no_dsl() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionOverrideException {
        koinApplication().close();

        for (int i = 1; i < 10; i++) {
            useDSL();
            dontUseDSL();
        }
    }

    private void dontUseDSL() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionOverrideException {
        double start = System.nanoTime() / 1000000.0;

        KoinApplication app = koinApplication();
        app.getKoin()
                .getScopeRegistry()
                .declareDefinition(
                        Definitions.INSTANCE().createSingle(A.class,
                                null,
                                new Definition() {
                                    @Override
                                    public Object invoke(Scope scope, DefinitionParameters parameters) {
                                        return new A();
                                    }
                                },
                                new Options(),
                                new ArrayList<>(),
                                ScopeDefinition.ROOT_SCOPE_QUALIFIER)
                );
        app.close();

        double end = System.nanoTime() / 1000000.0;

        DecimalFormat to = new DecimalFormat("0.0000");

        System.out.println("no dsl " + to.format(end - start) + " ms");
    }

    private void useDSL() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        double start = System.nanoTime() / 1000000.0;

        KoinApplication app = koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {
                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.single(A.class, new Definition<A>() {
                                @Override
                                public A invoke(Scope scope, DefinitionParameters parameters) {
                                    return new A();
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });
        app.close();

        double end = System.nanoTime() / 1000000.0;

        DecimalFormat to = new DecimalFormat("0.0000");

        System.out.println("dsl " + to.format(end - start) + " ms");
    }

    @Test
    public void perfModule400_module_perfs() throws NoBeanDefFoundException, DefinitionParameterException, ScopeAlreadyCreatedException, ClosedScopeException, NoScopeDefFoundException {
        runPerfs();
        runPerfs();
    }

    private void runPerfs() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {

        System.out.println("perf400 - start");

        double start = System.nanoTime() / 1000000.0;

        KoinApplication app = koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {
                try {
                    koinApplication.modules(Module400.perfModule400);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        Koin koin = app.getKoin();

        double end = System.nanoTime() / 1000000.0;

        DecimalFormat to = new DecimalFormat("0.0000");

        System.out.println("use time : " + to.format(end - start) + " ms");

        System.out.println("perf400 - executed");

        double start1 = System.nanoTime() / 1000000.0;

        koin.get(Perfs.A27.class);
        koin.get(Perfs.A31.class);
        koin.get(Perfs.A12.class);
        koin.get(Perfs.A42.class);

        double end1 = System.nanoTime() / 1000000.0;
        System.out.println("use time : " + to.format(end1 - start1) + " ms");

        app.close();
    }
}
