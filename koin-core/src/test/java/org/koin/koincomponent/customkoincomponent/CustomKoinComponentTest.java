package org.koin.koincomponent.customkoincomponent;

import org.junit.Test;
import org.koin.Simple;
import org.koin.core.error.ClosedScopeException;
import org.koin.core.error.DefinitionParameterException;
import org.koin.core.error.NoBeanDefFoundException;

import static org.junit.Assert.assertEquals;
import static org.koin.test.Asserts.assertHasNoStandaloneInstance;

public class CustomKoinComponentTest {

    @Test
    public void can_inject_KoinComponent_from_custom_instance() throws DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        MyCustomApp app = new MyCustomApp();
        Simple.ComponentA a = CustomKoinComponent.customKoin().get(Simple.ComponentA.class);
        assertEquals(app.a(), a);

        assertHasNoStandaloneInstance();
    }

}
