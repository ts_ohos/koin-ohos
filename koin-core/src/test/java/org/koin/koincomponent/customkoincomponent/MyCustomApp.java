package org.koin.koincomponent.customkoincomponent;

import org.koin.Simple;
import org.koin.core.error.ClosedScopeException;
import org.koin.core.error.DefinitionParameterException;
import org.koin.core.error.NoBeanDefFoundException;

public class MyCustomApp extends CustomKoinComponent {

    private Simple.ComponentA a;

    public Simple.ComponentA a() {
        if (a == null) {
            try {
                a = get(Simple.ComponentA.class);
            } catch (DefinitionParameterException e) {
                e.printStackTrace();
            } catch (NoBeanDefFoundException e) {
                e.printStackTrace();
            } catch (ClosedScopeException e) {
                e.printStackTrace();
            }
        }
        return a;
    }
}
