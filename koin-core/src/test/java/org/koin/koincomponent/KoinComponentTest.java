package org.koin.koincomponent;

import org.junit.Test;
import org.koin.Simple;
import org.koin.core.Koin;
import org.koin.core.KoinApplication;
import org.koin.core.component.KoinComponent;
import org.koin.core.definition.Definition;
import org.koin.core.error.*;
import org.koin.core.parameter.DefinitionParameters;
import org.koin.core.scope.Scope;
import org.koin.dsl.Module;
import org.koin.dsl.ModuleDeclaration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.koin.core.context.DefaultContextExt.startKoin;
import static org.koin.core.context.DefaultContextExt.stopKoin;

public class KoinComponentTest {

    @Test
    public void can_lazy_inject_from_KoinComponent() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, KoinAppAlreadyStartedException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        KoinApplication app = startKoin(new org.koin.dsl.KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {
                koinApplication.printLogger();

                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.single(Simple.ComponentA.class, new Definition<Simple.ComponentA>() {
                                @Override
                                public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Simple.ComponentA();
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });

                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        Koin koin = app.getKoin();
        Simple.ComponentA a = koin.<Simple.ComponentA>get(Simple.ComponentA.class);
        MyComponent component = new MyComponent();

        assertEquals(component.getAnInject(), a);
        assertEquals(component.getaGet(), a);

        stopKoin();
    }

    @Test
    public void can_lazy_inject_before_starting_Koin() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, KoinAppAlreadyStartedException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        Exception caughtException = null;
        MyLazyComponent component = null;
        try {
            component = new MyLazyComponent();
        } catch (Exception e) {
            caughtException = e;
        }

        assertNull(caughtException);

        Koin koin = startKoin(new org.koin.dsl.KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {
                koinApplication.printLogger();
                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.single(Simple.ComponentA.class, new Definition<Simple.ComponentA>() {
                                @Override
                                public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Simple.ComponentA();
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        Simple.ComponentA a = koin.get(Simple.ComponentA.class);
        assertEquals(component.getAnInject(), a);
        stopKoin();
    }
}

class MyComponent implements KoinComponent {
    private Simple.ComponentA anInject;
    private Simple.ComponentA aGet;

    public Simple.ComponentA getAnInject() {
        if (anInject == null) {
            try {
                anInject = get(Simple.ComponentA.class);
            } catch (DefinitionParameterException e) {
                e.printStackTrace();
            } catch (NoBeanDefFoundException e) {
                e.printStackTrace();
            } catch (ClosedScopeException e) {
                e.printStackTrace();
            }
        }
        return anInject;
    }

    public Simple.ComponentA getaGet() {
        if (aGet == null) {
            try {
                aGet = get(Simple.ComponentA.class);
            } catch (DefinitionParameterException e) {
                e.printStackTrace();
            } catch (NoBeanDefFoundException e) {
                e.printStackTrace();
            } catch (ClosedScopeException e) {
                e.printStackTrace();
            }
        }
        return aGet;
    }
}

class MyLazyComponent implements KoinComponent {


    private Simple.ComponentA anInject;

    public Simple.ComponentA getAnInject() {
        if (anInject == null) {
            try {
                anInject = get(Simple.ComponentA.class);
            } catch (DefinitionParameterException e) {
                e.printStackTrace();
            } catch (NoBeanDefFoundException e) {
                e.printStackTrace();
            } catch (ClosedScopeException e) {
                e.printStackTrace();
            }
        }
        return anInject;
    }
}
