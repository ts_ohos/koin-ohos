package org.koin.koincomponent.apptest;

import org.junit.Test;
import org.koin.core.Koin;
import org.koin.core.KoinApplication;
import org.koin.core.definition.Definition;
import org.koin.core.error.*;
import org.koin.core.parameter.DefinitionParameters;
import org.koin.core.scope.Scope;
import org.koin.dsl.Module;
import org.koin.dsl.ModuleDeclaration;

import static org.junit.Assert.assertEquals;
import static org.koin.core.context.DefaultContextExt.startKoin;
import static org.koin.core.context.DefaultContextExt.stopKoin;

public class AppTest {

    @Test
    public void can_run_KoinComponent_app() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, KoinAppAlreadyStartedException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        Koin koin = startKoin(new org.koin.dsl.KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {
                koinApplication.printLogger();

                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.single(TasksView.class, new Definition<TasksView>() {
                                @Override
                                public TasksView invoke(Scope scope, DefinitionParameters parameters) {
                                    return new TasksView();
                                }
                            });

                            module.single(TasksPresenter.class, new Definition<TasksPresenter>() {
                                @Override
                                public TasksPresenter invoke(Scope scope, DefinitionParameters parameters) {
                                    try {
                                        return new TasksPresenter(scope.get(TasksView.class));
                                    } catch (ClosedScopeException e) {
                                        e.printStackTrace();
                                    } catch (DefinitionParameterException e) {
                                        e.printStackTrace();
                                    } catch (NoBeanDefFoundException e) {
                                        e.printStackTrace();
                                    }
                                    return null;
                                }
                            });

                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });

                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        MyApp myApp = new MyApp();
        assertEquals(myApp.getPresenter().view, myApp.getView());
        assertEquals(myApp.getPresenter(), koin.get(TasksPresenter.class));

        stopKoin();
    }
}
