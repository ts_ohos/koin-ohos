package org.koin.koincomponent.apptest;

import org.koin.core.component.KoinComponent;
import org.koin.core.error.ClosedScopeException;
import org.koin.core.error.DefinitionParameterException;
import org.koin.core.error.NoBeanDefFoundException;

public class MyApp implements KoinComponent {

    private TasksView view;
    private TasksPresenter presenter;

    public TasksView getView() {
        if (view == null) {
            try {
                view = get(TasksView.class);
            } catch (DefinitionParameterException e) {
                e.printStackTrace();
            } catch (NoBeanDefFoundException e) {
                e.printStackTrace();
            } catch (ClosedScopeException e) {
                e.printStackTrace();
            }
        }
        return view;
    }

    public TasksPresenter getPresenter() {
        if (presenter == null) {
            try {
                presenter = get(TasksPresenter.class);
            } catch (DefinitionParameterException e) {
                e.printStackTrace();
            } catch (NoBeanDefFoundException e) {
                e.printStackTrace();
            } catch (ClosedScopeException e) {
                e.printStackTrace();
            }
        }
        return presenter;
    }
}
