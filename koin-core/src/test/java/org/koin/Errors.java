package org.koin;

public class Errors {

    public static class Boom {
        public Boom() {
            throw new IllegalStateException("Got error while init :(");
        }
    }

    public static class CycleA {
        public CycleA(CycleB b) {
        }
    }

    public static class CycleB {
        public CycleB(CycleA a) {

        }
    }
}
