package org.koin.java;

public class LocalDbImplementation implements DataSource {

    @Override
    public String getData() {
        return "data from db";
    }
}
