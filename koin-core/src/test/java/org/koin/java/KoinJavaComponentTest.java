package org.koin.java;

import org.junit.After;
import org.junit.Before;
import org.koin.core.KoinApplication;
import org.koin.core.definition.Definition;
import org.koin.core.error.DefinitionOverrideException;
import org.koin.core.error.KoinAppAlreadyStartedException;
import org.koin.core.error.NoScopeDefFoundException;
import org.koin.core.error.ScopeAlreadyCreatedException;
import org.koin.core.parameter.DefinitionParameters;
import org.koin.core.scope.Scope;
import org.koin.dsl.Module;
import org.koin.dsl.ModuleDeclaration;

import java.util.HashMap;
import java.util.Map;

import static org.koin.core.context.DefaultContextExt.startKoin;
import static org.koin.core.context.DefaultContextExt.stopKoin;
import static org.koin.core.qualifier.Qualifier.named;

public class KoinJavaComponentTest {

    @Before
    public void before() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, KoinAppAlreadyStartedException {


        startKoin(new org.koin.dsl.KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {
                Map<String, String> map = new HashMap<>();
                map.put("PrefixProp", "_");
                map.put("SeparatorProp", "|");
                koinApplication.properties(map);

                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.single(DataSource.class, named("db"), new Definition<DataSource>() {
                                @Override
                                public DataSource invoke(Scope scope, DefinitionParameters parameters) {
                                    return new LocalDbImplementation();
                                }
                            });

                            module.single(DataSource.class, named("api"), new Definition<DataSource>() {
                                @Override
                                public DataSource invoke(Scope scope, DefinitionParameters parameters) {
                                    return new RemoteApiImplementation();
                                }
                            });

                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });

                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @After
    public void after() {
        stopKoin();
    }
}
