package org.koin.java;

public class ComponentB {

    public ComponentA componentA;

    public ComponentB(ComponentA componentA) {
        this.componentA = componentA;
    }
}
