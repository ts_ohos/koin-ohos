package org.koin.java;

public interface DataSource {

    public String getData();

}
