package org.koin.java;

public class DataConverter {

    private String separator;

    public DataConverter(String separator) {
        this.separator = separator;
    }

    public String convert(String... data) {
        for (String datum : data) {
            separator += datum;
        }
        return separator;
    }
}
