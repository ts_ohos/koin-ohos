package org.koin.java;

public class RemoteApiImplementation implements DataSource {
    @Override
    public String getData() {
        return "data from api";
    }
}
