package org.koin.test;

import org.junit.Assert;
import org.koin.core.Koin;
import org.koin.core.annotation.KoinInternalApi;
import org.koin.core.definition.BeanDefinition;
import org.koin.core.instance.InstanceFactory;

import java.util.Collection;
import java.util.HashSet;

public class KoinApplicationExt {

    @KoinInternalApi
    public static void assertDefinitionsCount(Koin koin, int count) {
        Assert.assertEquals(count, koin.getScopeRegistry().size());
    }

    @KoinInternalApi
    public static BeanDefinition getBeanDefinition(Koin koin, Class clazz) {
        HashSet<BeanDefinition> definitions = koin
                .getScopeRegistry()
                .getRootScope()
                .getScopeDefinition()
                .getDefinitions();
        for (BeanDefinition definition : definitions) {
            if (definition.getPrimaryType().equals(clazz)) {
                return definition;
            }
        }
        return null;
    }

    @KoinInternalApi
    public static InstanceFactory getInstanceFactory(Koin koin, Class clazz) {
        Collection<InstanceFactory> values = koin
                .getScopeRegistry()
                .getRootScope()
                .getInstanceRegistry()
                .getInstances()
                .values();

        for (InstanceFactory value : values) {
            if (value.getBeanDefinition().getPrimaryType().equals(clazz)) {
                return value;
            }
        }
        return null;
    }
}
