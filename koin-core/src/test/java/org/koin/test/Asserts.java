package org.koin.test;

import org.koin.mp.KoinPlatformTools;

import static org.junit.Assert.assertNull;

public class Asserts {

    public static void assertHasNoStandaloneInstance() {
        assertNull(KoinPlatformTools.INSTANCE().defaultContext().getOrNull());
    }

}
