package org.koin.dsl;

import org.junit.Assert;
import org.junit.Test;
import org.koin.Simple;
import org.koin.core.definition.*;
import org.koin.core.error.DefinitionOverrideException;
import org.koin.core.error.DefinitionParameterException;
import org.koin.core.error.NoScopeDefFoundException;
import org.koin.core.error.ScopeAlreadyCreatedException;
import org.koin.core.instance.InstanceContext;
import org.koin.core.instance.InstanceFactory;
import org.koin.core.parameter.DefinitionParameters;
import org.koin.core.parameter.ParametersDefinition;
import org.koin.core.qualifier.Qualifier;
import org.koin.core.scope.Scope;

import static org.koin.test.KoinApplicationExt.getBeanDefinition;
import static org.koin.test.KoinApplicationExt.getInstanceFactory;


public class BeanDefinitionTest {

    private Scope scope = null;

    private Scope rootScope() {
        if (null == scope) {
            try {
                org.koin.core.KoinApplication app = KoinApplication.koinApplication(new KoinAppDeclaration() {
                    @Override
                    public void invoke(org.koin.core.KoinApplication koinApplication) {
                    }
                });
                scope = app.getKoin().getScopeRegistry().getRootScope();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return scope;
    }

    @Test
    public void equals_definitions() {

        BeanDefinition def = Definitions.INSTANCE().createSingle(Simple.ComponentA.class, null, null, new Options(), null, rootScope().getScopeDefinition().getQualifier());
        BeanDefinition def1 = Definitions.INSTANCE().createSingle(Simple.ComponentA.class, null, null, new Options(), null, rootScope().getScopeDefinition().getQualifier());
        Assert.assertEquals(def, def1);

    }

    @Test
    public void scope_definition() {
        BeanDefinition def = Definitions.INSTANCE().createSingle(Simple.ComponentA.class, null, null, new Options(), null, rootScope().getScopeDefinition().getQualifier());
        Assert.assertEquals(rootScope().getScopeDefinition().getQualifier(), def.getScopeQualifier());
        Assert.assertEquals(Kind.Single, def.getKind());
        Assert.assertEquals(rootScope().getScopeDefinition().getQualifier(), def.getScopeQualifier());
    }

    @Test
    public void equals_definitions_but_diff_kind() {

        BeanDefinition def = Definitions.INSTANCE().createSingle(Simple.ComponentA.class, null, null, new Options(), null, rootScope().getScopeDefinition().getQualifier());
        BeanDefinition def1 = Definitions.INSTANCE().createSingle(Simple.ComponentA.class, null, null, new Options(), null, rootScope().getScopeDefinition().getQualifier());
        Assert.assertEquals(def, def1);
    }

    @Test
    public void definition_kind() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        org.koin.core.KoinApplication app = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                try {
                    koinApplication.modules(Module.module(new ModuleDeclaration() {
                        @Override
                        public void invoke(org.koin.core.module.Module module) {
                            try {
                                module.single(Simple.ComponentA.class, new Definition<Simple.ComponentA>() {
                                    @Override
                                    public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                        return new Simple.ComponentA();
                                    }
                                });

                                module.factory(Simple.ComponentB.class, new Definition<Simple.ComponentB>() {
                                    @Override
                                    public Simple.ComponentB invoke(Scope scope, DefinitionParameters parameters) {
                                        try {
                                            return new Simple.ComponentB(scope.get(Simple.ComponentA.class));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        return null;
                                    }
                                });
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }));
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        BeanDefinition defA = getBeanDefinition(app.getKoin(), Simple.ComponentA.class);
        if (null == defA) {
            throw new InstantiationError("no definition found");
        }
        Assert.assertEquals(Kind.Single, defA.getKind());
        BeanDefinition defB = getBeanDefinition(app.getKoin(), Simple.ComponentB.class);
        if (null == defB) {
            throw new InstantiationError("no definition found");
        }
        Assert.assertEquals(Kind.Factory, defB.getKind());
    }

    @Test
    public void definition_name() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        Qualifier name = Qualifier.named("A");
        org.koin.core.KoinApplication app = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                try {
                    koinApplication.modules(Module.module(new ModuleDeclaration() {
                        @Override
                        public void invoke(org.koin.core.module.Module module) {
                            try {
                                module.single(Simple.ComponentA.class, name, false, false, new Definition<Simple.ComponentA>() {
                                    @Override
                                    public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                        return new Simple.ComponentA();
                                    }
                                });
                                module.factory(Simple.ComponentB.class, new Definition<Simple.ComponentB>() {
                                    @Override
                                    public Simple.ComponentB invoke(Scope scope, DefinitionParameters parameters) {
                                        try {
                                            return new Simple.ComponentB(scope.get(Simple.ComponentA.class));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        return null;
                                    }
                                });
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }));
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        BeanDefinition defA = getBeanDefinition(app.getKoin(), Simple.ComponentA.class);
        if (null == defA) {
            throw new InstantiationError("no definition found");
        }
        Assert.assertEquals(name, defA.getQualifier());
        BeanDefinition defB = getBeanDefinition(app.getKoin(), Simple.ComponentB.class);
        if (null == defB) {
            throw new InstantiationError("no definition found");
        }
        Assert.assertTrue(defB.getQualifier() == null);

    }

    @Test
    public void definition_function() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        org.koin.core.KoinApplication app = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                try {
                    koinApplication.modules(Module.module(new ModuleDeclaration() {
                        @Override
                        public void invoke(org.koin.core.module.Module module) {
                            try {
                                module.single(Simple.ComponentA.class, new Definition<Simple.ComponentA>() {
                                    @Override
                                    public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                        return new Simple.ComponentA();
                                    }
                                });
                            } catch (DefinitionOverrideException e) {
                                e.printStackTrace();
                            }
                        }
                    }));
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });
        try {
            BeanDefinition def = getBeanDefinition(app.getKoin(), Simple.ComponentA.class);
            if (null == def) {
                throw new InstantiationError("no definition found");
            }

            InstanceFactory instance = getInstanceFactory(app.getKoin(), Simple.ComponentA.class);
            Simple.ComponentA componentA = (Simple.ComponentA) instance.get(new InstanceContext(app.getKoin(), rootScope(), new ParametersDefinition() {
                @Override
                public DefinitionParameters invoke() {
                    try {
                        return DefinitionParameters.parametersOf(null);
                    } catch (DefinitionParameterException e) {
                        e.printStackTrace();
                    }
                    return null;
                }
            }));
            Assert.assertEquals(componentA, app.getKoin().get(Simple.ComponentA.class));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
