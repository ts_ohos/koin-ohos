package org.koin.dsl;

import org.junit.Assert;
import org.junit.Test;
import org.koin.Simple;
import org.koin.core.definition.Definition;
import org.koin.core.error.*;
import org.koin.core.logger.Level;
import org.koin.core.parameter.DefinitionParameters;
import org.koin.core.scope.Scope;
import org.koin.test.KoinApplicationExt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ModuleCreationTest {

    @Test
    public void create_an_empty_module() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        org.koin.core.KoinApplication app = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                try {
                    koinApplication.modules(Module.module(null));
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        KoinApplicationExt.assertDefinitionsCount(app.getKoin(), 0);
    }

    @Test
    public void load_a_module_once_started() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        org.koin.core.KoinApplication app = KoinApplication.koinApplication(null);
        KoinApplicationExt.assertDefinitionsCount(app.getKoin(), 0);

        org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
            @Override
            public void invoke(org.koin.core.module.Module module) {
                try {
                    module.single(Simple.ComponentA.class, new Definition<Simple.ComponentA>() {
                        @Override
                        public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                            return new Simple.ComponentA();
                        }
                    });
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });
        try {
            app.modules(module);
        } catch (DefinitionOverrideException e) {
            e.printStackTrace();
        }
        KoinApplicationExt.assertDefinitionsCount(app.getKoin(), 1);
    }

    @Test
    public void create_a_module_with_single() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        org.koin.core.KoinApplication app = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.single(Simple.ComponentA.class, new Definition<Simple.ComponentA>() {
                                @Override
                                public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Simple.ComponentA();
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        KoinApplicationExt.assertDefinitionsCount(app.getKoin(), 1);
    }

    @Test
    public void create_a_complex_single_DI_module() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        org.koin.core.KoinApplication app = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.single(Simple.ComponentA.class, new Definition<Simple.ComponentA>() {
                                @Override
                                public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Simple.ComponentA();
                                }
                            });

                            module.single(Simple.ComponentB.class, new Definition<Simple.ComponentB>() {
                                @Override
                                public Simple.ComponentB invoke(Scope scope, DefinitionParameters parameters) {
                                    try {
                                        return new Simple.ComponentB(scope.get(Simple.ComponentA.class));
                                    } catch (ClosedScopeException e) {
                                        e.printStackTrace();
                                    } catch (DefinitionParameterException e) {
                                        e.printStackTrace();
                                    } catch (NoBeanDefFoundException e) {
                                        e.printStackTrace();
                                    }
                                    return null;
                                }
                            });

                            module.factory(Simple.ComponentC.class, new Definition<Simple.ComponentC>() {
                                @Override
                                public Simple.ComponentC invoke(Scope scope, DefinitionParameters parameters) {
                                    try {
                                        return new Simple.ComponentC(scope.get(Simple.ComponentB.class));
                                    } catch (ClosedScopeException e) {
                                        e.printStackTrace();
                                    } catch (DefinitionParameterException e) {
                                        e.printStackTrace();
                                    } catch (NoBeanDefFoundException e) {
                                        e.printStackTrace();
                                    }
                                    return null;
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        KoinApplicationExt.assertDefinitionsCount(app.getKoin(), 3);
    }

    @Test
    public void create_several_modules() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {

        org.koin.core.KoinApplication app = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                org.koin.core.module.Module module1 = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.single(Simple.ComponentA.class, new Definition<Simple.ComponentA>() {
                                @Override
                                public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Simple.ComponentA();
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });

                org.koin.core.module.Module module2 = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.single(Simple.ComponentB.class, new Definition<Simple.ComponentB>() {
                                @Override
                                public Simple.ComponentB invoke(Scope scope, DefinitionParameters parameters) {
                                    try {
                                        return new Simple.ComponentB(scope.get(Simple.ComponentA.class));
                                    } catch (ClosedScopeException e) {
                                        e.printStackTrace();
                                    } catch (DefinitionParameterException e) {
                                        e.printStackTrace();
                                    } catch (NoBeanDefFoundException e) {
                                        e.printStackTrace();
                                    }
                                    return null;
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });
                try {
                    koinApplication.modules(Arrays.asList(module1, module2));
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        KoinApplicationExt.assertDefinitionsCount(app.getKoin(), 2);
    }

    @Test
    public void create_modules_list() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {

        org.koin.core.KoinApplication app = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                org.koin.core.module.Module module1 = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.single(Simple.ComponentA.class, new Definition<Simple.ComponentA>() {
                                @Override
                                public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Simple.ComponentA();
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });

                org.koin.core.module.Module module2 = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.single(Simple.ComponentB.class, new Definition<Simple.ComponentB>() {
                                @Override
                                public Simple.ComponentB invoke(Scope scope, DefinitionParameters parameters) {
                                    try {
                                        return new Simple.ComponentB(scope.get(Simple.ComponentA.class));
                                    } catch (ClosedScopeException e) {
                                        e.printStackTrace();
                                    } catch (DefinitionParameterException e) {
                                        e.printStackTrace();
                                    } catch (NoBeanDefFoundException e) {
                                        e.printStackTrace();
                                    }
                                    return null;
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });

                try {
                    koinApplication.modules(Arrays.asList(module1, module2));
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        KoinApplicationExt.assertDefinitionsCount(app.getKoin(), 2);
    }

    @Test
    public void create_modules_list_timing() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {

        KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                org.koin.core.module.Module module1 = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.single(Simple.ComponentA.class, new Definition<Simple.ComponentA>() {
                                @Override
                                public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Simple.ComponentA();
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });

                org.koin.core.module.Module module2 = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.single(Simple.ComponentB.class, new Definition<Simple.ComponentB>() {
                                @Override
                                public Simple.ComponentB invoke(Scope scope, DefinitionParameters parameters) {
                                    try {
                                        return new Simple.ComponentB(scope.get(Simple.ComponentA.class));
                                    } catch (ClosedScopeException e) {
                                        e.printStackTrace();
                                    } catch (DefinitionParameterException e) {
                                        e.printStackTrace();
                                    } catch (NoBeanDefFoundException e) {
                                        e.printStackTrace();
                                    }
                                    return null;
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });
                try {
                    koinApplication.modules(Arrays.asList(module1, module2));
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                org.koin.core.module.Module module1 = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.single(Simple.ComponentA.class, new Definition<Simple.ComponentA>() {
                                @Override
                                public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Simple.ComponentA();
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });

                org.koin.core.module.Module module2 = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.single(Simple.ComponentB.class, new Definition<Simple.ComponentB>() {
                                @Override
                                public Simple.ComponentB invoke(Scope scope, DefinitionParameters parameters) {
                                    try {
                                        return new Simple.ComponentB(scope.get(Simple.ComponentA.class));
                                    } catch (ClosedScopeException e) {
                                        e.printStackTrace();
                                    } catch (DefinitionParameterException e) {
                                        e.printStackTrace();
                                    } catch (NoBeanDefFoundException e) {
                                        e.printStackTrace();
                                    }
                                    return null;
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });
                try {
                    koinApplication.modules(Arrays.asList(module1, module2));
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Test
    public void can_add_modules_for_list() {
        org.koin.core.module.Module modA = Module.module(new ModuleDeclaration() {
            @Override
            public void invoke(org.koin.core.module.Module module) {
                try {
                    module.single(Simple.ComponentA.class, new Definition<Simple.ComponentA>() {
                        @Override
                        public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                            return new Simple.ComponentA();
                        }
                    });
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        org.koin.core.module.Module modB = Module.module(new ModuleDeclaration() {
            @Override
            public void invoke(org.koin.core.module.Module module) {
                try {
                    module.single(Simple.ComponentB.class, new Definition<Simple.ComponentB>() {
                        @Override
                        public Simple.ComponentB invoke(Scope scope, DefinitionParameters parameters) {
                            try {
                                return new Simple.ComponentB(scope.get(Simple.ComponentA.class));
                            } catch (ClosedScopeException e) {
                                e.printStackTrace();
                            } catch (DefinitionParameterException e) {
                                e.printStackTrace();
                            } catch (NoBeanDefFoundException e) {
                                e.printStackTrace();
                            }
                            return null;
                        }
                    });
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        Assert.assertEquals(modA.plus(modB), Arrays.asList(modA, modB));
    }

    @Test
    public void can_add_modules_to_list() {
        org.koin.core.module.Module modA = Module.module(new ModuleDeclaration() {
            @Override
            public void invoke(org.koin.core.module.Module module) {
                try {
                    module.single(Simple.ComponentA.class, new Definition<Simple.ComponentA>() {
                        @Override
                        public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                            return new Simple.ComponentA();
                        }
                    });
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        org.koin.core.module.Module modB = Module.module(new ModuleDeclaration() {
            @Override
            public void invoke(org.koin.core.module.Module module) {
                try {
                    module.single(Simple.ComponentB.class, new Definition<Simple.ComponentB>() {
                        @Override
                        public Simple.ComponentB invoke(Scope scope, DefinitionParameters parameters) {
                            try {
                                return new Simple.ComponentB(scope.get(Simple.ComponentA.class));
                            } catch (ClosedScopeException e) {
                                e.printStackTrace();
                            } catch (DefinitionParameterException e) {
                                e.printStackTrace();
                            } catch (NoBeanDefFoundException e) {
                                e.printStackTrace();
                            }
                            return null;
                        }
                    });
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        org.koin.core.module.Module modC = Module.module(new ModuleDeclaration() {
            @Override
            public void invoke(org.koin.core.module.Module module) {
                try {
                    module.single(Simple.ComponentC.class, new Definition<Simple.ComponentC>() {
                        @Override
                        public Simple.ComponentC invoke(Scope scope, DefinitionParameters parameters) {
                            try {
                                return new Simple.ComponentC(scope.get(Simple.ComponentB.class));
                            } catch (ClosedScopeException e) {
                                e.printStackTrace();
                            } catch (DefinitionParameterException e) {
                                e.printStackTrace();
                            } catch (NoBeanDefFoundException e) {
                                e.printStackTrace();
                            }
                            return null;
                        }
                    });
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        List<org.koin.core.module.Module> list1 = new ArrayList<>();
        list1.addAll(Arrays.asList(modA, modB));
        Assert.assertEquals(modA.plus(modB).add(modC), list1.add(modC));
    }

    @Test
    public void can_add_module_to_list() {
        org.koin.core.module.Module modA = Module.module(new ModuleDeclaration() {
            @Override
            public void invoke(org.koin.core.module.Module module) {
                try {
                    module.single(Simple.ComponentA.class, new Definition<Simple.ComponentA>() {
                        @Override
                        public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                            return new Simple.ComponentA();
                        }
                    });
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        org.koin.core.module.Module modB = Module.module(new ModuleDeclaration() {
            @Override
            public void invoke(org.koin.core.module.Module module) {
                try {
                    module.single(Simple.ComponentB.class, new Definition<Simple.ComponentB>() {
                        @Override
                        public Simple.ComponentB invoke(Scope scope, DefinitionParameters parameters) {
                            try {
                                return new Simple.ComponentB(scope.get(Simple.ComponentA.class));
                            } catch (ClosedScopeException e) {
                                e.printStackTrace();
                            } catch (DefinitionParameterException e) {
                                e.printStackTrace();
                            } catch (NoBeanDefFoundException e) {
                                e.printStackTrace();
                            }
                            return null;
                        }
                    });
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        org.koin.core.module.Module modC = Module.module(new ModuleDeclaration() {
            @Override
            public void invoke(org.koin.core.module.Module module) {
                try {
                    module.single(Simple.ComponentC.class, new Definition<Simple.ComponentC>() {
                        @Override
                        public Simple.ComponentC invoke(Scope scope, DefinitionParameters parameters) {
                            try {
                                return new Simple.ComponentC(scope.get(Simple.ComponentB.class));
                            } catch (ClosedScopeException e) {
                                e.printStackTrace();
                            } catch (DefinitionParameterException e) {
                                e.printStackTrace();
                            } catch (NoBeanDefFoundException e) {
                                e.printStackTrace();
                            }
                            return null;
                        }
                    });
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        List<org.koin.core.module.Module> list1 = modA.plus(modB);
        list1.add(modC);
        List<org.koin.core.module.Module> modules = Arrays.asList(modA, modB, modC);
        Assert.assertEquals(list1, modules);
    }
}
