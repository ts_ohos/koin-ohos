package org.koin.dsl;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.koin.Simple;
import org.koin.core.Koin;
import org.koin.core.KoinApplication;
import org.koin.core.definition.Definition;
import org.koin.core.error.*;
import org.koin.core.module.Module;
import org.koin.core.parameter.DefinitionParameters;
import org.koin.core.scope.Scope;

import static org.koin.core.context.DefaultContextExt.startKoin;
import static org.koin.core.context.DefaultContextExt.stopKoin;

public class ModuleRestartTest {

    @Before
    public void before() {
        try {
            startKoin(new org.koin.dsl.KoinAppDeclaration() {
                @Override
                public void invoke(KoinApplication koinApplication) {
                    Module module = org.koin.dsl.Module.module(new ModuleDeclaration() {
                        @Override
                        public void invoke(Module module) {
                            try {
                                module.single(Simple.ComponentA.class, new Definition<Simple.ComponentA>() {
                                    @Override
                                    public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                        return new Simple.ComponentA();
                                    }
                                });
                            } catch (DefinitionOverrideException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            });
        } catch (NoScopeDefFoundException e) {
            e.printStackTrace();
        } catch (ScopeAlreadyCreatedException e) {
            e.printStackTrace();
        } catch (KoinAppAlreadyStartedException e) {
            e.printStackTrace();
        }
    }

    @After
    public void after() {
        stopKoin();
    }

    @Test
    public void first_test() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        Koin koin = org.koin.dsl.KoinApplication.koinApplication(null).getKoin();
        try {
            koin.get(Simple.ComponentA.class);
        } catch (DefinitionParameterException e) {
            e.printStackTrace();
        } catch (NoBeanDefFoundException e) {
            e.printStackTrace();
        } catch (ClosedScopeException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void second_test() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        Koin koin = org.koin.dsl.KoinApplication.koinApplication(null).getKoin();
        try {
            koin.get(Simple.ComponentA.class);
        } catch (DefinitionParameterException e) {
            e.printStackTrace();
        } catch (NoBeanDefFoundException e) {
            e.printStackTrace();
        } catch (ClosedScopeException e) {
            e.printStackTrace();
        }
    }
}
