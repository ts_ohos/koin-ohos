package org.koin.dsl;

import org.junit.Assert;
import org.junit.Test;
import org.koin.core.Koin;
import org.koin.core.definition.Definition;
import org.koin.core.error.*;
import org.koin.core.parameter.DefinitionParameters;
import org.koin.core.qualifier.Qualifier;
import org.koin.core.scope.Scope;

import java.util.ArrayList;


public class ModuleSpecialRulesTest {

    @Test
    public void generic_type_declaration() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        Koin koin = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                try {
                    koinApplication.modules(Module.module(new ModuleDeclaration() {
                        @Override
                        public void invoke(org.koin.core.module.Module module) {
                            try {
                                module.single(ArrayList.class, new Definition<ArrayList>() {
                                    @Override
                                    public ArrayList invoke(Scope scope, DefinitionParameters parameters) {
                                        return new ArrayList<String>();
                                    }
                                });
                            } catch (DefinitionOverrideException e) {
                                e.printStackTrace();
                            }
                        }
                    }));
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        koin.get(ArrayList.class);
    }

    @Test
    public void generic_types_declaration() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        Koin koin = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.single(ArrayList.class, Qualifier.named("strings"), new Definition<ArrayList>() {
                                @Override
                                public ArrayList invoke(Scope scope, DefinitionParameters parameters) {
                                    return new ArrayList<String>();
                                }
                            });

                            module.single(ArrayList.class, Qualifier.named("ints"), new Definition<ArrayList>() {
                                @Override
                                public ArrayList invoke(Scope scope, DefinitionParameters parameters) {
                                    return new ArrayList<Integer>();
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        ArrayList strings = koin.get(ArrayList.class, Qualifier.named("strings"));
        strings.add("test");

        Assert.assertEquals(1, koin.get(ArrayList.class, Qualifier.named("strings")).size());
        Assert.assertEquals(0, koin.get(ArrayList.class, Qualifier.named("ints")).size());
    }
}
