package org.koin.dsl;

import org.junit.Assert;
import org.junit.Test;
import org.koin.Simple;
import org.koin.core.Koin;
import org.koin.core.definition.Definition;
import org.koin.core.error.*;
import org.koin.core.module.Module;
import org.koin.core.parameter.DefinitionParameters;
import org.koin.core.qualifier.Qualifier;
import org.koin.core.scope.Scope;

enum MyNames {
    MY_SCOPE,
    MY_STRING
}

public class NamingTest {

    @Test
    public void can_resolve_naming_from_root() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        Qualifier scopeName = Qualifier.named("MY_SCOPE");
        Koin koin = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                Module module = org.koin.dsl.Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(Module module) {
                        try {
                            module.single(Simple.MySingle.class, Qualifier.named("24"), new Definition<Simple.MySingle>() {
                                @Override
                                public Simple.MySingle invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Simple.MySingle(24);
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                        module.scope(scopeName, new Module.ScopeSet() {
                            @Override
                            public void invoke(ScopeDSL scopeDSL) {
                                try {
                                    scopeDSL.scoped(Simple.MySingle.class, new Definition<Simple.MySingle>() {
                                        @Override
                                        public Simple.MySingle invoke(Scope scope, DefinitionParameters parameters) {
                                            return new Simple.MySingle(42);
                                        }
                                    });
                                } catch (DefinitionOverrideException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                });
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        Scope scope = koin.createScope("myScope", scopeName);
        Simple.MySingle a = scope.get(Simple.MySingle.class, Qualifier.named("24"), null);
        Assert.assertEquals(24, a.getId());
        Simple.MySingle b = scope.get(Simple.MySingle.class);
        Assert.assertEquals(42, b.getId());
    }

    @Test
    public void enum_naming() {
        Assert.assertEquals("my_string", Qualifier.named(MyNames.MY_STRING).value);
    }

    @Test
    public void can_resolve_enum_naming() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        Koin koin = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                Module module = org.koin.dsl.Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(Module module) {
                        try {
                            module.single(Simple.MySingle.class, Qualifier.named(MyNames.MY_STRING), new Definition<Simple.MySingle>() {
                                @Override
                                public Simple.MySingle invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Simple.MySingle(24);
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        Assert.assertEquals(24, koin.get(Simple.MySingle.class, Qualifier.named(MyNames.MY_STRING)).getId());
    }

    @Test
    public void can_resolve_naming_with_q() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        Qualifier scopeName = Qualifier._q("MY_SCOPE");
        Koin koin = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                Module module = org.koin.dsl.Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(Module module) {
                        try {
                            module.single(Simple.MySingle.class, Qualifier._q("24"), new Definition<Simple.MySingle>() {
                                @Override
                                public Simple.MySingle invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Simple.MySingle(24);
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }

                        module.scope(scopeName, new Module.ScopeSet() {
                            @Override
                            public void invoke(ScopeDSL scopeDSL) {
                                try {
                                    scopeDSL.scoped(Simple.MySingle.class, new Definition<Simple.MySingle>() {
                                        @Override
                                        public Simple.MySingle invoke(Scope scope, DefinitionParameters parameters) {
                                            return new Simple.MySingle(42);
                                        }
                                    });
                                } catch (DefinitionOverrideException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                });
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        Scope scope = koin.createScope("myScope", scopeName);
        Simple.MySingle a = scope.get(Simple.MySingle.class, Qualifier.named("24"), null);
        Assert.assertEquals(24, a.getId());
        Simple.MySingle b = scope.get(Simple.MySingle.class);
        Assert.assertEquals(42, b.getId());
    }
}
