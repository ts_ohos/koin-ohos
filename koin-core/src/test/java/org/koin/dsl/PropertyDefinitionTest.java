package org.koin.dsl;

import org.junit.Assert;
import org.junit.Test;
import org.koin.core.Koin;
import org.koin.core.error.NoScopeDefFoundException;
import org.koin.core.error.ScopeAlreadyCreatedException;

import java.util.HashMap;

public class PropertyDefinitionTest {

    @Test
    public void load_and_get_properties() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        String key = "KEY";
        String value = "VALUE";
        HashMap<String, String> values = new HashMap<>();
        values.put(key, value);

        Koin koin = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.properties(values);
            }
        }).getKoin();

        String gotValue = koin.<String>getProperty(key);

        Assert.assertEquals(value, gotValue);

    }

    @Test
    public void default_value_properties() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        Koin koin = KoinApplication.koinApplication(null).getKoin();

        String defaultValue = "defaultValue";
        String gotValue = koin.getProperty("aKey", defaultValue);

        Assert.assertEquals(defaultValue, gotValue);
    }

    @Test
    public void set_a_property() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        String key = "KEY";
        String value = "VALUE";

        Koin koin = KoinApplication.koinApplication(null).getKoin();

        koin.setProperty(key, value);
        String gotValue = koin.<String>getProperty(key);
        Assert.assertEquals(value, gotValue);
    }

    @Test
    public void missing_property() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        String key = "KEY";
        Koin koin = KoinApplication.koinApplication(null).getKoin();

        String gotValue = koin.<String>getProperty(key);
        Assert.assertNull(gotValue);
    }

    @Test
    public void overwrite_a_property() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        String key = "KEY";
        String value = "VALUE";
        String value2 = "VALUE2";

        HashMap<String, String> values = new HashMap<>();
        values.put(key, value);

        Koin koin = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.properties(values);
            }
        }).getKoin();

        koin.setProperty(key, value2);
        String gotValue = koin.<String>getProperty(key);
        Assert.assertEquals(value2, gotValue);
    }
}
