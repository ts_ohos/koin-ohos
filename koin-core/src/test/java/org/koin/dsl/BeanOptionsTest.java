package org.koin.dsl;

import org.junit.Assert;
import org.junit.Test;
import org.koin.Simple;
import org.koin.core.definition.BeanDefinition;
import org.koin.core.definition.Definition;
import org.koin.core.error.*;
import org.koin.core.module.Module;
import org.koin.core.parameter.DefinitionParameters;
import org.koin.core.scope.Scope;
import org.koin.test.KoinApplicationExt;

public class BeanOptionsTest {

    @Test
    public void definition_created_at_start() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        org.koin.core.KoinApplication app = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                Module module = org.koin.dsl.Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(Module module) {
                        try {
                            module.single(Simple.ComponentA.class, null, true, false, new Definition<Simple.ComponentA>() {
                                @Override
                                public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Simple.ComponentA();
                                }
                            });

                            module.single(Simple.ComponentB.class, new Definition<Simple.ComponentB>() {
                                @Override
                                public Simple.ComponentB invoke(Scope scope, DefinitionParameters parameters) {
                                    try {
                                        return new Simple.ComponentB(scope.get(Simple.ComponentA.class));
                                    } catch (ClosedScopeException e) {
                                        e.printStackTrace();
                                    } catch (DefinitionParameterException e) {
                                        e.printStackTrace();
                                    } catch (NoBeanDefFoundException e) {
                                        e.printStackTrace();
                                    }
                                    return null;
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        BeanDefinition defA = KoinApplicationExt.getBeanDefinition(app.getKoin(), Simple.ComponentA.class);
        if (defA == null) {
            throw new IllegalStateException("no definition found");
        }
        Assert.assertTrue(defA.getOptions().isCreatedAtStart());

        BeanDefinition defB = KoinApplicationExt.getBeanDefinition(app.getKoin(), Simple.ComponentB.class);
        if (defB == null) {
            throw new IllegalStateException("no definition found");
        }
        Assert.assertFalse(defB.getOptions().isCreatedAtStart());
    }

    @Test
    public void definition_override() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        org.koin.core.KoinApplication app = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                Module module = org.koin.dsl.Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(Module module) {
                        try {
                            module.single(Simple.ComponentA.class, new Definition<Simple.ComponentA>() {
                                @Override
                                public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Simple.ComponentA();
                                }
                            });
                            module.single(Simple.ComponentB.class, null, false, true, new Definition<Simple.ComponentB>() {
                                @Override
                                public Simple.ComponentB invoke(Scope scope, DefinitionParameters parameters) {
                                    try {
                                        return new Simple.ComponentB(scope.get(Simple.ComponentA.class));
                                    } catch (ClosedScopeException e) {
                                        e.printStackTrace();
                                    } catch (DefinitionParameterException e) {
                                        e.printStackTrace();
                                    } catch (NoBeanDefFoundException e) {
                                        e.printStackTrace();
                                    }
                                    return null;
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }

                    }
                });
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });
        BeanDefinition defA = KoinApplicationExt.getBeanDefinition(app.getKoin(), Simple.ComponentA.class);
        if (defA == null) {
            throw new IllegalStateException("no definition found");
        }
        Assert.assertFalse(defA.getOptions().isOverride());

        BeanDefinition defB = KoinApplicationExt.getBeanDefinition(app.getKoin(), Simple.ComponentB.class);
        if (defB == null) {
            throw new IllegalStateException("no definition found");
        }
        Assert.assertTrue(defB.getOptions().isOverride());
    }
}
