package org.koin.dsl;

import org.junit.Assert;
import org.junit.Test;
import org.koin.Simple;
import org.koin.core.Koin;
import org.koin.core.definition.Definition;
import org.koin.core.error.*;
import org.koin.core.logger.Level;
import org.koin.core.module.Module;
import org.koin.core.parameter.DefinitionParameters;
import org.koin.core.qualifier.Qualifier;
import org.koin.core.scope.Scope;
import org.koin.test.KoinApplicationExt;

import static org.junit.Assert.fail;

public class ModuleDeclarationRulesTest {

    @Test
    public void do_not_allow_redeclaration() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {

        KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                Module module = org.koin.dsl.Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(Module module) {
                        try {
                            module.single(Simple.ComponentA.class, new Definition<Simple.ComponentA>() {
                                @Override
                                public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Simple.ComponentA();
                                }
                            });

                            module.single(Simple.ComponentA.class, new Definition<Simple.ComponentA>() {
                                @Override
                                public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Simple.ComponentA();
                                }
                            });
                            fail("should not redeclare");
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });

                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Test
    public void allow_redeclaration_different_names() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        org.koin.core.KoinApplication app = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger(Level.INFO);
                Module module = org.koin.dsl.Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(Module module) {
                        try {
                            module.single(Simple.ComponentA.class, Qualifier.named("default"), new Definition<Simple.ComponentA>() {
                                @Override
                                public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Simple.ComponentA();
                                }
                            });

                            module.single(Simple.ComponentA.class, Qualifier.named("other"), new Definition<Simple.ComponentA>() {
                                @Override
                                public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Simple.ComponentA();
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });
        KoinApplicationExt.assertDefinitionsCount(app.getKoin(), 2);
    }

    @Test
    public void allow_qualifier_redeclaration_same_names() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        Koin koin = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                Module module = org.koin.dsl.Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(Module module) {
                        try {
                            module.single(Simple.ComponentA.class, Qualifier.named("default"), new Definition<Simple.ComponentA>() {
                                @Override
                                public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Simple.ComponentA();
                                }
                            });

                            module.single(Simple.ComponentB.class, Qualifier.named("default"), new Definition<Simple.ComponentB>() {
                                @Override
                                public Simple.ComponentB invoke(Scope scope, DefinitionParameters parameters) {
                                    try {
                                        return new Simple.ComponentB(scope.get(Simple.ComponentA.class, Qualifier.named("default")));
                                    } catch (ClosedScopeException e) {
                                        e.printStackTrace();
                                    } catch (DefinitionParameterException e) {
                                        e.printStackTrace();
                                    } catch (NoBeanDefFoundException e) {
                                        e.printStackTrace();
                                    }
                                    return null;
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();
        try {
            Simple.ComponentA a = koin.get(Simple.ComponentA.class, Qualifier.named("default"));
            Simple.ComponentB b = koin.get(Simple.ComponentB.class, Qualifier.named("default"));
            Assert.assertEquals(a, b.a);
        } catch (DefinitionParameterException e) {
            e.printStackTrace();
        } catch (NoBeanDefFoundException e) {
            e.printStackTrace();
        } catch (ClosedScopeException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void allow_redeclaration_default() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        org.koin.core.KoinApplication app = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                Module module = org.koin.dsl.Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(Module module) {
                        try {
                            module.single(Simple.ComponentA.class, new Definition<Simple.ComponentA>() {
                                @Override
                                public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Simple.ComponentA();
                                }
                            });

                            module.single(Simple.ComponentA.class, Qualifier.named("other"), new Definition<Simple.ComponentA>() {
                                @Override
                                public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Simple.ComponentA();
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });
        KoinApplicationExt.assertDefinitionsCount(app.getKoin(), 2);
    }

    @Test
    public void do_not_allow_redeclaration_with_different_implementation() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {

        KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                Module module = org.koin.dsl.Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(Module module) {
                        try {
                            module.single(Simple.ComponentInterface1.class, new Definition<Simple.ComponentInterface1>() {
                                @Override
                                public Simple.ComponentInterface1 invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Simple.Component1();
                                }
                            });

                            module.single(Simple.ComponentInterface1.class, new Definition<Simple.ComponentInterface1>() {
                                @Override
                                public Simple.ComponentInterface1 invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Simple.Component2();
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
