package org.koin.dsl;

import org.junit.Assert;
import org.junit.Test;
import org.koin.Simple;
import org.koin.core.Koin;
import org.koin.core.definition.Definition;
import org.koin.core.error.*;
import org.koin.core.module.Module;
import org.koin.core.parameter.DefinitionParameters;
import org.koin.core.scope.Scope;

import java.util.HashMap;

import static org.junit.Assert.fail;

public class ModuleAndPropertiesTest {

    @Test
    public void get_a_property_from_a_module() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        String key = "KEY";
        String value = "VALUE";
        HashMap<String, String> values = new HashMap<>();
        values.put(key, value);

        Koin koin = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.properties(values);
                Module module = org.koin.dsl.Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(Module module) {
                        try {
                            module.single(Simple.MyStringFactory.class, new Definition<Simple.MyStringFactory>() {
                                @Override
                                public Simple.MyStringFactory invoke(Scope scope, DefinitionParameters parameters) {
                                    try {
                                        return new Simple.MyStringFactory(scope.getProperty(key));
                                    } catch (MissingPropertyException e) {
                                        e.printStackTrace();
                                    }
                                    return null;
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        }).getKoin();

        try {
            Simple.MyStringFactory fact = koin.get(Simple.MyStringFactory.class);
            Assert.assertEquals(value, fact.s);
        } catch (DefinitionParameterException e) {
            e.printStackTrace();
        } catch (NoBeanDefFoundException e) {
            e.printStackTrace();
        } catch (ClosedScopeException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void missing_property_from_a_module() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        String key = "KEY";

        Koin koin = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                Module module = org.koin.dsl.Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(Module module) {
                        try {
                            module.single(Simple.MyStringFactory.class, new Definition<Simple.MyStringFactory>() {
                                @Override
                                public Simple.MyStringFactory invoke(Scope scope, DefinitionParameters parameters) {
                                    try {
                                        return new Simple.MyStringFactory(scope.getProperty(key));
                                    } catch (MissingPropertyException e) {
                                        e.printStackTrace();
                                    }
                                    return null;
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        }).getKoin();

        try {
            koin.get(Simple.MyStringFactory.class);
            fail();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
