package org.koin.dsl;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.koin.core.error.KoinAppAlreadyStartedException;
import org.koin.core.error.NoScopeDefFoundException;
import org.koin.core.error.ScopeAlreadyCreatedException;
import org.koin.core.logger.Level;
import org.koin.mp.KoinPlatformTools;
import org.koin.test.Asserts;
import org.koin.test.KoinApplicationExt;

import static org.junit.Assert.fail;
import static org.koin.core.context.DefaultContextExt.startKoin;
import static org.koin.core.context.DefaultContextExt.stopKoin;

public class KoinAppCreationTest {

    @After
    public void after() {
        stopKoin();
    }

    @Test
    public void make_a_koin_application() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        org.koin.core.KoinApplication app = KoinApplication.koinApplication(null);

        KoinApplicationExt.assertDefinitionsCount(app.getKoin(), 0);

        Asserts.assertHasNoStandaloneInstance();
    }

    @Test
    public void start_a_koin_application() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, KoinAppAlreadyStartedException {
        org.koin.core.KoinApplication app = startKoin(new org.koin.dsl.KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
            }
        });

        Assert.assertEquals(KoinPlatformTools.INSTANCE().defaultContext().get(), app.getKoin());
        stopKoin();

        Asserts.assertHasNoStandaloneInstance();
    }

    @Test
    public void can_not_restart_a_koin_application() {
        try {
            startKoin(new org.koin.dsl.KoinAppDeclaration() {
                @Override
                public void invoke(org.koin.core.KoinApplication koinApplication) {

                }
            });
        } catch (NoScopeDefFoundException e) {
            e.printStackTrace();
        } catch (ScopeAlreadyCreatedException e) {
            e.printStackTrace();
        } catch (KoinAppAlreadyStartedException e) {
            e.printStackTrace();
        }

        try {
            startKoin(new org.koin.dsl.KoinAppDeclaration() {
                @Override
                public void invoke(org.koin.core.KoinApplication koinApplication) {

                }
            });
            fail("should throw KoinAppAlreadyStartedException");
        } catch (NoScopeDefFoundException e) {
            e.printStackTrace();
        } catch (ScopeAlreadyCreatedException e) {
            e.printStackTrace();
        } catch (KoinAppAlreadyStartedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void allow_declare_a_logger() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, KoinAppAlreadyStartedException {
        startKoin(new org.koin.dsl.KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.logger(KoinPlatformTools.INSTANCE().defaultLogger(Level.ERROR));
            }
        });

        Assert.assertEquals(KoinPlatformTools.INSTANCE().defaultContext().get().getLogger().getLevel(), Level.ERROR);

        KoinPlatformTools.INSTANCE().defaultContext().get().getLogger().debug("debug");
        KoinPlatformTools.INSTANCE().defaultContext().get().getLogger().info("info");
        KoinPlatformTools.INSTANCE().defaultContext().get().getLogger().error("error");
    }
}
