package org.koin.dsl;

import org.junit.Assert;
import org.junit.Test;
import org.koin.Simple;
import org.koin.core.Koin;
import org.koin.core.KoinApplication;
import org.koin.core.annotation.KoinInternalApi;
import org.koin.core.definition.Definition;
import org.koin.core.error.*;
import org.koin.core.logger.Level;
import org.koin.core.module.Module;
import org.koin.core.parameter.DefinitionParameters;
import org.koin.core.qualifier.Qualifier;
import org.koin.core.scope.Scope;
import org.koin.core.scope.ScopeDefinition;
import org.koin.core.setterinjecttest.A;

import java.util.Collection;

import static org.junit.Assert.fail;
import static org.koin.core.context.DefaultContextExt.startKoin;
import static org.koin.core.context.DefaultContextExt.stopKoin;

@KoinInternalApi
public class ScopeSetDeclarationTest {

    public static Qualifier scopeKey = Qualifier.named("KEY");

    @Test
    public void can_declare_a_scoped_definition() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        Koin koin = org.koin.dsl.KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {
                Module module = org.koin.dsl.Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(Module module) {
                        module.scope(scopeKey, new Module.ScopeSet() {
                            @Override
                            public void invoke(ScopeDSL scopeDSL) {
                                try {
                                    scopeDSL.scoped(Simple.ComponentA.class, new Definition<Simple.ComponentA>() {
                                        @Override
                                        public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                            return new Simple.ComponentA();
                                        }
                                    });
                                } catch (DefinitionOverrideException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                });

                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        ScopeDefinition def = null;
        Collection<ScopeDefinition> values = koin.getScopeRegistry().getScopeDefinitions().values();
        for (ScopeDefinition scopeDefinition : values) {
            if (scopeDefinition.getQualifier().value.equals(scopeKey.value)) {
                def = scopeDefinition;
                return;
            }
        }
        Assert.assertTrue(def.getQualifier().value.equals(scopeKey.value));

        Scope scope = koin.createScope("id", scopeKey);
        Assert.assertTrue(scope.getScopeDefinition().equals(def));
    }

    @Test
    public void can_declare_2_scoped_definitions_from_same_type_without_naming() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        Koin koin = org.koin.dsl.KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {
                Module module = org.koin.dsl.Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(Module module) {
                        module.scope(Qualifier.named("B"), new Module.ScopeSet() {
                            @Override
                            public void invoke(ScopeDSL scopeDSL) {
                                try {
                                    scopeDSL.scoped(Simple.ComponentA.class, new Definition<Simple.ComponentA>() {
                                        @Override
                                        public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                            return new Simple.ComponentA();
                                        }
                                    });
                                } catch (DefinitionOverrideException e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                        module.scope(Qualifier.named("A"), new Module.ScopeSet() {
                            @Override
                            public void invoke(ScopeDSL scopeDSL) {
                                try {
                                    scopeDSL.scoped(Simple.ComponentA.class, new Definition<Simple.ComponentA>() {
                                        @Override
                                        public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                            return new Simple.ComponentA();
                                        }
                                    });
                                } catch (DefinitionOverrideException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                });
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        ScopeDefinition defA = null;
        ScopeDefinition defB = null;
        Collection<ScopeDefinition> values = koin.getScopeRegistry().getScopeDefinitions().values();
        for (ScopeDefinition scopeDefinition : values) {
            if (scopeDefinition.getQualifier().value.equals(Qualifier._q("A").value)) {
                defA = scopeDefinition;
                return;
            }
        }
        Assert.assertTrue(defA.getQualifier().equals(Qualifier._q("A")));

        for (ScopeDefinition scopeDefinition : values) {
            if (scopeDefinition.getQualifier().value.equals(Qualifier._q("B").value)) {
                defB = scopeDefinition;
                return;
            }
        }
        Assert.assertTrue(defB.getQualifier().equals(Qualifier._q("B")));

        Simple.ComponentA scopeA = koin.createScope("A", Qualifier.named("A")).get(Simple.ComponentA.class);
        Simple.ComponentA scopeB = koin.createScope("B", Qualifier.named("B")).get(Simple.ComponentA.class);
        Assert.assertNotEquals(scopeA, scopeB);
    }

    @Test
    public void can_declare_a_scope_definition() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        Koin koin = org.koin.dsl.KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {
                Module module = org.koin.dsl.Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(Module module) {
                        module.scope(scopeKey, new Module.ScopeSet() {
                            @Override
                            public void invoke(ScopeDSL scopeDSL) {
                                try {
                                    scopeDSL.scoped(A.class, new Definition<A>() {
                                        @Override
                                        public A invoke(Scope scope, DefinitionParameters parameters) {
                                            return new A();
                                        }
                                    });
                                } catch (DefinitionOverrideException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                });
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        ScopeDefinition def = null;
        Collection<ScopeDefinition> values = koin.getScopeRegistry().getScopeDefinitions().values();
        for (ScopeDefinition scopeDefinition : values) {
            if (scopeDefinition.getQualifier().value.equals(scopeKey.value)) {
                def = scopeDefinition;
                return;
            }
        }
        Assert.assertTrue(def.getQualifier().equals(scopeKey));
    }

    @Test
    public void cannot_declare_2_scoped_same_definitions() {
        try {
            startKoin(new KoinAppDeclaration() {
                @Override
                public void invoke(KoinApplication koinApplication) {
                    koinApplication.printLogger(Level.DEBUG);
                    Module module = org.koin.dsl.Module.module(new ModuleDeclaration() {
                        @Override
                        public void invoke(Module module) {
                            module.scope(scopeKey, new Module.ScopeSet() {
                                @Override
                                public void invoke(ScopeDSL scopeDSL) {
                                    try {
                                        scopeDSL.scoped(Simple.ComponentA.class, new Definition<Simple.ComponentA>() {
                                            @Override
                                            public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                                return new Simple.ComponentA();
                                            }
                                        });
                                        scopeDSL.scoped(Simple.ComponentA.class, new Definition<Simple.ComponentA>() {
                                            @Override
                                            public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                                return new Simple.ComponentA();
                                            }
                                        });
                                        fail();
                                    } catch (DefinitionOverrideException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        }
                    });
                    try {
                        koinApplication.modules(module);
                    } catch (DefinitionOverrideException e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
        stopKoin();
    }

}
