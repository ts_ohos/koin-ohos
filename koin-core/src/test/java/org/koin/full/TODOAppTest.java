package org.koin.full;

import org.junit.Test;
import org.koin.core.Koin;
import org.koin.core.KoinApplication;
import org.koin.core.definition.Definition;
import org.koin.core.error.*;
import org.koin.core.logger.Level;
import org.koin.core.module.Module;
import org.koin.core.parameter.DefinitionParameters;
import org.koin.core.scope.Scope;
import org.koin.dsl.KoinAppDeclaration;
import org.koin.dsl.ModuleDeclaration;

import static org.junit.Assert.assertTrue;
import static org.koin.dsl.KoinApplication.koinApplication;
import static org.koin.dsl.Module.module;

public class TODOAppTest {

    Module defaultModule = module(new ModuleDeclaration() {
        @Override
        public void invoke(Module module) {
            try {
                module.single(TasksRepository.class, new Definition<TasksRepository>() {
                    @Override
                    public TasksRepository invoke(Scope scope, DefinitionParameters parameters) {
                        try {
                            return new TasksRepository(scope.get(TasksDataSource.class));
                        } catch (ClosedScopeException e) {
                            e.printStackTrace();
                        } catch (DefinitionParameterException e) {
                            e.printStackTrace();
                        } catch (NoBeanDefFoundException e) {
                            e.printStackTrace();
                        }
                        return null;
                    }
                });

                module.single(TasksDataSource.class, new Definition<TasksDataSource>() {
                    @Override
                    public TasksDataSource invoke(Scope scope, DefinitionParameters parameters) {
                        return new FakeTasksRemoteDataSource();
                    }
                });

            } catch (DefinitionOverrideException e) {
                e.printStackTrace();
            }
        }
    });
    Module overrideModule = module(true, new ModuleDeclaration() {
        @Override
        public void invoke(Module module) {
            try {
                module.single(TasksDataSource.class, new Definition<TasksDataSource>() {
                    @Override
                    public TasksDataSource invoke(Scope scope, DefinitionParameters parameters) {
                        return new TasksLocalDataSource();
                    }
                });
            } catch (DefinitionOverrideException e) {
                e.printStackTrace();
            }
        }
    });
    Module overrideDef = module(new ModuleDeclaration() {
        @Override
        public void invoke(Module module) {
            try {
                module.single(TasksDataSource.class, null, false, true, new Definition<TasksDataSource>() {
                    @Override
                    public TasksDataSource invoke(Scope scope, DefinitionParameters parameters) {
                        return new TasksLocalDataSource();
                    }
                });
            } catch (DefinitionOverrideException e) {
                e.printStackTrace();
            }
        }
    });

    @Test
    public void default_module() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        Koin koin = koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                try {
                    koinApplication.modules(defaultModule);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        TasksDataSource tasksDataSource = koin.<TasksDataSource>get(TasksDataSource.class);
        assertTrue(tasksDataSource instanceof FakeTasksRemoteDataSource);
    }

    @Test
    public void overloaded_module() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        Koin koin = koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                try {
                    koinApplication.modules(defaultModule, overrideModule);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        TasksDataSource tasksDataSource = koin.get(TasksDataSource.class);
        assertTrue(tasksDataSource instanceof TasksLocalDataSource);
    }

    @Test
    public void overloaded_definition() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        Koin koin = koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);

                try {
                    koinApplication.modules(defaultModule, overrideDef);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        TasksDataSource tasksDataSource = koin.get(TasksDataSource.class);
        assertTrue(tasksDataSource instanceof TasksLocalDataSource);

    }

    public interface TasksDataSource {
    }

    public static class FakeTasksRemoteDataSource implements TasksDataSource {

    }

    public static class TasksLocalDataSource implements TasksDataSource {

    }

    public static class TasksRepository {

        public TasksRepository(TasksDataSource dataSource) {

        }
    }
}
