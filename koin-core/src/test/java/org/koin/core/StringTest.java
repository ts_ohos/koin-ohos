package org.koin.core;

import org.junit.Test;
import org.koin.ext.StringUtil;

public class StringTest {

    @Test
    public void quoted_strings() {

        assert StringUtil.clearQuotes("test") == "test";
        assert StringUtil.clearQuotes("te\"st") == "te\"st";
        assert StringUtil.clearQuotes("\"test\"").equals("test");
    }
}
