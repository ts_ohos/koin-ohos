package org.koin.core;

import org.junit.Assert;
import org.junit.Test;
import org.koin.Simple;
import org.koin.core.definition.Definition;
import org.koin.core.error.*;
import org.koin.core.logger.Level;
import org.koin.core.module.Module;
import org.koin.core.parameter.DefinitionParameters;
import org.koin.core.qualifier.Qualifier;
import org.koin.core.scope.Scope;
import org.koin.core.setterinjecttest.B;
import org.koin.dsl.KoinAppDeclaration;
import org.koin.dsl.KoinApplication;
import org.koin.dsl.ModuleDeclaration;
import org.koin.dsl.ScopeDSL;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.fail;
import static org.koin.dsl.Module.module;

public class DeclareInstanceTest {

    @Test
    public void can_declare_a_single_on_the_fly() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionOverrideException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {

        Koin koin = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger();
                try {
                    koinApplication.modules(new ArrayList<>());
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        Simple.ComponentA a = new Simple.ComponentA();
        koin.declare(Simple.ComponentA.class, a);

        Assert.assertEquals(a, koin.get(Simple.ComponentA.class));
    }

    @Test
    public void can_not_declare_a_single_on_the_fly() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        Koin koin = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                try {
                    koinApplication.modules(module(new ModuleDeclaration() {
                        @Override
                        public void invoke(Module module) {
                            try {
                                module.single(Simple.ComponentA.class, new Definition<Simple.ComponentA>() {
                                    @Override
                                    public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                        return new Simple.ComponentA();
                                    }
                                });
                            } catch (DefinitionOverrideException e) {
                                e.printStackTrace();
                            }
                        }
                    }));
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        Simple.ComponentA a = new Simple.ComponentA();

        try {
            koin.declare(Simple.ComponentA.class, a);
            fail();
        } catch (DefinitionOverrideException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void can_declare_and_override_a_single_on_the_fly() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionOverrideException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        Koin koin = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger();
                try {
                    koinApplication.modules(module(new ModuleDeclaration() {
                        @Override
                        public void invoke(Module module) {
                            try {
                                module.single(Simple.MySingle.class, new Definition<Simple.MySingle>() {
                                    @Override
                                    public Simple.MySingle invoke(Scope scope, DefinitionParameters parameters) {
                                        return new Simple.MySingle(1);
                                    }
                                });
                            } catch (DefinitionOverrideException e) {
                                e.printStackTrace();
                            }
                        }
                    }));
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        Simple.MySingle a = new Simple.MySingle(2);

        koin.declare(Simple.MySingle.class, a, null, new ArrayList<>(), true);
        Assert.assertEquals(2, koin.get(a.getClass()).getId());
    }

    @Test
    public void can_declare_and_override_a_single_on_the_fly_when_override_is_set_to_false() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        Koin koin = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger();
                try {
                    koinApplication.modules(module(new ModuleDeclaration() {
                        @Override
                        public void invoke(Module module) {
                            try {
                                module.single(Simple.MySingle.class, new Definition<Simple.MySingle>() {
                                    @Override
                                    public Simple.MySingle invoke(Scope scope, DefinitionParameters parameters) {
                                        return new Simple.MySingle(1);
                                    }
                                });
                            } catch (DefinitionOverrideException e) {
                                e.printStackTrace();
                            }
                        }
                    }));
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        Simple.MySingle a = new Simple.MySingle(2);

        try {
            koin.declare(Simple.MySingle.class, a, null, new ArrayList<>(), false);
            fail();
        } catch (DefinitionOverrideException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void can_declare_a_single_with_qualifier_on_the_fly() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionOverrideException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        Koin koin = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger();
                try {
                    koinApplication.modules(module(new ModuleDeclaration() {
                        @Override
                        public void invoke(Module module) {
                            try {
                                module.single(Simple.ComponentA.class, new Definition<Simple.ComponentA>() {
                                    @Override
                                    public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                        return new Simple.ComponentA();
                                    }
                                });
                            } catch (DefinitionOverrideException e) {
                                e.printStackTrace();
                            }
                        }
                    }));
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        Simple.ComponentA a = new Simple.ComponentA();

        koin.declare(Simple.ComponentA.class, a, Qualifier.named("another_a"), new ArrayList<>(), false);
        Assert.assertEquals(a, koin.get(Simple.ComponentA.class, Qualifier.named("another_a"), null));
        Assert.assertNotEquals(a, koin.get(Simple.ComponentA.class));
    }

    @Test
    public void can_declare_and_override_a_single_with_qualifier_on_the_fly() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionOverrideException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        Koin koin = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger();
                try {
                    koinApplication.modules(module(new ModuleDeclaration() {
                        @Override
                        public void invoke(Module module) {
                            try {
                                module.single(Simple.ComponentA.class, new Definition<Simple.ComponentA>() {
                                    @Override
                                    public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                        return new Simple.ComponentA();
                                    }
                                });
                                module.single(Simple.ComponentA.class, Qualifier.named("another_a"), false, false, new Definition<Simple.ComponentA>() {
                                    @Override
                                    public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                        return new Simple.ComponentA();
                                    }
                                });
                            } catch (DefinitionOverrideException e) {
                                e.printStackTrace();
                            }
                        }
                    }));
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        Simple.ComponentA a = new Simple.ComponentA();
        koin.declare(Simple.ComponentA.class, a, Qualifier.named("another_a"), new ArrayList<>(), true);

        Assert.assertEquals(a, koin.get(Simple.ComponentA.class, Qualifier.named("another_a"), null));
        Assert.assertNotEquals(a, koin.get(Simple.ComponentA.class));
    }

    @Test
    public void can_declare_a_single_with_secondary_type_on_the_fly() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException, DefinitionOverrideException {
        Koin koin = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger();
                try {
                    koinApplication.modules(new ArrayList<>());
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        Simple.Component1 a = new Simple.Component1();

        koin.declare(Simple.Component1.class, a, null, Arrays.asList(Simple.ComponentInterface1.class), false);

        Assert.assertEquals(a, koin.get(Simple.Component1.class));
        Assert.assertEquals(a, koin.get(Simple.ComponentInterface1.class));
    }

    @Test
    public void can_override_a_single_on_the_fly() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionOverrideException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        Koin koin = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                try {
                    koinApplication.modules(new ArrayList<>());
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        Simple.Component1 a = new Simple.Component1();
        Simple.Component1 b = new Simple.Component1();

        koin.declare(Simple.Component1.class, a);
        koin.declare(Simple.Component1.class, b, null, new ArrayList<>(), true);

        Assert.assertEquals(b, koin.get(Simple.Component1.class));
    }

    @Test
    public void can_declare_and_override_a_single_with_secondary_type_on_the_fly() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionOverrideException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        Koin koin = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                try {
                    koinApplication.modules(new ArrayList<>());
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        Simple.Component1 a = new Simple.Component1();
        Simple.Component1 b = new Simple.Component1();

        koin.declare(Simple.Component1.class, a, null, Arrays.asList(Simple.ComponentInterface1.class), false);
        koin.declare(Simple.Component1.class, b, null, Arrays.asList(Simple.ComponentInterface1.class), true);

        Assert.assertEquals(b, koin.get(Simple.Component1.class));
        Assert.assertEquals(koin.get(Simple.Component1.class), koin.get(Simple.Component1.class));
        Assert.assertEquals(b, koin.get(Simple.ComponentInterface1.class));
    }

    @Test
    public void can_declare_a_scoped_on_the_fly() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionOverrideException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {

        Koin koin = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger();
                try {
                    koinApplication.modules(module(new ModuleDeclaration() {
                        @Override
                        public void invoke(Module module) {
                            module.scope(Qualifier.named("Session"), new Module.ScopeSet() {
                                @Override
                                public void invoke(ScopeDSL scopeDSL) {
                                    try {
                                        scopeDSL.scoped(Simple.ComponentB.class, new Definition<Simple.ComponentB>() {
                                            @Override
                                            public Simple.ComponentB invoke(Scope scope, DefinitionParameters parameters) {
                                                try {
                                                    return new Simple.ComponentB(scope.get(Simple.ComponentA.class));
                                                } catch (ClosedScopeException e) {
                                                    e.printStackTrace();
                                                } catch (DefinitionParameterException e) {
                                                    e.printStackTrace();
                                                } catch (NoBeanDefFoundException e) {
                                                    e.printStackTrace();
                                                }
                                                return null;
                                            }
                                        });
                                    } catch (DefinitionOverrideException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        }
                    }));
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        Simple.ComponentA a = new Simple.ComponentA();

        Scope session1 = koin.createScope("session1", Qualifier.named("Session"), null);
        session1.declare(Simple.ComponentA.class, a, null, null, false);

        Assert.assertEquals(a, session1.get(Simple.ComponentA.class));
        Assert.assertEquals(a, ((Simple.ComponentB) session1.get(Simple.ComponentB.class)).a);

    }

    @Test
    public void can_declare_a_scoped_on_the_fly_with_primary_type() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionOverrideException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        Koin koin = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger();

                try {
                    koinApplication.modules(module(new ModuleDeclaration() {
                        @Override
                        public void invoke(Module module) {
                            module.scope(Qualifier.named("Session"), new Module.ScopeSet() {
                                @Override
                                public void invoke(ScopeDSL scopeDSL) {
                                    try {
                                        scopeDSL.scoped(B.class, new Definition<B>() {
                                            @Override
                                            public B invoke(Scope scope, DefinitionParameters parameters) {
                                                return new B();
                                            }
                                        });
                                    } catch (DefinitionOverrideException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        }
                    }));
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        Simple.Component2 a = new Simple.Component2();

        Scope session1 = koin.createScope("session1", Qualifier.named("Session"), null);

        session1.declare(Simple.ComponentInterface1.class, a, Qualifier.named("another_a"), null, false);
        Assert.assertEquals(a, session1.get(Simple.ComponentInterface1.class, Qualifier.named("another_a"), null));
    }

    @Test
    public void can_not_declare_a_scoped_single_on_the_fly() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionOverrideException {
        Koin koin = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger();
                try {
                    koinApplication.modules(module(new ModuleDeclaration() {
                        @Override
                        public void invoke(Module module) {
                            module.scope(Qualifier.named("Session"), new Module.ScopeSet() {
                                @Override
                                public void invoke(ScopeDSL scopeDSL) {
                                    try {
                                        scopeDSL.scoped(B.class, new Definition<B>() {
                                            @Override
                                            public B invoke(Scope scope, DefinitionParameters parameters) {
                                                return new B();
                                            }
                                        });
                                    } catch (DefinitionOverrideException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        }
                    }));
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        Simple.ComponentA a = new Simple.ComponentA();

        Scope session1 = koin.createScope("session1", Qualifier.named("Session"), null);

        session1.declare(Simple.ComponentA.class, a, null, null, false);
        try {
            koin.get(Simple.ComponentA.class);
            fail();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void can_not_declare_a_other_scoped_on_the_fly() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionOverrideException {
        Koin koin = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger();

                try {
                    koinApplication.modules(module(new ModuleDeclaration() {
                        @Override
                        public void invoke(Module module) {
                            module.scope(Qualifier.named("Session"), new Module.ScopeSet() {
                                @Override
                                public void invoke(ScopeDSL scopeDSL) {
                                    try {
                                        scopeDSL.scoped(B.class, new Definition<B>() {
                                            @Override
                                            public B invoke(Scope scope, DefinitionParameters parameters) {
                                                return new B();
                                            }
                                        });
                                    } catch (DefinitionOverrideException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        }
                    }));
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        Simple.ComponentA a = new Simple.ComponentA();
        Scope session1 = koin.createScope("session1", Qualifier.named("Session"), null);
        Scope session2 = koin.createScope("session2", Qualifier.named("Session"), null);

        session1.declare(Simple.ComponentA.class, a, null, null, false);
        try {
            session2.get(Simple.ComponentA.class);
            fail();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
