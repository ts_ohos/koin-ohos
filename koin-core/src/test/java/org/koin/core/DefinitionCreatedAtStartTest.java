package org.koin.core;

import org.junit.Assert;
import org.junit.Test;
import org.koin.Simple;
import org.koin.core.context.DefaultContextExt;
import org.koin.core.definition.BeanDefinition;
import org.koin.core.definition.Definition;
import org.koin.core.error.DefinitionOverrideException;
import org.koin.core.error.KoinAppAlreadyStartedException;
import org.koin.core.error.NoScopeDefFoundException;
import org.koin.core.error.ScopeAlreadyCreatedException;
import org.koin.core.instance.InstanceFactory;
import org.koin.core.logger.Level;
import org.koin.core.parameter.DefinitionParameters;
import org.koin.core.scope.Scope;
import org.koin.dsl.KoinAppDeclaration;
import org.koin.dsl.KoinApplication;
import org.koin.dsl.Module;
import org.koin.dsl.ModuleDeclaration;
import org.koin.test.KoinApplicationExt;

public class DefinitionCreatedAtStartTest {

    @Test
    public void isDeclaredAsCreatedAtStart() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        org.koin.core.KoinApplication app = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                try {
                    koinApplication.modules(Module.module(new ModuleDeclaration() {
                        @Override
                        public void invoke(org.koin.core.module.Module module) {
                            try {
                                module.single(Simple.ComponentA.class, null, true, false, new Definition<Simple.ComponentA>() {
                                    @Override
                                    public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                        return new Simple.ComponentA();
                                    }
                                });
                            } catch (DefinitionOverrideException e) {
                                e.printStackTrace();
                            }
                        }
                    }));
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        BeanDefinition defA = KoinApplicationExt.getBeanDefinition(app.getKoin(), Simple.ComponentA.class);
        if (defA == null) {
            throw new IllegalStateException("no definition found");
        }
        Assert.assertTrue(defA.getOptions().isCreatedAtStart());
        InstanceFactory instanceFactory = KoinApplicationExt.getInstanceFactory(app.getKoin(), Simple.ComponentA.class);
        Assert.assertFalse(instanceFactory.isCreated());
    }

    @Test
    public void isCreatedAtStart() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, KoinAppAlreadyStartedException {
        org.koin.core.KoinApplication app = DefaultContextExt.startKoin(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                try {
                    koinApplication.modules(Module.module(new ModuleDeclaration() {
                        @Override
                        public void invoke(org.koin.core.module.Module module) {
                            try {
                                module.single(Simple.ComponentA.class, null, true, false, new Definition<Simple.ComponentA>() {
                                    @Override
                                    public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                        return new Simple.ComponentA();
                                    }
                                });
                            } catch (DefinitionOverrideException e) {
                                e.printStackTrace();
                            }
                        }
                    }));
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });
        BeanDefinition defA = KoinApplicationExt.getBeanDefinition(app.getKoin(), Simple.ComponentA.class);
        if (null == defA) {
            throw new IllegalStateException("no definition found");
        }
        Assert.assertTrue(defA.getOptions().isCreatedAtStart());
        Assert.assertTrue(KoinApplicationExt.getInstanceFactory(app.getKoin(), Simple.ComponentA.class).isCreated());
    }

    @Test
    public void factoryIsNotCreatedAtStart() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        org.koin.core.KoinApplication app = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                try {
                    koinApplication.modules(Module.module(new ModuleDeclaration() {
                        @Override
                        public void invoke(org.koin.core.module.Module module) {
                            try {
                                module.factory(Simple.ComponentA.class, null, false, new Definition<Simple.ComponentA>() {
                                    @Override
                                    public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                        return new Simple.ComponentA();
                                    }
                                });
                            } catch (DefinitionOverrideException e) {
                                e.printStackTrace();
                            }
                        }
                    }));
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });
        BeanDefinition defA = KoinApplicationExt.getBeanDefinition(app.getKoin(), Simple.ComponentA.class);
        if (null == defA) {
            throw new IllegalStateException("no definition found");
        }

        Assert.assertFalse(defA.getOptions().isCreatedAtStart());
        Assert.assertFalse(KoinApplicationExt.getInstanceFactory(app.getKoin(), Simple.ComponentA.class).isCreated());
        app.close();
    }

}
