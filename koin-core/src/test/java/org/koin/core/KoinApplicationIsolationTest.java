package org.koin.core;

import org.junit.Assert;
import org.junit.Test;
import org.koin.Simple;
import org.koin.core.definition.Definition;
import org.koin.core.error.*;
import org.koin.core.instance.InstanceFactory;
import org.koin.core.logger.Level;
import org.koin.core.parameter.DefinitionParameters;
import org.koin.core.qualifier.Qualifier;
import org.koin.core.scope.Scope;
import org.koin.dsl.KoinAppDeclaration;
import org.koin.dsl.Module;
import org.koin.dsl.ModuleDeclaration;
import org.koin.dsl.ScopeDSL;
import org.koin.mp.KoinPlatformTools;
import org.koin.test.KoinApplicationExt;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.fail;
import static org.koin.core.context.DefaultContextExt.startKoin;
import static org.koin.core.context.DefaultContextExt.stopKoin;
import static org.koin.dsl.KoinApplication.koinApplication;

public class KoinApplicationIsolationTest {

    @Test
    public void can_isolate_several_koin_apps() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        org.koin.core.KoinApplication app1 = koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {

                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.single(Simple.ComponentA.class, new Definition<Simple.ComponentA>() {
                                @Override
                                public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Simple.ComponentA();
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });

                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        org.koin.core.KoinApplication app2 = koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {

                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.single(Simple.ComponentA.class, new Definition<Simple.ComponentA>() {
                                @Override
                                public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Simple.ComponentA();
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });

                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        Simple.ComponentA a1 = app1.getKoin().<Simple.ComponentA>get(Simple.ComponentA.class);
        Simple.ComponentA a2 = app2.getKoin().<Simple.ComponentA>get(Simple.ComponentA.class);

        Assert.assertNotEquals(a1, a2);
    }

    @Test
    public void koin_app_instance_run_instance() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        org.koin.core.KoinApplication app = koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {

                koinApplication.printLogger(Level.DEBUG);

                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.single(Simple.ComponentA.class, null, true, false, new Definition<Simple.ComponentA>() {
                                @Override
                                public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Simple.ComponentA();
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });

                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        app.createEagerInstances();

        KoinApplicationExt.getBeanDefinition(app.getKoin(), Simple.ComponentA.class);

        Collection<InstanceFactory> values = app.getKoin()
                .getScopeRegistry()
                .getRootScope()
                .getInstanceRegistry()
                .getInstances()
                .values();

        InstanceFactory instanceFactory = null;
        for (InstanceFactory value : values) {
            if (value.getBeanDefinition().getPrimaryType().equals(Simple.ComponentA.class)) {
                instanceFactory = value;
                break;
            }
        }

        Assert.assertTrue(instanceFactory != null && instanceFactory.isCreated());
    }

    @Test
    public void can_isolate_koin_apps_standalone() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, KoinAppAlreadyStartedException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        startKoin(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {

                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.single(Simple.ComponentA.class, new Definition<Simple.ComponentA>() {
                                @Override
                                public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Simple.ComponentA();
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });

                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        org.koin.core.KoinApplication app2 = koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.single(Simple.ComponentA.class, new Definition<Simple.ComponentA>() {
                                @Override
                                public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Simple.ComponentA();
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });

                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        Simple.ComponentA a1 = KoinPlatformTools.INSTANCE().defaultContext().get().<Simple.ComponentA>get(Simple.ComponentA.class);
        Simple.ComponentA a2 = app2.getKoin().<Simple.ComponentA>get(Simple.ComponentA.class);

        Assert.assertNotEquals(a1, a2);
        stopKoin();
    }

    @Test
    public void stopping_koin_releases_resources() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, KoinAppAlreadyStartedException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
            @Override
            public void invoke(org.koin.core.module.Module module) {
                try {
                    module.single(Simple.ComponentA.class, new Definition<Simple.ComponentA>() {
                        @Override
                        public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                            return new Simple.ComponentA();
                        }
                    });

                    module.scope(Qualifier.named(Simple.class), new org.koin.core.module.Module.ScopeSet() {
                        @Override
                        public void invoke(ScopeDSL scopeDSL) {
                            try {
                                scopeDSL.scoped(Simple.ComponentB.class, new Definition<Simple.ComponentB>() {
                                    @Override
                                    public Simple.ComponentB invoke(Scope scope, DefinitionParameters parameters) {
                                        try {
                                            return new Simple.ComponentB(scope.get(Simple.ComponentA.class));
                                        } catch (ClosedScopeException e) {
                                            e.printStackTrace();
                                        } catch (DefinitionParameterException e) {
                                            e.printStackTrace();
                                        } catch (NoBeanDefFoundException e) {
                                            e.printStackTrace();
                                        }
                                        return null;
                                    }
                                });
                            } catch (DefinitionOverrideException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        startKoin(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        Simple.ComponentA a1 = KoinPlatformTools.INSTANCE().defaultContext().get().<Simple.ComponentA>get(Simple.ComponentA.class);
        Scope scope1 = KoinPlatformTools
                .INSTANCE()
                .defaultContext()
                .get()
                .createScope("simple", Qualifier.named(Simple.class));
        Simple.ComponentB b1 = scope1.<Simple.ComponentB>get(Simple.ComponentB.class);

        stopKoin();

        startKoin(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        Simple.ComponentA a2 = KoinPlatformTools.INSTANCE().defaultContext().get().<Simple.ComponentA>get(Simple.ComponentA.class);
        Scope scope2 = KoinPlatformTools
                .INSTANCE()
                .defaultContext()
                .get()
                .createScope("simple", Qualifier.named(Simple.class));
        Simple.ComponentB b2 = scope2.<Simple.ComponentB>get(Simple.ComponentB.class);

        Assert.assertNotEquals(a1, a2);
        Assert.assertNotEquals(b1, b2);

        stopKoin();
    }

    @Test
    public void create_multiple_context_without_named_qualifier() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        org.koin.core.KoinApplication koinA = koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                org.koin.core.module.Module module1 = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.single(ModelA.class, new Definition<ModelA>() {
                                @Override
                                public ModelA invoke(Scope scope, DefinitionParameters parameters) {
                                    return new ModelA();
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });

                org.koin.core.module.Module module2 = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.single(ModelB.class, new Definition<ModelB>() {
                                @Override
                                public ModelB invoke(Scope scope, DefinitionParameters parameters) {
                                    try {
                                        return new ModelB(scope.get(ModelA.class));
                                    } catch (ClosedScopeException e) {
                                        e.printStackTrace();
                                    } catch (DefinitionParameterException e) {
                                        e.printStackTrace();
                                    } catch (NoBeanDefFoundException e) {
                                        e.printStackTrace();
                                    }
                                    return null;
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });

                try {
                    koinApplication.modules(Arrays.asList(module1, module2));
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        org.koin.core.KoinApplication koinB = koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {

                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.single(ModelC.class, new Definition<ModelC>() {
                                @Override
                                public ModelC invoke(Scope scope, DefinitionParameters parameters) {
                                    return new ModelC();
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });

                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        koinA.getKoin().get(ModelA.class);
        koinA.getKoin().get(ModelB.class);
        koinB.getKoin().get(ModelC.class);

        try {
            koinB.getKoin().get(ModelA.class);
            fail();
        } catch (Exception e) {

        }

        try {
            koinB.getKoin().get(ModelB.class);
            fail();
        } catch (Exception e) {

        }

        try {
            koinA.getKoin().get(ModelC.class);
            fail();
        } catch (Exception e) {

        }
    }

    public static class ModelA {
    }

    public static class ModelB {
        public ModelA a;

        public ModelB(ModelA a) {
            this.a = a;
        }
    }

    public static class ModelC {
    }
}
