package org.koin.core;

import org.junit.Assert;
import org.junit.Test;
import org.koin.core.definition.Definition;
import org.koin.core.error.*;
import org.koin.core.logger.Level;
import org.koin.core.module.Module;
import org.koin.core.parameter.DefinitionParameters;
import org.koin.core.qualifier.Qualifier;
import org.koin.core.scope.Scope;
import org.koin.dsl.KoinAppDeclaration;
import org.koin.dsl.KoinApplication;
import org.koin.dsl.ModuleDeclaration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class GenericDeclarationTest {

    org.koin.core.module.Module modules = org.koin.dsl.Module.module(new ModuleDeclaration() {
        @Override
        public void invoke(Module module) {
            try {
                module.single(List.class, Qualifier.named("strings"), true, true, new Definition<List>() {
                    @Override
                    public List invoke(Scope scope, DefinitionParameters parameters) {
                        return new ArrayList(Collections.singleton("a string"));
                    }
                });

                module.single(List.class, Qualifier.named("ints"), false, false, new Definition<List>() {
                    @Override
                    public List invoke(Scope scope, DefinitionParameters parameters) {
                        return new ArrayList(Collections.singleton(42));
                    }
                });
            } catch (DefinitionOverrideException e) {
                e.printStackTrace();
            }
        }
    });

    private Koin createKoin() {
        KoinAppDeclaration koinAppDeclaration = new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                try {
                    koinApplication.modules(modules);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        };
        org.koin.core.KoinApplication app = null;
        try {
            app = KoinApplication.koinApplication(koinAppDeclaration);
        } catch (ScopeAlreadyCreatedException e) {
            e.printStackTrace();
        } catch (NoScopeDefFoundException e) {
            e.printStackTrace();
        }
        return app.getKoin();
    }

    @Test
    public void declare_and_retrieve_generic_definitions() {
        org.koin.core.Koin koin = createKoin();
        try {
            List aString = koin.get(List.class, Qualifier.named("strings"), null);
            Assert.assertEquals("a string", aString.get(0));
            List anInt = koin.get(List.class, Qualifier.named("ints"), null);
            Assert.assertEquals(42, anInt.get(0));
        } catch (DefinitionParameterException e) {
            e.printStackTrace();
        } catch (NoBeanDefFoundException e) {
            e.printStackTrace();
        } catch (ClosedScopeException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void declare_and_not_retrieve_generic_definitions() {
        org.koin.core.Koin koin = createKoin();
        try {
            koin.get(List.class);
            Assert.fail();
        } catch (Exception e) {
            Assert.assertNotNull(e);
        }
    }


}
