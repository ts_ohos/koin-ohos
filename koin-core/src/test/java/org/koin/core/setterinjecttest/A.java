package org.koin.core.setterinjecttest;

import org.koin.core.component.KoinScopeComponent;
import org.koin.core.error.NoScopeDefFoundException;
import org.koin.core.error.ScopeAlreadyCreatedException;
import org.koin.core.scope.Scope;

public class A extends KoinScopeComponent {

    public B b;
    public C c;

    public A() {

    }

    @Override
    protected Scope initScope() {
        if (scope == null) {
            try {
                scope = crateScope(this, this);
                return scope;
            } catch (ScopeAlreadyCreatedException e) {
                e.printStackTrace();
            } catch (NoScopeDefFoundException e) {
                e.printStackTrace();
            }
        }

        return scope;
    }
}
