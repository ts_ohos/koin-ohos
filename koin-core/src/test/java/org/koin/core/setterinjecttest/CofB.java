package org.koin.core.setterinjecttest;

public class CofB {

    private BofA b;

    public CofB(BofA b) {
        this.b = b;
    }

    public BofA getB() {
        return b;
    }
}
