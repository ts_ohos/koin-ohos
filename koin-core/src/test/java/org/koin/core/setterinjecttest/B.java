package org.koin.core.setterinjecttest;

import org.koin.core.component.KoinScopeComponent;
import org.koin.core.error.NoScopeDefFoundException;
import org.koin.core.error.ScopeAlreadyCreatedException;
import org.koin.core.scope.Scope;

public class B extends KoinScopeComponent {

    @Override
    protected Scope initScope() {
        try {
            Scope scope = KoinScopeComponent.crateScope(this, this);
            return scope;
        } catch (ScopeAlreadyCreatedException e) {
            e.printStackTrace();
        } catch (NoScopeDefFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
