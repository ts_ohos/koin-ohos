package org.koin.core;

import org.junit.Assert;
import org.junit.Test;
import org.koin.KoinCoreTest;
import org.koin.Simple;
import org.koin.core.definition.BeanDefinition;
import org.koin.core.definition.Definition;
import org.koin.core.definition.Kind;
import org.koin.core.error.*;
import org.koin.core.logger.Level;
import org.koin.core.module.Module;
import org.koin.core.parameter.DefinitionParameters;
import org.koin.core.parameter.ParametersDefinition;
import org.koin.core.qualifier.Qualifier;
import org.koin.core.scope.Scope;
import org.koin.dsl.KoinAppDeclaration;
import org.koin.dsl.KoinApplication;
import org.koin.dsl.*;
import org.koin.mp.KoinPlatformTools;
import org.koin.test.KoinApplicationExt;

import java.util.Arrays;

import static org.junit.Assert.fail;
import static org.koin.core.context.DefaultContextExt.*;
import static org.koin.core.parameter.DefinitionParameters.parametersOf;
import static org.koin.dsl.Module.module;

public class DynamicModulesTest extends KoinCoreTest {

    @Test
    public void should_unload_single_definition() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        Module module = module(new ModuleDeclaration() {
            @Override
            public void invoke(Module module) {
                try {
                    module.single(Simple.ComponentA.class, new Definition<Simple.ComponentA>() {
                        @Override
                        public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                            return new Simple.ComponentA();
                        }
                    });
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });
        org.koin.core.KoinApplication app = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        BeanDefinition defA = KoinApplicationExt.getBeanDefinition(app.getKoin(), Simple.ComponentA.class);
        if (defA == null) {
            throw new IllegalStateException("no definition found");
        }
        Assert.assertEquals(Kind.Single, defA.getKind());
        try {
            Assert.assertNotNull(app.getKoin().get(Simple.ComponentA.class));
        } catch (Exception e) {
            e.printStackTrace();
        }

        app.unloadModules(module);
        Assert.assertNull(KoinApplicationExt.getBeanDefinition(app.getKoin(), Simple.ComponentA.class));

        try {
            app.getKoin().get(Simple.ComponentA.class);
            fail();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void should_unload_additional_bound_definition() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        Module module = module(new ModuleDeclaration() {
            @Override
            public void invoke(Module module) {
                try {
                    BeanDefinition<Simple.Component1> single = module.single(Simple.Component1.class, new Definition<Simple.Component1>() {
                        @Override
                        public Simple.Component1 invoke(Scope scope, DefinitionParameters parameters) {
                            return new Simple.Component1();
                        }
                    });
                    DefinitionBinding.bind(single, Simple.ComponentInterface1.class);

                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        org.koin.core.KoinApplication app = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        BeanDefinition defA = KoinApplicationExt.getBeanDefinition(app.getKoin(), Simple.Component1.class);
        if (defA == null) {
            throw new IllegalStateException("no definition found");
        }
        Assert.assertEquals(Kind.Single, defA.getKind());

        try {
            Assert.assertNotNull(app.getKoin().get(Simple.Component1.class));
            Assert.assertNotNull(app.getKoin().get(Simple.ComponentInterface1.class));
        } catch (Exception e) {
            e.printStackTrace();
        }

        app.unloadModules(module);

        Assert.assertNull(KoinApplicationExt.getBeanDefinition(app.getKoin(), Simple.ComponentA.class));

        try {
            app.getKoin().get(Simple.Component1.class);
            fail();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void should_unload_one_module_definition() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        Module module1 = module(new ModuleDeclaration() {
            @Override
            public void invoke(Module module) {
                try {
                    module.single(Simple.ComponentA.class, new Definition<Simple.ComponentA>() {
                        @Override
                        public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                            return new Simple.ComponentA();
                        }
                    });
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });
        Module module2 = module(new ModuleDeclaration() {
            @Override
            public void invoke(Module module) {
                try {
                    module.single(Simple.ComponentB.class, new Definition<Simple.ComponentB>() {
                        @Override
                        public Simple.ComponentB invoke(Scope scope, DefinitionParameters parameters) {
                            try {
                                return new Simple.ComponentB(scope.get(Simple.ComponentA.class));
                            } catch (ClosedScopeException e) {
                                e.printStackTrace();
                            } catch (DefinitionParameterException e) {
                                e.printStackTrace();
                            } catch (NoBeanDefFoundException e) {
                                e.printStackTrace();
                            }
                            return null;
                        }
                    });
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });
        org.koin.core.KoinApplication app = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                try {
                    koinApplication.modules(Arrays.asList(module1, module2));
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        if (KoinApplicationExt.getBeanDefinition(app.getKoin(), Simple.ComponentA.class) == null) {
            throw new IllegalStateException("no definition found");
        }
        if (KoinApplicationExt.getBeanDefinition(app.getKoin(), Simple.ComponentB.class) == null) {
            throw new IllegalStateException("no definition found");
        }

        try {
            Assert.assertNotNull(app.getKoin().get(Simple.ComponentA.class));
            Assert.assertNotNull(app.getKoin().get(Simple.ComponentB.class));
        } catch (Exception e) {
            e.printStackTrace();
        }

        app.unloadModules(module2);

        Assert.assertNull(KoinApplicationExt.getBeanDefinition(app.getKoin(), Simple.ComponentB.class));

        try {
            app.getKoin().get(Simple.ComponentB.class);
            fail();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void should_unload_one_module_definition_factory() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        Module module1 = module(new ModuleDeclaration() {
            @Override
            public void invoke(Module module) {
                try {
                    module.single(Simple.ComponentA.class, new Definition<Simple.ComponentA>() {
                        @Override
                        public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                            return new Simple.ComponentA();
                        }
                    });
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });
        Module module2 = module(new ModuleDeclaration() {
            @Override
            public void invoke(Module module) {
                try {
                    module.factory(Simple.ComponentB.class, null, false, new Definition<Simple.ComponentB>() {
                        @Override
                        public Simple.ComponentB invoke(Scope scope, DefinitionParameters parameters) {
                            try {
                                return new Simple.ComponentB(scope.get(Simple.ComponentA.class));
                            } catch (ClosedScopeException e) {
                                e.printStackTrace();
                            } catch (DefinitionParameterException e) {
                                e.printStackTrace();
                            } catch (NoBeanDefFoundException e) {
                                e.printStackTrace();
                            }
                            return null;
                        }
                    });
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });
        org.koin.core.KoinApplication app = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                try {
                    koinApplication.modules(Arrays.asList(module1, module2));
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        if (KoinApplicationExt.getBeanDefinition(app.getKoin(), Simple.ComponentA.class) == null) {
            throw new IllegalStateException("no definition found");
        }
        if (KoinApplicationExt.getBeanDefinition(app.getKoin(), Simple.ComponentB.class) == null) {
            throw new IllegalStateException("no definition found");
        }

        try {
            Assert.assertNotNull(app.getKoin().get(Simple.ComponentA.class));
            Assert.assertNotNull(app.getKoin().get(Simple.ComponentB.class));
        } catch (Exception e) {
            e.printStackTrace();
        }

        app.unloadModules(module2);

        Assert.assertNull(KoinApplicationExt.getBeanDefinition(app.getKoin(), Simple.ComponentB.class));

        try {
            app.getKoin().get(Simple.ComponentB.class);
            fail();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void should_unload_module_override_definition() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        Module module1 = module(new ModuleDeclaration() {
            @Override
            public void invoke(Module module) {
                try {
                    module.single(Simple.MySingle.class, new Definition<Simple.MySingle>() {
                        @Override
                        public Simple.MySingle invoke(Scope scope, DefinitionParameters parameters) {
                            return new Simple.MySingle(42);
                        }
                    });
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });
        Module module2 = module(new ModuleDeclaration() {
            @Override
            public void invoke(Module module) {
                try {
                    module.single(Simple.MySingle.class, null, false, true, new Definition<Simple.MySingle>() {
                        @Override
                        public Simple.MySingle invoke(Scope scope, DefinitionParameters parameters) {
                            return new Simple.MySingle(24);
                        }
                    });
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });
        org.koin.core.KoinApplication app = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                try {
                    koinApplication.modules(Arrays.asList(module1, module2));
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }

        });

        if (KoinApplicationExt.getBeanDefinition(app.getKoin(), Simple.MySingle.class) == null) {
            throw new IllegalStateException("no definition found");
        }
        try {
            Assert.assertEquals(24, app.getKoin().get(Simple.MySingle.class).getId());
        } catch (Exception e) {
            e.printStackTrace();
        }

        app.unloadModules(module2);

        Assert.assertNull(KoinApplicationExt.getBeanDefinition(app.getKoin(), Simple.MySingle.class));

        try {
            app.getKoin().get(Simple.MySingle.class);
            fail();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void should_reload_module_definition() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException, DefinitionOverrideException {
        Module module = module(new ModuleDeclaration() {
            @Override
            public void invoke(Module module) {
                try {
                    module.single(Simple.MySingle.class, new Definition<Simple.MySingle>() {
                        @Override
                        public Simple.MySingle invoke(Scope scope, DefinitionParameters parameters) {

                            if (parameters.size() > 0) {
                                int i = parameters.get(0);
                                return new Simple.MySingle(i);
                            }

                            return null;
                        }
                    });
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });
        org.koin.core.KoinApplication app = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        Koin koin = app.getKoin();

        if (KoinApplicationExt.getBeanDefinition(app.getKoin(), Simple.MySingle.class) == null) {
            throw new IllegalStateException("no definition found");
        }
        Assert.assertEquals(42, app.getKoin().get(Simple.MySingle.class, null, new ParametersDefinition() {
            @Override
            public DefinitionParameters invoke() {
                try {
                    return DefinitionParameters.parametersOf(42);
                } catch (DefinitionParameterException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }).getId());

        koin.unloadModules(Arrays.asList(module));
        koin.loadModules(Arrays.asList(module));

        Assert.assertNotNull(KoinApplicationExt.getBeanDefinition(app.getKoin(), Simple.MySingle.class));
        Assert.assertEquals(24, app.getKoin().get(Simple.MySingle.class, null, new ParametersDefinition() {
            @Override
            public DefinitionParameters invoke() {
                try {
                    return DefinitionParameters.parametersOf(24);
                } catch (DefinitionParameterException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }).getId());
    }

    @Test
    public void create_at_start_for_external_modules() throws DefinitionOverrideException, ScopeAlreadyCreatedException, NoScopeDefFoundException {
        final boolean[] created = {false};

        Module module = org.koin.dsl.Module.module(true, false, new ModuleDeclaration() {
            @Override
            public void invoke(Module module) {
                try {
                    module.single(Simple.MySingle.class, new Definition<Simple.MySingle>() {
                                @Override
                                public Simple.MySingle invoke(Scope scope, DefinitionParameters parameters) {

                                    created[0] = true;

                                    return new Simple.MySingle(42);
                                }
                            }
                    );
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });
        org.koin.core.KoinApplication app = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
            }
        });

        Koin koin = app.getKoin();

        koin.loadModules(Arrays.asList(module), true);
        Assert.assertTrue(created[0]);
    }

    @Test
    public void should_reload_module_definition_global_context() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, KoinAppAlreadyStartedException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        Module module = org.koin.dsl.Module.module(new ModuleDeclaration() {
            @Override
            public void invoke(Module module) {
                try {
                    module.single(Simple.MySingle.class, new Definition<Simple.MySingle>() {
                        @Override
                        public Simple.MySingle invoke(Scope scope, DefinitionParameters parameters) {

                            int i = parameters.get(0);
                            return new Simple.MySingle(i);
                        }
                    });
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });
        startKoin(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        Koin koin = KoinPlatformTools.INSTANCE().defaultContext().get();
        Simple.MySingle a1 = koin.get(Simple.MySingle.class, null, new ParametersDefinition() {
            @Override
            public DefinitionParameters invoke() {
                try {
                    return parametersOf(42);
                } catch (DefinitionParameterException e) {
                    e.printStackTrace();
                }
                return null;
            }
        });
        Assert.assertEquals(42, a1.getId());

        unloadKoinModules(module);
        loadKoinModules(module);

        Koin koin1 = KoinPlatformTools.INSTANCE().defaultContext().get();
        Simple.MySingle a3 = koin1.get(Simple.MySingle.class, null, new ParametersDefinition() {
            @Override
            public DefinitionParameters invoke() {
                try {
                    return DefinitionParameters.parametersOf(24);
                } catch (DefinitionParameterException e) {
                    e.printStackTrace();
                }
                return null;
            }
        });
        Assert.assertEquals(24, a3.getId());
        stopKoin();
    }

    @Test
    public void should_unload_scoped_definition() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        Qualifier scopeKey = Qualifier.named("-SCOPE-");
        Module module = org.koin.dsl.Module.module(new ModuleDeclaration() {
            @Override
            public void invoke(Module module) {
                module.scope(scopeKey, new Module.ScopeSet() {
                    @Override
                    public void invoke(ScopeDSL scopeDSL) {
                        try {
                            scopeDSL.scoped(Simple.ComponentA.class, new Definition<Simple.ComponentA>() {
                                @Override
                                public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Simple.ComponentA();
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
        org.koin.core.KoinApplication app = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        Scope scope = app.getKoin().createScope("id", scopeKey);
        BeanDefinition defA = scope.getBeanDefinition(Simple.ComponentA.class);
        if (defA == null) {
            throw new IllegalStateException("no definition found");
        }
        Assert.assertEquals(Kind.Single, defA.getKind());
        Assert.assertEquals(scopeKey, defA.getScopeQualifier());
        try {
            Assert.assertNotNull(scope.get(Simple.ComponentA.class));
        } catch (Exception e) {
            e.printStackTrace();
        }

        app.unloadModules(module);

        Assert.assertNull(scope.getBeanDefinition(Simple.ComponentA.class));

        try {
            scope.get(Simple.ComponentA.class);
            fail();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void should_reload_scoped_definition() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException, DefinitionOverrideException {
        Qualifier scopeKey = Qualifier.named("-SCOPE-");
        Module module = org.koin.dsl.Module.module(new ModuleDeclaration() {
            @Override
            public void invoke(Module module) {
                module.scope(scopeKey, new Module.ScopeSet() {
                    @Override
                    public void invoke(ScopeDSL scopeDSL) {
                        try {
                            scopeDSL.scoped(Simple.ComponentA.class, new Definition<Simple.ComponentA>() {
                                @Override
                                public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Simple.ComponentA();
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
        org.koin.core.KoinApplication app = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });
        Koin koin = app.getKoin();

        Scope scope = app.getKoin().createScope("id", scopeKey);
        BeanDefinition defA = scope.getBeanDefinition(Simple.ComponentA.class);
        if (defA == null) {
            throw new IllegalStateException("no definition found");
        }

        Assert.assertEquals(Kind.Single, defA.getKind());
        Assert.assertEquals(scopeKey, defA.getScopeQualifier());
        Assert.assertNotNull(scope.get(Simple.ComponentA.class));

        koin.unloadModules(Arrays.asList(module));
        koin.loadModules(Arrays.asList(module));

        scope.get(Simple.ComponentA.class);
        Assert.assertNotNull(scope.getBeanDefinition(Simple.ComponentA.class));
    }

    @Test
    public void should_reload_scoped_definition_global() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, KoinAppAlreadyStartedException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        Qualifier scopeKey = Qualifier.named("-SCOPE-");
        Module module = org.koin.dsl.Module.module(new ModuleDeclaration() {
            @Override
            public void invoke(Module module) {
                module.scope(scopeKey, new Module.ScopeSet() {
                    @Override
                    public void invoke(ScopeDSL scopeDSL) {
                        try {
                            scopeDSL.scoped(Simple.ComponentA.class, new Definition<Simple.ComponentA>() {
                                @Override
                                public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Simple.ComponentA();
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
        startKoin(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        Scope scope = KoinPlatformTools.INSTANCE().defaultContext().get().createScope("id", scopeKey);
        Assert.assertNotNull(scope.get(Simple.ComponentA.class));

        unloadKoinModules(module);
        loadKoinModules(module);

        scope.get(Simple.ComponentA.class);

        stopKoin();
    }

}
