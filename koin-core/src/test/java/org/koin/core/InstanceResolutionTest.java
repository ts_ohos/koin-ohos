package org.koin.core;

import org.junit.Assert;
import org.junit.Test;
import org.koin.Simple;
import org.koin.core.definition.BeanDefinition;
import org.koin.core.definition.Definition;
import org.koin.core.error.*;
import org.koin.core.logger.Level;
import org.koin.core.parameter.DefinitionParameters;
import org.koin.core.qualifier.Qualifier;
import org.koin.core.scope.Scope;
import org.koin.dsl.KoinAppDeclaration;
import org.koin.dsl.KoinApplication;
import org.koin.dsl.*;

import java.util.List;

public class InstanceResolutionTest {

    @Test
    public void can_resolve_a_single() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        org.koin.core.KoinApplication app = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {

                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.single(Simple.ComponentA.class, new Definition<Simple.ComponentA>() {
                                @Override
                                public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Simple.ComponentA();
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });

                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });
        Koin koin = app.getKoin();
        Simple.ComponentA a = koin.<Simple.ComponentA>get(Simple.ComponentA.class);
        Simple.ComponentA a2 = koin.<Simple.ComponentA>get(Simple.ComponentA.class);

        Assert.assertEquals(a, a2);
    }

    @Test
    public void can_resolve_all_ComponentInterface1() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {

        Koin koin = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {

                koinApplication.printLogger(Level.DEBUG);

                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            BeanDefinition<Simple.Component1> single1 = module.single(Simple.Component1.class, new Definition<Simple.Component1>() {
                                @Override
                                public Simple.Component1 invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Simple.Component1();
                                }
                            });

                            DefinitionBinding.bind(single1, Simple.ComponentInterface1.class);

                            BeanDefinition<Simple.Component2> single2 = module.single(Simple.Component2.class, new Definition<Simple.Component2>() {
                                @Override
                                public Simple.Component2 invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Simple.Component2();
                                }
                            });

                            DefinitionBinding.bind(single2, Simple.ComponentInterface1.class);

                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });

                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        Simple.Component1 a1 = koin.<Simple.Component1>get(Simple.Component1.class);
        Simple.Component2 a2 = koin.<Simple.Component2>get(Simple.Component2.class);

        List<Simple.ComponentInterface1> interfaces = koin.<Simple.ComponentInterface1>getAll(Simple.ComponentInterface1.class);

        Assert.assertTrue(interfaces.size() == 2 && interfaces.contains(a1) && interfaces.contains(a2));
    }

    @Test
    public void cannot_resolve_a_single() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        Koin koin = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {

                koinApplication.printLogger(Level.DEBUG);

                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {

                    }
                });

                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        Simple.ComponentA a = koin.<Simple.ComponentA>getOrNull(Simple.ComponentA.class);
        Assert.assertTrue(a == null);
    }

    @Test
    public void can_resolve_a_singles_by_name() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        Koin koin = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {

                koinApplication.printLogger(Level.DEBUG);

                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        Simple.ComponentA componentA = new Simple.ComponentA();

                        try {
                            module.single(Simple.ComponentA.class, Qualifier.named("A"), false, false, new Definition<Simple.ComponentA>() {
                                @Override
                                public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                    return componentA;
                                }
                            });

                            module.single(Simple.ComponentA.class, Qualifier.named("B"), false, false, new Definition<Simple.ComponentA>() {
                                @Override
                                public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                    return componentA;
                                }
                            });

                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }

                    }
                });

                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        Simple.ComponentA a = koin.<Simple.ComponentA>get(Simple.ComponentA.class, Qualifier.named("A"), null);
        Simple.ComponentA b = koin.<Simple.ComponentA>get(Simple.ComponentA.class, Qualifier.named("B"), null);

        Assert.assertEquals(a, b);
    }

    @Test
    public void can_resolve_a_factory_by_name() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        Koin koin = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {

                koinApplication.printLogger(Level.DEBUG);

                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {

                        Simple.ComponentA componentA = new Simple.ComponentA();

                        try {
                            module.factory(Simple.ComponentA.class, Qualifier.named("A"), false, new Definition<Simple.ComponentA>() {
                                @Override
                                public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                    return componentA;
                                }
                            });
                            module.factory(Simple.ComponentA.class, Qualifier.named("B"), false, new Definition<Simple.ComponentA>() {
                                @Override
                                public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                    return componentA;
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });

                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        Simple.ComponentA a = koin.<Simple.ComponentA>get(Simple.ComponentA.class, Qualifier.named("A"), null);
        Simple.ComponentA b = koin.<Simple.ComponentA>get(Simple.ComponentA.class, Qualifier.named("B"), null);

        Assert.assertEquals(a, b);
    }

    @Test
    public void can_resolve_a_factory() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        Koin koin = KoinApplication.koinApplication(new KoinAppDeclaration() {

            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {

                koinApplication.printLogger(Level.DEBUG);

                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.factory(Simple.ComponentA.class, null, false, new Definition<Simple.ComponentA>() {
                                @Override
                                public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Simple.ComponentA();
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });

                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        Simple.ComponentA a = koin.<Simple.ComponentA>get(Simple.ComponentA.class);
        Simple.ComponentA a2 = koin.<Simple.ComponentA>get(Simple.ComponentA.class);

        Assert.assertNotEquals(a, a2);
    }

    @Test
    public void should_resolve_default() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        Koin koin = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {

                koinApplication.printLogger(Level.DEBUG);

                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.single(Simple.ComponentInterface1.class, Qualifier.named("2"), new Definition<Simple.ComponentInterface1>() {
                                @Override
                                public Simple.ComponentInterface1 invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Simple.Component2();
                                }
                            });

                            module.single(Simple.ComponentInterface1.class, new Definition<Simple.ComponentInterface1>() {
                                @Override
                                public Simple.ComponentInterface1 invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Simple.Component1();
                                }
                            });

                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });

                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        Simple.ComponentInterface1 component = koin.<Simple.ComponentInterface1>get(Simple.ComponentInterface1.class);
        Assert.assertTrue(component instanceof Simple.Component1);
        Assert.assertTrue(koin.<Simple.ComponentInterface1>get(Simple.ComponentInterface1.class, Qualifier.named("2")) instanceof Simple.Component2);
    }
}
