package org.koin.core;

import org.junit.Assert;
import org.junit.Test;
import org.koin.Simple;
import org.koin.core.definition.Definition;
import org.koin.core.error.*;
import org.koin.core.parameter.DefinitionParameters;
import org.koin.core.parameter.ParametersDefinition;
import org.koin.core.scope.Scope;
import org.koin.dsl.Module;
import org.koin.dsl.ModuleDeclaration;
import org.koin.mp.KoinPlatformTools;

import static org.koin.core.context.DefaultContextExt.startKoin;
import static org.koin.core.context.DefaultContextExt.stopKoin;

public class InstanceReleaseTest {

    @Test
    public void can_resolve_a_single() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, KoinAppAlreadyStartedException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {

        org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
            @Override
            public void invoke(org.koin.core.module.Module module) {
                try {
                    module.single(Simple.MySingle.class, new Definition<Simple.MySingle>() {
                        @Override
                        public Simple.MySingle invoke(Scope scope, DefinitionParameters parameters) {

                            if (parameters.size() > 0) {
                                int i = parameters.get(0);
                                return new Simple.MySingle(i);
                            }

                            return null;
                        }
                    });
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        startKoin(new org.koin.dsl.KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        Koin koin = KoinPlatformTools.INSTANCE().defaultContext().get();
        Simple.MySingle a1 = koin.<Simple.MySingle>get(Simple.MySingle.class, null, new ParametersDefinition() {
            @Override
            public DefinitionParameters invoke() {
                try {
                    return DefinitionParameters.parametersOf(42);
                } catch (DefinitionParameterException e) {
                    e.printStackTrace();
                }
                return null;
            }
        });

        Assert.assertEquals(42, a1.getId());
        stopKoin();

        startKoin(new org.koin.dsl.KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        Koin koin1 = KoinPlatformTools.INSTANCE().defaultContext().get();
        Simple.MySingle a3 = koin1.<Simple.MySingle>get(Simple.MySingle.class, null, new ParametersDefinition() {
            @Override
            public DefinitionParameters invoke() {
                try {
                    return DefinitionParameters.parametersOf(24);
                } catch (DefinitionParameterException e) {
                    e.printStackTrace();
                }
                return null;
            }
        });

        Assert.assertEquals(24, a3.getId());
        stopKoin();

    }

}
