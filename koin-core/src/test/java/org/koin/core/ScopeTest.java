package org.koin.core;

import org.junit.Test;
import org.koin.Simple;
import org.koin.core.definition.Definition;
import org.koin.core.error.*;
import org.koin.core.logger.Level;
import org.koin.core.parameter.DefinitionParameters;
import org.koin.core.qualifier.StringQualifier;
import org.koin.core.scope.Scope;
import org.koin.core.setterinjecttest.B;
import org.koin.dsl.KoinAppDeclaration;
import org.koin.dsl.Module;
import org.koin.dsl.ModuleDeclaration;
import org.koin.dsl.ScopeDSL;

import static org.junit.Assert.*;
import static org.koin.core.qualifier.Qualifier.named;
import static org.koin.dsl.KoinApplication.koinApplication;

public class ScopeTest {

    @Test
    public void get_definition_from_current_scopes_type() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        Koin koin = koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);

                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        module.scope(named(ClosedScopeAPI.ScopeType.class), new org.koin.core.module.Module.ScopeSet() {
                            @Override
                            public void invoke(ScopeDSL scopeDSL) {
                                try {
                                    scopeDSL.scoped(Simple.ComponentA.class, new Definition<Simple.ComponentA>() {
                                        @Override
                                        public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                            return new Simple.ComponentA();
                                        }
                                    });
                                } catch (DefinitionOverrideException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                });

                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        Scope scope = koin.createScope("scope1", named(ClosedScopeAPI.ScopeType.class));
        assertNotNull(scope.getOrNull(Simple.ComponentA.class));
        assertEquals(scope.<Simple.ComponentA>getOrNull(Simple.ComponentA.class), scope.<Simple.ComponentA>getOrNull(Simple.ComponentA.class));
        scope.close();
        assertTrue(scope.getClosed());
        assertNull(scope.getOrNull(Simple.ComponentA.class));
        try {
            scope.get(Simple.ComponentA.class);
            fail();
        } catch (ClosedScopeException e) {
            e.printStackTrace();
        } catch (DefinitionParameterException e) {
            e.printStackTrace();
        } catch (NoBeanDefFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void call_linked_scope_factory_definition_only_once_if_not_found_in_current_scope() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        final int[] factoryCallCounter = {0};
        Koin koin = koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {
                koinApplication.printLogger();

                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.factory(Simple.ComponentA.class, new Definition<Simple.ComponentA>() {
                                @Override
                                public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                    factoryCallCounter[0]++;
                                    return new Simple.ComponentA();
                                }
                            });

                            module.scope(named(ClosedScopeAPI.ScopeType.class), new org.koin.core.module.Module.ScopeSet() {
                                @Override
                                public void invoke(ScopeDSL scopeDSL) {
                                    try {
                                        scopeDSL.scoped(B.class, new Definition<B>() {
                                            @Override
                                            public B invoke(Scope scope, DefinitionParameters parameters) {
                                                return new B();
                                            }
                                        });
                                    } catch (DefinitionOverrideException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });

                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });

                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        Scope scope = koin.createScope("scope1", named(ClosedScopeAPI.ScopeType.class));
        scope.get(Simple.ComponentA.class);
        assertEquals(factoryCallCounter[0], 1);
    }

    @Test
    public void recreate_a_scope() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionOverrideException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        String baseUrl = "base_url";
        String baseUrl2 = "base_url";
        StringQualifier baseUrlKey = named("BASE_URL_KEY");

        String scopeId = "user_scope";
        StringQualifier scopeKey = named("KEY");

        Koin koin = koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {

                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        module.scope(scopeKey, new org.koin.core.module.Module.ScopeSet() {
                            @Override
                            public void invoke(ScopeDSL scopeDSL) {
                                try {
                                    scopeDSL.scoped(Simple.ComponentA.class, new Definition<Simple.ComponentA>() {
                                        @Override
                                        public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                            return new Simple.ComponentA();
                                        }
                                    });
                                } catch (DefinitionOverrideException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                });

                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        Scope scope = koin.createScope(scopeId, scopeKey);
        scope.declare(String.class, baseUrl, baseUrlKey);
        assertEquals(baseUrl, scope.get(String.class, baseUrlKey));

        scope.close();

        Scope scope1 = koin.createScope(scopeId, scopeKey);
        scope1.declare(String.class, baseUrl2, baseUrlKey);

        assertEquals(baseUrl2, scope1.get(String.class, baseUrlKey));
    }

    @Test
    public void can_create_empty_scope() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionOverrideException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        String baseUrl = "base_url";
        StringQualifier baseUrlKey = named("BASE_URL_KEY");
        String scopeId = "user_scope";
        StringQualifier scopeKey = named("KEY");

        Koin koin = koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {
                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        module.scope(scopeKey, new org.koin.core.module.Module.ScopeSet() {
                            @Override
                            public void invoke(ScopeDSL scopeDSL) {

                            }
                        });
                    }
                });

                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        Scope scope = koin.createScope(scopeId, scopeKey);
        scope.declare(String.class, baseUrl, baseUrlKey);
        assertEquals(baseUrl, scope.get(String.class, baseUrlKey));
        scope.close();
    }

    @Test
    public void redeclare_scope() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        String scopeId = "user_scope";
        StringQualifier scopeKey = named("KEY");

        Koin koin = koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {
                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        module.scope(scopeKey, new org.koin.core.module.Module.ScopeSet() {
                            @Override
                            public void invoke(ScopeDSL scopeDSL) {
                                try {
                                    scopeDSL.scoped(Simple.ComponentA.class, new Definition<Simple.ComponentA>() {
                                        @Override
                                        public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                            return new Simple.ComponentA();
                                        }
                                    });
                                } catch (DefinitionOverrideException e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                        module.scope(scopeKey, new org.koin.core.module.Module.ScopeSet() {
                            @Override
                            public void invoke(ScopeDSL scopeDSL) {
                                try {
                                    scopeDSL.scoped(Simple.ComponentB.class, new Definition<Simple.ComponentB>() {
                                        @Override
                                        public Simple.ComponentB invoke(Scope scope, DefinitionParameters parameters) {
                                            try {
                                                return new Simple.ComponentB(scope.get(Simple.ComponentA.class));
                                            } catch (ClosedScopeException e) {
                                                e.printStackTrace();
                                            } catch (DefinitionParameterException e) {
                                                e.printStackTrace();
                                            } catch (NoBeanDefFoundException e) {
                                                e.printStackTrace();
                                            }
                                            return null;
                                        }
                                    });
                                } catch (DefinitionOverrideException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                });

                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        Scope scope = koin.createScope(scopeId, scopeKey);
        scope.get(Simple.ComponentB.class);
        scope.close();
    }
}
