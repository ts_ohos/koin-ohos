package org.koin.core;

import org.junit.Before;
import org.junit.Test;
import org.koin.Simple;
import org.koin.core.definition.Definition;
import org.koin.core.error.*;
import org.koin.core.parameter.DefinitionParameters;
import org.koin.core.qualifier.StringQualifier;
import org.koin.core.scope.Scope;
import org.koin.core.setterinjecttest.A;
import org.koin.dsl.KoinAppDeclaration;
import org.koin.dsl.Module;
import org.koin.dsl.ModuleDeclaration;
import org.koin.dsl.ScopeDSL;

import static org.junit.Assert.*;
import static org.koin.core.qualifier.Qualifier.named;
import static org.koin.dsl.KoinApplication.koinApplication;

public class ScopeAPITest {

    StringQualifier scopeKey = named("KEY");

    Koin koin;

    @Before
    public void init() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        koin = koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {
                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        module.scope(scopeKey, new org.koin.core.module.Module.ScopeSet() {
                            @Override
                            public void invoke(ScopeDSL scopeDSL) {
                                try {
                                    scopeDSL.scoped(A.class, new Definition<A>() {
                                        @Override
                                        public A invoke(Scope scope, DefinitionParameters parameters) {
                                            return new A();
                                        }
                                    });
                                } catch (DefinitionOverrideException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                });

                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();
    }

    @Test
    public void create_a_scope_instance() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, ScopeNotCreatedException {
        String scopeId = "myScope";
        Scope scope1 = koin.createScope(scopeId, scopeKey);
        Scope scope2 = koin.getScope(scopeId);

        assertEquals(scope1, scope2);
    }

    @Test
    public void cannot_find_a_non_created_scope_instance() {
        String scopeId = "myScope";
        try {
            koin.getScope(scopeId);
            fail();
        } catch (ScopeNotCreatedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void create_different_scopes() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        Scope scope1 = koin.createScope("myScope1", scopeKey);
        Scope scope2 = koin.createScope("myScope2", scopeKey);

        assertNotEquals(scope1, scope2);
    }

    @Test
    public void cannot_create_scope_instance_with_unknown_scope_def() {
        try {
            koin.createScope("myScope", named("a_scope"));
            fail();
        } catch (ScopeAlreadyCreatedException e) {
            e.printStackTrace();
        } catch (NoScopeDefFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void create_scope_instance_with_scope_def() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        assertNotNull(koin.createScope("myScope", scopeKey));
    }

    @Test
    public void cannot_create_a_new_scope_if_not_closed() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        koin.createScope("myScope1", scopeKey);
        try {
            koin.createScope("myScope1", scopeKey);
            fail();
        } catch (ScopeAlreadyCreatedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void cannot_get_a_closed_scope() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        Scope scope = koin.createScope("myScope1", scopeKey);
        scope.close();
        try {
            koin.getScope("myScope1");
            fail();
        } catch (ScopeNotCreatedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void reuse_a_closed_scope() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        Scope scope = koin.createScope("myScope1", scopeKey);
        scope.close();
        try {
            scope.get(Simple.ComponentA.class);
            fail();
        } catch (ClosedScopeException e) {
            e.printStackTrace();
        } catch (DefinitionParameterException e) {
            e.printStackTrace();
        } catch (NoBeanDefFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void find_a_scope_by_id() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, ScopeNotCreatedException {
        String scopeId = "myScope";
        Scope scope1 = koin.createScope(scopeId, scopeKey);
        assertEquals(scope1, koin.getScope(scope1.getId()));
    }
}
