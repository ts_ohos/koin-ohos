package org.koin.core;

import org.junit.Test;
import org.koin.core.error.DefinitionParameterException;
import org.koin.core.parameter.DefinitionParameters;

import static org.junit.Assert.*;
import static org.koin.core.parameter.DefinitionParameters.MAX_PARAMS;
import static org.koin.core.parameter.DefinitionParameters.parametersOf;

public class ParametersHolderTest {

    @Test
    public void create_a_parameters_holder_nullable_params() throws DefinitionParameterException {
        DefinitionParameters parameters = parametersOf(42, null);
        int i = parameters.get(0);
        String s = parameters.get(1);

        assertTrue(i == 42);
        assertTrue(s == null);
    }

    @Test
    public void create_a_parameters_holder_1_param() throws DefinitionParameterException {
        String myString = "empty";
        int myInt = 42;
        DefinitionParameters parameters = parametersOf(myString);
        DefinitionParameters newParams = parameters.insert(0, myInt);

        assertEquals(2, newParams.size());
        assertTrue(newParams.<Integer>get(0) == myInt);
        assertEquals(newParams.<String>get(1), myString);
    }

    @Test
    public void create_a_parameters_holder_2_param() throws DefinitionParameterException {
        String myString = "empty";
        int myInt = 42;
        DefinitionParameters parameters = parametersOf(myString, myInt);
        DefinitionParameters newParams = parameters.insert(0, myInt);

        assertEquals(3, newParams.size());
        assertTrue(newParams.<Integer>get(0) == myInt);
        assertEquals(newParams.<String>get(1), myString);
    }

    @Test
    public void create_a_parameters_holder() throws DefinitionParameterException {
        String myString = "empty";
        int myInt = 42;
        DefinitionParameters parameters = parametersOf(myString, myInt);

        assertEquals(2, parameters.size());
        assertTrue(parameters.isNotEmpty());
    }

    @Test
    public void create_an_empty_parameters_holder() throws DefinitionParameterException {
        DefinitionParameters parameters = parametersOf();

        assertEquals(0, parameters.size());
        assertTrue(parameters.isEmpty());
    }

    @Test
    public void get_parameters_from_a_parameter_holder() throws DefinitionParameterException {
        String myString = "empty";
        int myInt = 42;
        DefinitionParameters parameters = parametersOf(myString, myInt);

        assertEquals(myString, parameters.get(0));
        assertTrue(myInt == parameters.<Integer>get(1));
    }

    @Test
    public void cannot_create_parameters_more_than_max_params() {
        try {
            parametersOf(1, 2, 3, 4, 5, 6);
            fail("Can't build more than " + MAX_PARAMS);
        } catch (DefinitionParameterException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void can_insert_param() throws DefinitionParameterException {
        DefinitionParameters p = parametersOf(1, 2, 3, 4);
        assertTrue(p.insert(4, 5).<Integer>get(4) == 5);
        assertTrue(p.insert(0, 0).<Integer>get(0) == 0);
    }

    @Test
    public void can_add_param() throws DefinitionParameterException {
        DefinitionParameters p = parametersOf(1, 2, 3, 4);
        assertTrue(p.add(5).<Integer>get(4) == 5);
    }

    @Test
    public void can_add_into_empty_param() throws DefinitionParameterException {
        DefinitionParameters p = parametersOf();
        assertTrue(p.add(5).<Integer>get(0) == 5);
    }

    @Test
    public void can_insert_at_0() throws DefinitionParameterException {
        DefinitionParameters p = parametersOf(1, 2, 3, 4, 5);
        assertTrue(p.insert(0, 0).<Integer>get(0) == 0);
    }

    @Test
    public void get_class_value() throws DefinitionParameterException {
        DefinitionParameters p = parametersOf("42");
        assertTrue(p.<String>getOrNull(String.class).equals("42"));
        assertTrue(!p.<String>getOrNull(String.class).equals("43"));
        assertTrue(p.<Integer>getOrNull(Integer.class) == null);
    }

    @Test
    public void ambiguous_values() throws DefinitionParameterException {
        DefinitionParameters p = parametersOf("42", "43");
        try {
            p.<String>getOrNull(String.class);
            fail();
        } catch (DefinitionParameterException e) {
            e.printStackTrace();
        }
    }
}
