package org.koin.core;

import org.junit.Test;
import org.koin.Simple;
import org.koin.core.definition.Definition;
import org.koin.core.error.*;
import org.koin.core.logger.Level;
import org.koin.core.parameter.DefinitionParameters;
import org.koin.core.scope.Scope;
import org.koin.dsl.KoinAppDeclaration;
import org.koin.dsl.Module;
import org.koin.dsl.ModuleDeclaration;
import org.koin.dsl.ScopeDSL;

import static org.junit.Assert.assertTrue;
import static org.koin.core.qualifier.Qualifier.named;
import static org.koin.dsl.KoinApplication.koinApplication;

public class ScopeShadowingTest {

    @Test
    public void cannot_get_scoped_dependency_without_scope_from_single() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        Koin koin = koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);

                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.single(Simple.MySingle.class, new Definition<Simple.MySingle>() {
                                @Override
                                public Simple.MySingle invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Simple.MySingle(24);
                                }
                            });

                            module.scope(ClosedScopeAPI.ScopeType.class, new org.koin.core.module.Module.ScopeSet() {
                                @Override
                                public void invoke(ScopeDSL scopeDSL) {
                                    try {
                                        scopeDSL.scoped(Simple.MySingle.class, new Definition<Simple.MySingle>() {
                                            @Override
                                            public Simple.MySingle invoke(Scope scope, DefinitionParameters parameters) {
                                                return new Simple.MySingle(42);
                                            }
                                        });
                                    } catch (DefinitionOverrideException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });

                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });

                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        Scope scope = koin.createScope("scope", named(ClosedScopeAPI.ScopeType.class));
        assertTrue(42 == scope.<Simple.MySingle>get(Simple.MySingle.class).getId());
        assertTrue(24 == koin.<Simple.MySingle>get(Simple.MySingle.class).getId());
    }

}
