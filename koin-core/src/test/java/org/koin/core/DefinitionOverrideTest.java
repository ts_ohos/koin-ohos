package org.koin.core;

import org.junit.Assert;
import org.junit.Test;
import org.koin.Simple;
import org.koin.core.definition.Definition;
import org.koin.core.error.*;
import org.koin.core.parameter.DefinitionParameters;
import org.koin.core.qualifier.Qualifier;
import org.koin.core.scope.Scope;
import org.koin.dsl.KoinAppDeclaration;
import org.koin.dsl.KoinApplication;
import org.koin.dsl.*;
import org.koin.test.KoinApplicationExt;

public class DefinitionOverrideTest {

    @Test
    public void allowOverridesByType() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {

        org.koin.core.KoinApplication app = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.single(Simple.ComponentInterface1.class, new Definition<Simple.ComponentInterface1>() {
                                @Override
                                public Simple.ComponentInterface1 invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Simple.Component2();
                                }
                            });
                            module.single(Simple.ComponentInterface1.class, null, false, true, new Definition<Simple.ComponentInterface1>() {
                                @Override
                                public Simple.ComponentInterface1 invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Simple.Component1();
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        KoinApplicationExt.assertDefinitionsCount(app.getKoin(), 1);
        Assert.assertTrue(app.getKoin().get(Simple.ComponentInterface1.class) instanceof Simple.Component1);
    }

    @Test
    public void allowOverridesByTypeScop() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        org.koin.core.KoinApplication app = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        module.scope(Simple.ComponentA.class, new org.koin.core.module.Module.ScopeSet() {
                            @Override
                            public void invoke(ScopeDSL scopeDSL) {
                                try {
                                    scopeDSL.scoped(Simple.ComponentInterface1.class, new Definition<Simple.ComponentInterface1>() {
                                        @Override
                                        public Simple.ComponentInterface1 invoke(Scope scope, DefinitionParameters parameters) {
                                            return new Simple.Component2();
                                        }
                                    });
                                    scopeDSL.scoped(Simple.ComponentInterface1.class, null, true, new Definition<Simple.ComponentInterface1>() {
                                        @Override
                                        public Simple.ComponentInterface1 invoke(Scope scope, DefinitionParameters parameters) {
                                            return new Simple.Component1();
                                        }
                                    });
                                } catch (DefinitionOverrideException e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                    }
                });
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        Scope scope = app.getKoin().createScope(Simple.ComponentA.class, "_ID_");
        Assert.assertTrue(scope.get(Simple.ComponentInterface1.class) instanceof Simple.Component1);

    }

    @Test
    public void allowOverridesByName() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        org.koin.core.KoinApplication app = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.single(Simple.ComponentInterface1.class, Qualifier.named("DEF"), false, false, new Definition<Simple.ComponentInterface1>() {
                                @Override
                                public Simple.ComponentInterface1 invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Simple.Component2();
                                }
                            });
                            module.single(Simple.ComponentInterface1.class, Qualifier.named("DEF"), false, true, new Definition<Simple.ComponentInterface1>() {
                                @Override
                                public Simple.ComponentInterface1 invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Simple.Component1();
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }

            }
        });
        KoinApplicationExt.assertDefinitionsCount(app.getKoin(), 1);
        Assert.assertTrue(app.getKoin().get(Simple.ComponentInterface1.class, Qualifier.named("DEF"), null) instanceof Simple.Component1);
    }


}
