package org.koin.core;

import org.junit.Assert;
import org.junit.Test;
import org.koin.Simple;
import org.koin.core.definition.Definition;
import org.koin.core.error.*;
import org.koin.core.parameter.DefinitionParameters;
import org.koin.core.scope.Scope;
import org.koin.dsl.KoinAppDeclaration;
import org.koin.dsl.Module;
import org.koin.dsl.ModuleDeclaration;
import org.koin.test.KoinApplicationExt;

import static org.koin.dsl.KoinApplication.koinApplication;

public class MultipleModuleDeclarationTest {

    @Test
    public void run_with_DI_with_several_modules() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        KoinApplication app = koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {
                org.koin.core.module.Module module1 = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.single(Simple.ComponentA.class, new Definition<Simple.ComponentA>() {
                                @Override
                                public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Simple.ComponentA();
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });

                org.koin.core.module.Module module2 = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.single(Simple.ComponentB.class, new Definition<Simple.ComponentB>() {
                                @Override
                                public Simple.ComponentB invoke(Scope scope, DefinitionParameters parameters) {
                                    try {
                                        return new Simple.ComponentB(scope.get(Simple.ComponentA.class));
                                    } catch (ClosedScopeException e) {
                                        e.printStackTrace();
                                    } catch (DefinitionParameterException e) {
                                        e.printStackTrace();
                                    } catch (NoBeanDefFoundException e) {
                                        e.printStackTrace();
                                    }
                                    return null;
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });

                try {
                    koinApplication.modules(module1, module2);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        KoinApplicationExt.assertDefinitionsCount(app.getKoin(), 2);
    }

    @Test
    public void resolve_DI_with_serveral_modules() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        KoinApplication app = koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {
                org.koin.core.module.Module module1 = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.single(Simple.ComponentA.class, new Definition<Simple.ComponentA>() {
                                @Override
                                public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Simple.ComponentA();
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });

                org.koin.core.module.Module module2 = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.single(Simple.ComponentB.class, new Definition<Simple.ComponentB>() {
                                @Override
                                public Simple.ComponentB invoke(Scope scope, DefinitionParameters parameters) {
                                    try {
                                        return new Simple.ComponentB(scope.get(Simple.ComponentA.class));
                                    } catch (ClosedScopeException e) {
                                        e.printStackTrace();
                                    } catch (DefinitionParameterException e) {
                                        e.printStackTrace();
                                    } catch (NoBeanDefFoundException e) {
                                        e.printStackTrace();
                                    }
                                    return null;
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });

                try {
                    koinApplication.modules(module1, module2);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });

        Koin koin = app.getKoin();
        Simple.ComponentA a = koin.<Simple.ComponentA>get(Simple.ComponentA.class);
        Simple.ComponentB b = koin.<Simple.ComponentB>get(Simple.ComponentB.class);

        Assert.assertEquals(a, b.a);
    }
}
