package org.koin.core;

import org.junit.Test;
import org.koin.Simple;
import org.koin.core.definition.Definition;
import org.koin.core.error.*;
import org.koin.core.logger.Level;
import org.koin.core.parameter.DefinitionParameters;
import org.koin.core.qualifier.Qualifier;
import org.koin.core.scope.Scope;
import org.koin.dsl.KoinAppDeclaration;
import org.koin.dsl.KoinApplication;
import org.koin.dsl.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class GlobalToScopeTest {

    @Test
    public void cannot_get_scoped_dependency_without_scope() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {

        Koin koin = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);

                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        module.scope(ClosedScopeAPI.ScopeType.class, new org.koin.core.module.Module.ScopeSet() {
                            @Override
                            public void invoke(ScopeDSL scopeDSL) {
                                try {
                                    scopeDSL.scoped(Simple.ComponentA.class, new Definition<Simple.ComponentA>() {
                                        @Override
                                        public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                            return new Simple.ComponentA();
                                        }
                                    });
                                } catch (DefinitionOverrideException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                });

                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        try {
            koin.get(Simple.ComponentA.class);
            fail();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void cannot_get_scoped_dependency_without_scope_from_single() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        Koin koin = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);

                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.single(Simple.ComponentB.class, new Definition<Simple.ComponentB>() {
                                @Override
                                public Simple.ComponentB invoke(Scope scope, DefinitionParameters parameters) {
                                    try {
                                        return new Simple.ComponentB(scope.get(Simple.ComponentA.class));
                                    } catch (ClosedScopeException e) {
                                        e.printStackTrace();
                                    } catch (DefinitionParameterException e) {
                                        e.printStackTrace();
                                    } catch (NoBeanDefFoundException e) {
                                        e.printStackTrace();
                                    }
                                    return null;
                                }
                            });

                            module.scope(ClosedScopeAPI.ScopeType.class, new org.koin.core.module.Module.ScopeSet() {
                                @Override
                                public void invoke(ScopeDSL scopeDSL) {
                                    try {
                                        scopeDSL.scoped(Simple.ComponentA.class, new Definition<Simple.ComponentA>() {
                                            @Override
                                            public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                                return new Simple.ComponentA();
                                            }
                                        });
                                    } catch (DefinitionOverrideException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });

                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });

                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        try {
            koin.get(Simple.ComponentA.class);
            fail();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void get_scoped_dependency_without_scope_from_single() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        String scopeId = "MY_SCOPE_ID";
        Koin koin = KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(org.koin.core.KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);

                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.single(Simple.ComponentB.class, new Definition<Simple.ComponentB>() {
                                @Override
                                public Simple.ComponentB invoke(Scope scope, DefinitionParameters parameters) {
                                    try {
                                        return new Simple.ComponentB(scope.getScope(scopeId).get(Simple.ComponentA.class));
                                    } catch (ClosedScopeException e) {
                                        e.printStackTrace();
                                    } catch (DefinitionParameterException e) {
                                        e.printStackTrace();
                                    } catch (NoBeanDefFoundException e) {
                                        e.printStackTrace();
                                    } catch (ScopeNotCreatedException e) {
                                        e.printStackTrace();
                                    }
                                    return null;
                                }
                            });

                            module.scope(Qualifier.named(ClosedScopeAPI.ScopeType.class), new org.koin.core.module.Module.ScopeSet() {
                                @Override
                                public void invoke(ScopeDSL scopeDSL) {
                                    try {
                                        scopeDSL.scoped(Simple.ComponentA.class, new Definition<Simple.ComponentA>() {
                                            @Override
                                            public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                                return new Simple.ComponentA();
                                            }
                                        });
                                    } catch (DefinitionOverrideException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });

                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });

                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        Scope scope = koin.createScope(scopeId, Qualifier.named(ClosedScopeAPI.ScopeType.class));
        assertEquals(koin.<Simple.ComponentB>get(Simple.ComponentB.class).a, scope.get(Simple.ComponentA.class));
    }
}
