package org.koin.core;

import org.junit.Assert;
import org.junit.Test;
import org.koin.core.definition.Properties;
import org.koin.core.error.MissingPropertyException;

public class AttributesTest {

    @Test
    public void can_store_get_an_attribute_value() {
        Properties attr = new Properties();
        attr.set("myKey", "myString");
        String string = "";
        try {
            string = attr.get("myKey");
        } catch (MissingPropertyException e) {
            e.printStackTrace();
        }

        Assert.assertEquals(string, "myString");
    }

    @Test
    public void attribute_empty_no_value() {
        Properties attr = new Properties();
        Assert.assertTrue(attr.getOrNull("myKey") == null);
    }

    @Test
    public void attribute_value_overwrite() {
        Properties attr = new Properties();
        attr.set("myKey", "myString");
        attr.set("myKey", "myString2");

        String string = "";
        try {
            string = attr.get("myKey");
        } catch (MissingPropertyException e) {
            e.printStackTrace();
        }
        Assert.assertEquals("myString2", string);
    }
}
