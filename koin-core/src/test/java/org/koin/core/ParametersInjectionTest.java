package org.koin.core;

import org.junit.Test;
import org.koin.Simple;
import org.koin.core.definition.Definition;
import org.koin.core.error.*;
import org.koin.core.logger.Level;
import org.koin.core.parameter.DefinitionParameters;
import org.koin.core.parameter.ParametersDefinition;
import org.koin.core.scope.Scope;
import org.koin.dsl.KoinAppDeclaration;
import org.koin.dsl.Module;
import org.koin.dsl.ModuleDeclaration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.koin.dsl.KoinApplication.koinApplication;

public class ParametersInjectionTest {

    @Test
    public void can_create_a_single_with_parameters() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        Koin koin = koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {

                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.single(Simple.MySingle.class, new Definition<Simple.MySingle>() {
                                @Override
                                public Simple.MySingle invoke(Scope scope, DefinitionParameters parameters) {
                                    if (parameters.isNotEmpty()) {
                                        return new Simple.MySingle(parameters.get(0));
                                    }
                                    return null;
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });

                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        Simple.MySingle a = koin.get(Simple.MySingle.class, null, new ParametersDefinition() {
            @Override
            public DefinitionParameters invoke() {
                try {
                    return DefinitionParameters.parametersOf(42);
                } catch (DefinitionParameterException e) {
                    e.printStackTrace();
                }
                return null;
            }
        });

        assertEquals(42, a.getId());
    }

    @Test
    public void can_create_a_single_with_parameters_resolved_in_graph() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        Koin koin = koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {

                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.factory(Simple.MySingle.class, new Definition<Simple.MySingle>() {
                                @Override
                                public Simple.MySingle invoke(Scope scope, DefinitionParameters parameters) {
                                    try {
                                        return new Simple.MySingle(scope.get(Integer.class));
                                    } catch (ClosedScopeException e) {
                                        e.printStackTrace();
                                    } catch (DefinitionParameterException e) {
                                        e.printStackTrace();
                                    } catch (NoBeanDefFoundException e) {
                                        e.printStackTrace();
                                    }
                                    return null;
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });

                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        Simple.MySingle a42 = koin.get(Simple.MySingle.class, new ParametersDefinition() {
            @Override
            public DefinitionParameters invoke() {
                try {
                    return DefinitionParameters.parametersOf(42);
                } catch (DefinitionParameterException e) {
                    e.printStackTrace();
                }
                return null;
            }
        });

        assertTrue(42 == a42.getId());

        Simple.MySingle a24 = koin.get(Simple.MySingle.class, new ParametersDefinition() {
            @Override
            public DefinitionParameters invoke() {
                try {
                    return DefinitionParameters.parametersOf(24);
                } catch (DefinitionParameterException e) {
                    e.printStackTrace();
                }
                return null;
            }
        });

        assertTrue(24 == a24.getId());
    }

    @Test
    public void can_create_a_single_with_parameters_using_param_object_resolution() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        Koin koin = koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {

                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.single(Simple.MySingle.class, new Definition<Simple.MySingle>() {
                                @Override
                                public Simple.MySingle invoke(Scope scope, DefinitionParameters parameters) {
                                    try {
                                        return new Simple.MySingle(parameters.get(Integer.class));
                                    } catch (DefinitionParameterException e) {
                                        e.printStackTrace();
                                    }
                                    return null;
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });

                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        Simple.MySingle a = koin.get(Simple.MySingle.class, new ParametersDefinition() {
            @Override
            public DefinitionParameters invoke() {
                try {
                    return DefinitionParameters.parametersOf(42);
                } catch (DefinitionParameterException e) {
                    e.printStackTrace();
                }
                return null;
            }
        });

        assertTrue(42 == a.getId());
    }

    @Test
    public void can_create_a_single_with_nullable_parameters() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        Koin koin = koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);

                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.single(Simple.MySingleWithNull.class, new Definition<Simple.MySingleWithNull>() {
                                @Override
                                public Simple.MySingleWithNull invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Simple.MySingleWithNull(parameters.get(0));
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });

                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        Simple.MySingleWithNull a = koin.get(Simple.MySingleWithNull.class, new ParametersDefinition() {
            @Override
            public DefinitionParameters invoke() {
                try {
                    return DefinitionParameters.parametersOf(new Object[]{null});
                } catch (DefinitionParameterException e) {
                    e.printStackTrace();
                }
                return null;
            }
        });

        assertTrue(a.id == null);
    }

    @Test
    public void can_get_a_single_created_with_parameters_no_need_of_give_it_again() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        Koin koin = koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);
                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.single(Simple.MySingle.class, new Definition<Simple.MySingle>() {
                                @Override
                                public Simple.MySingle invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Simple.MySingle(parameters.get(0));
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        Simple.MySingle a = koin.get(Simple.MySingle.class, new ParametersDefinition() {
            @Override
            public DefinitionParameters invoke() {
                try {
                    return DefinitionParameters.parametersOf(42);
                } catch (DefinitionParameterException e) {
                    e.printStackTrace();
                }
                return null;
            }

        });

        assertTrue(42 == a.getId());

        Simple.MySingle a2 = koin.<Simple.MySingle>get(Simple.MySingle.class);

        assertTrue(42 == a2.getId());
    }

    @Test
    public void can_create_factories_with_params() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        Koin koin = koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {
                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.factory(Simple.MyIntFactory.class, new Definition<Simple.MyIntFactory>() {
                                @Override
                                public Simple.MyIntFactory invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Simple.MyIntFactory(parameters.get(0));
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        Simple.MyIntFactory a = koin.get(Simple.MyIntFactory.class, new ParametersDefinition() {
            @Override
            public DefinitionParameters invoke() {
                try {
                    return DefinitionParameters.parametersOf(42);
                } catch (DefinitionParameterException e) {
                    e.printStackTrace();
                }
                return null;
            }
        });

        Simple.MyIntFactory a2 = koin.get(Simple.MyIntFactory.class, new ParametersDefinition() {
            @Override
            public DefinitionParameters invoke() {
                try {
                    return DefinitionParameters.parametersOf(43);
                } catch (DefinitionParameterException e) {
                    e.printStackTrace();
                }
                return null;
            }
        });

        assertTrue(42 == a.id);
        assertTrue(43 == a2.id);
    }

    @Test
    public void chained_factory_injection() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        Koin koin = koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {
                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.factory(Simple.MyIntFactory.class, new Definition<Simple.MyIntFactory>() {
                                @Override
                                public Simple.MyIntFactory invoke(Scope scope, DefinitionParameters parameters) {
                                    try {
                                        return new Simple.MyIntFactory(parameters.get(Integer.class));
                                    } catch (DefinitionParameterException e) {
                                        e.printStackTrace();
                                    }
                                    return null;
                                }

                            });

                            module.factory(Simple.MyStringFactory.class, new Definition<Simple.MyStringFactory>() {
                                @Override
                                public Simple.MyStringFactory invoke(Scope scope, DefinitionParameters parameters) {
                                    try {
                                        return new Simple.MyStringFactory(parameters.get(String.class));
                                    } catch (DefinitionParameterException e) {
                                        e.printStackTrace();
                                    }
                                    return null;
                                }
                            });

                            module.factory(Simple.AllFactory.class, new Definition<Simple.AllFactory>() {
                                @Override
                                public Simple.AllFactory invoke(Scope scope, DefinitionParameters parameters) {
                                    Integer i = parameters.get(0);
                                    String s = parameters.get(1);
                                    try {
                                        Simple.MyIntFactory i1 = scope.get(Simple.MyIntFactory.class, new ParametersDefinition() {
                                            @Override
                                            public DefinitionParameters invoke() {
                                                try {
                                                    return DefinitionParameters.parametersOf(i);
                                                } catch (DefinitionParameterException e) {
                                                    e.printStackTrace();
                                                }
                                                return null;
                                            }
                                        });

                                        Simple.MyStringFactory s1 = scope.get(Simple.MyStringFactory.class, new ParametersDefinition() {
                                            @Override
                                            public DefinitionParameters invoke() {

                                                try {
                                                    return DefinitionParameters.parametersOf(s);
                                                } catch (DefinitionParameterException e) {
                                                    e.printStackTrace();
                                                }

                                                return null;
                                            }
                                        });

                                        return new Simple.AllFactory(i1, s1);

                                    } catch (ClosedScopeException e) {
                                        e.printStackTrace();
                                    } catch (DefinitionParameterException e) {
                                        e.printStackTrace();
                                    } catch (NoBeanDefFoundException e) {
                                        e.printStackTrace();
                                    }
                                    return null;
                                }
                            });

                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });

                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        Simple.AllFactory f = koin.get(Simple.AllFactory.class, new ParametersDefinition() {
            @Override
            public DefinitionParameters invoke() {
                try {
                    return DefinitionParameters.parametersOf(42, "42");
                } catch (DefinitionParameterException e) {
                    e.printStackTrace();
                }
                return null;
            }
        });

        assertTrue(42 == f.ints.id);
        assertTrue("42" == f.strings.s);

    }

    @Test
    public void inject_in_graph() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        Koin koin = koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);

                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.single(Simple.MySingle.class, new Definition<Simple.MySingle>() {
                                @Override
                                public Simple.MySingle invoke(Scope scope, DefinitionParameters parameters) {
                                    try {
                                        return new Simple.MySingle(scope.get(Integer.class));
                                    } catch (ClosedScopeException e) {
                                        e.printStackTrace();
                                    } catch (DefinitionParameterException e) {
                                        e.printStackTrace();
                                    } catch (NoBeanDefFoundException e) {
                                        e.printStackTrace();
                                    }
                                    return null;
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });

                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        Simple.MySingle a = koin.get(Simple.MySingle.class, new ParametersDefinition() {
            @Override
            public DefinitionParameters invoke() {
                try {
                    return DefinitionParameters.parametersOf(42);
                } catch (DefinitionParameterException e) {
                    e.printStackTrace();
                }
                return null;
            }
        });

        assertTrue(42 == a.getId());
    }

    @Test
    public void chained_factory_injection_graph() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        Koin koin = koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {
                koinApplication.printLogger(Level.DEBUG);

                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.factory(Simple.MyIntFactory.class, new Definition<Simple.MyIntFactory>() {
                                @Override
                                public Simple.MyIntFactory invoke(Scope scope, DefinitionParameters parameters) {
                                    try {
                                        return new Simple.MyIntFactory(scope.get(Integer.class));
                                    } catch (ClosedScopeException e) {
                                        e.printStackTrace();
                                    } catch (DefinitionParameterException e) {
                                        e.printStackTrace();
                                    } catch (NoBeanDefFoundException e) {
                                        e.printStackTrace();
                                    }
                                    return null;
                                }
                            });

                            module.factory(Simple.MyStringFactory.class, new Definition<Simple.MyStringFactory>() {
                                @Override
                                public Simple.MyStringFactory invoke(Scope scope, DefinitionParameters parameters) {

                                    try {
                                        return new Simple.MyStringFactory(scope.get(String.class));
                                    } catch (ClosedScopeException e) {
                                        e.printStackTrace();
                                    } catch (DefinitionParameterException e) {
                                        e.printStackTrace();
                                    } catch (NoBeanDefFoundException e) {
                                        e.printStackTrace();
                                    }

                                    return null;
                                }
                            });

                            module.factory(Simple.AllFactory.class, new Definition<Simple.AllFactory>() {
                                @Override
                                public Simple.AllFactory invoke(Scope scope, DefinitionParameters parameters) {

                                    Integer i = parameters.get(0);
                                    String s = parameters.get(1);

                                    try {
                                        Simple.MyIntFactory myIntFactory = scope.<Simple.MyIntFactory>get(Simple.MyIntFactory.class, new ParametersDefinition() {
                                            @Override
                                            public DefinitionParameters invoke() {
                                                try {
                                                    return DefinitionParameters.parametersOf(i);
                                                } catch (DefinitionParameterException e) {
                                                    e.printStackTrace();
                                                }
                                                return null;
                                            }
                                        });

                                        Simple.MyStringFactory myStringFactory = scope.<Simple.MyStringFactory>get(Simple.MyStringFactory.class, new ParametersDefinition() {
                                            @Override
                                            public DefinitionParameters invoke() {
                                                try {
                                                    return DefinitionParameters.parametersOf(s);
                                                } catch (DefinitionParameterException e) {
                                                    e.printStackTrace();
                                                }
                                                return null;
                                            }
                                        });

                                        return new Simple.AllFactory(myIntFactory, myStringFactory);
                                    } catch (ClosedScopeException e) {
                                        e.printStackTrace();
                                    } catch (DefinitionParameterException e) {
                                        e.printStackTrace();
                                    } catch (NoBeanDefFoundException e) {
                                        e.printStackTrace();
                                    }


                                    return null;
                                }
                            });

                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });

                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        Simple.AllFactory f = koin.get(Simple.AllFactory.class, new ParametersDefinition() {
            @Override
            public DefinitionParameters invoke() {
                try {
                    return DefinitionParameters.parametersOf(42, "42");
                } catch (DefinitionParameterException e) {
                    e.printStackTrace();
                }
                return null;
            }
        });

        assertTrue(42 == f.ints.id);
        assertTrue("42".equals(f.strings.s));

    }
}
