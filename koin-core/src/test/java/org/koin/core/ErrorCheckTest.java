package org.koin.core;

import org.junit.Assert;
import org.junit.Test;
import org.koin.Errors;
import org.koin.Simple;
import org.koin.core.definition.Definition;
import org.koin.core.error.*;
import org.koin.core.parameter.DefinitionParameters;
import org.koin.core.scope.Scope;
import org.koin.dsl.KoinAppDeclaration;
import org.koin.dsl.Module;
import org.koin.dsl.ModuleDeclaration;


public class ErrorCheckTest {

    @Test
    public void unknown_definition() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        org.koin.core.KoinApplication app = org.koin.dsl.KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {
            }
        });
        try {
            app.getKoin().get(Simple.ComponentA.class);
            Assert.fail("should not get instancen");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void unknown_linked_dependency() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        org.koin.core.KoinApplication app = org.koin.dsl.KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {
                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.single(Simple.ComponentB.class, new Definition<Simple.ComponentB>() {
                                @Override
                                public Simple.ComponentB invoke(Scope scope, DefinitionParameters parameters) {
                                    try {
                                        return new Simple.ComponentB(scope.get(Simple.ComponentA.class));
                                    } catch (ClosedScopeException e) {
                                        e.printStackTrace();
                                    } catch (DefinitionParameterException e) {
                                        e.printStackTrace();
                                    } catch (NoBeanDefFoundException e) {
                                        e.printStackTrace();
                                    }
                                    return null;
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });
        try {
            app.getKoin().get(Simple.ComponentB.class);
            Assert.fail("should not get instance");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void error_while_creating_instance() throws ScopeAlreadyCreatedException, NoScopeDefFoundException {
        org.koin.core.KoinApplication app = org.koin.dsl.KoinApplication.koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {
                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.single(Errors.Boom.class, new Definition<Errors.Boom>() {
                                @Override
                                public Errors.Boom invoke(Scope scope, DefinitionParameters parameters) {
                                    return new Errors.Boom();
                                }
                            });
                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });
                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        });
        try {
            app.getKoin().get(org.koin.Errors.Boom.class);
            Assert.fail("should got InstanceCreationException");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
