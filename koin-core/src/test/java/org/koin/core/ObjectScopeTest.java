package org.koin.core;

import org.junit.Assert;
import org.junit.Test;
import org.koin.Simple;
import org.koin.core.context.DefaultContextExt;
import org.koin.core.definition.Definition;
import org.koin.core.error.*;
import org.koin.core.logger.Level;
import org.koin.core.parameter.DefinitionParameters;
import org.koin.core.scope.Scope;
import org.koin.core.setterinjecttest.*;
import org.koin.dsl.KoinAppDeclaration;
import org.koin.dsl.Module;
import org.koin.dsl.ModuleDeclaration;
import org.koin.dsl.ScopeDSL;

import static org.koin.core.context.DefaultContextExt.startKoin;
import static org.koin.core.context.DefaultContextExt.stopKoin;
import static org.koin.dsl.KoinApplication.koinApplication;

public class ObjectScopeTest {

    @Test
    public void typed_scope() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {

        Koin koin = koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {

                koinApplication.printLogger(Level.DEBUG);

                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.single(A.class, new Definition<A>() {
                                @Override
                                public A invoke(Scope scope, DefinitionParameters parameters) {
                                    return new A();
                                }
                            });

                            module.scope(A.class, new org.koin.core.module.Module.ScopeSet() {
                                @Override
                                public void invoke(ScopeDSL scopeDSL) {
                                    try {
                                        scopeDSL.scoped(B.class, new Definition<B>() {
                                            @Override
                                            public B invoke(Scope scope, DefinitionParameters parameters) {
                                                return new B();
                                            }
                                        });

                                        scopeDSL.scoped(C.class, new Definition<C>() {
                                            @Override
                                            public C invoke(Scope scope, DefinitionParameters parameters) {
                                                return new C();
                                            }
                                        });

                                    } catch (DefinitionOverrideException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });

                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });

                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        Assert.assertNotNull(koin.get(A.class));
        Assert.assertNull(koin.getOrNull(B.class));
        Assert.assertNull(koin.getOrNull(C.class));

        stopKoin();
    }

    @Test
    public void typed_scope_source() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, KoinAppAlreadyStartedException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {

        Koin koin = DefaultContextExt.startKoin(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {

                koinApplication.printLogger(Level.DEBUG);

                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.single(A.class, new Definition<A>() {
                                @Override
                                public A invoke(Scope scope, DefinitionParameters parameters) {
                                    return new A();
                                }
                            });

                            module.scope(A.class, new org.koin.core.module.Module.ScopeSet() {
                                @Override
                                public void invoke(ScopeDSL scopeDSL) {
                                    try {
                                        scopeDSL.scoped(BofA.class, new Definition<BofA>() {
                                            @Override
                                            public BofA invoke(Scope scope, DefinitionParameters parameters) {
                                                try {
                                                    return new BofA(scope.get(A.class));
                                                } catch (ClosedScopeException e) {
                                                    e.printStackTrace();
                                                } catch (DefinitionParameterException e) {
                                                    e.printStackTrace();
                                                } catch (NoBeanDefFoundException e) {
                                                    e.printStackTrace();
                                                }
                                                return null;
                                            }
                                        });
                                    } catch (DefinitionOverrideException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });

                            module.scope(BofA.class, new org.koin.core.module.Module.ScopeSet() {
                                @Override
                                public void invoke(ScopeDSL scopeDSL) {
                                    try {
                                        scopeDSL.scoped(CofB.class, new Definition<CofB>() {
                                            @Override
                                            public CofB invoke(Scope scope, DefinitionParameters parameters) {
                                                return new CofB(scope.getSource(BofA.class));
                                            }
                                        });
                                    } catch (DefinitionOverrideException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });

                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });

                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        A a = koin.<A>get(A.class);
        BofA b = a.getScope().<BofA>get(BofA.class);
        Assert.assertTrue(b.a.equals(a));
        CofB c = b.getScope().<CofB>get(CofB.class);
        Assert.assertTrue(c.getB().equals(b));

        stopKoin();
    }

    @Test
    public void typed_scope_source_with_get() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, KoinAppAlreadyStartedException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        Koin koin = startKoin(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {

                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.single(A.class, new Definition<A>() {
                                @Override
                                public A invoke(Scope scope, DefinitionParameters parameters) {
                                    return new A();
                                }
                            });

                            module.scope(A.class, new org.koin.core.module.Module.ScopeSet() {
                                @Override
                                public void invoke(ScopeDSL scopeDSL) {
                                    try {
                                        scopeDSL.scoped(BofA.class, new Definition<BofA>() {
                                            @Override
                                            public BofA invoke(Scope scope, DefinitionParameters parameters) {
                                                try {
                                                    return new BofA(scope.get(A.class));
                                                } catch (ClosedScopeException e) {
                                                    e.printStackTrace();
                                                } catch (DefinitionParameterException e) {
                                                    e.printStackTrace();
                                                } catch (NoBeanDefFoundException e) {
                                                    e.printStackTrace();
                                                }
                                                return null;
                                            }
                                        });
                                    } catch (DefinitionOverrideException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });

                            module.scope(BofA.class, new org.koin.core.module.Module.ScopeSet() {
                                @Override
                                public void invoke(ScopeDSL scopeDSL) {
                                    try {
                                        scopeDSL.scoped(CofB.class, new Definition<CofB>() {
                                            @Override
                                            public CofB invoke(Scope scope, DefinitionParameters parameters) {
                                                try {
                                                    return new CofB(scope.get(BofA.class));
                                                } catch (ClosedScopeException e) {
                                                    e.printStackTrace();
                                                } catch (DefinitionParameterException e) {
                                                    e.printStackTrace();
                                                } catch (NoBeanDefFoundException e) {
                                                    e.printStackTrace();
                                                }
                                                return null;
                                            }
                                        });
                                    } catch (DefinitionOverrideException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });

                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });

                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        A a = koin.<A>get(A.class);
        BofA b = a.getScope().<BofA>get(BofA.class);
        Assert.assertTrue(b.a.equals(a));
        CofB c = b.getScope().<CofB>get(CofB.class);
        Assert.assertTrue(c.getB().equals(b));

        stopKoin();
    }

    @Test
    public void scope_from_instance_object() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, KoinAppAlreadyStartedException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        Koin koin = startKoin(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {

                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.single(A.class, new Definition<A>() {
                                @Override
                                public A invoke(Scope scope, DefinitionParameters parameters) {
                                    return new A();
                                }
                            });

                            module.scope(A.class, new org.koin.core.module.Module.ScopeSet() {
                                @Override
                                public void invoke(ScopeDSL scopeDSL) {
                                    try {
                                        scopeDSL.scoped(B.class, new Definition<B>() {
                                            @Override
                                            public B invoke(Scope scope, DefinitionParameters parameters) {
                                                return new B();
                                            }
                                        });

                                        scopeDSL.scoped(C.class, new Definition<C>() {
                                            @Override
                                            public C invoke(Scope scope, DefinitionParameters parameters) {
                                                return new C();
                                            }
                                        });

                                    } catch (DefinitionOverrideException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });

                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });

                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        A a = koin.<A>get(A.class);
        Scope scopeForA = koin.createScope(A.class, a);
        B b1 = scopeForA.<B>get(B.class);
        Assert.assertNotNull(b1);
        Assert.assertNotNull(scopeForA.get(C.class));

        scopeForA.close();

        Assert.assertNull(scopeForA.getOrNull(B.class));
        Assert.assertNull(scopeForA.getOrNull(C.class));

        Scope scopeForA2 = koin.createScope(A.class, a);

        B b2 = scopeForA2.<B>getOrNull(B.class);
        Assert.assertNotNull(b2);
        Assert.assertNotEquals(b1, b2);

        stopKoin();
    }

    @Test
    public void scope_property() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, KoinAppAlreadyStartedException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        Koin koin = startKoin(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {

                koinApplication.printLogger(Level.DEBUG);

                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.single(A.class, new Definition<A>() {
                                @Override
                                public A invoke(Scope scope, DefinitionParameters parameters) {
                                    return new A();
                                }
                            });

                            module.scope(A.class, new org.koin.core.module.Module.ScopeSet() {
                                @Override
                                public void invoke(ScopeDSL scopeDSL) {
                                    try {
                                        scopeDSL.scoped(B.class, new Definition<B>() {
                                            @Override
                                            public B invoke(Scope scope, DefinitionParameters parameters) {
                                                return new B();
                                            }
                                        });

                                        scopeDSL.scoped(C.class, new Definition<C>() {
                                            @Override
                                            public C invoke(Scope scope, DefinitionParameters parameters) {
                                                return new C();
                                            }
                                        });

                                    } catch (DefinitionOverrideException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });

                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });

                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        A a = koin.<A>get(A.class);

        B b1 = a.getScope().<B>get(B.class);
        Assert.assertNotNull(b1);
        Assert.assertNotNull(a.getScope().get(C.class));

        a.getScope().close();

        try {
            a.getScope().get(B.class);
        } catch (Exception e) {

        }
        stopKoin();
    }

    @Test
    public void scope_property_koin_isolation() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        Koin koin = koinApplication(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {

                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.single(A.class, new Definition<A>() {
                                @Override
                                public A invoke(Scope scope, DefinitionParameters parameters) {
                                    return new A();
                                }
                            });

                            module.scope(A.class, new org.koin.core.module.Module.ScopeSet() {
                                @Override
                                public void invoke(ScopeDSL scopeDSL) {
                                    try {
                                        scopeDSL.scoped(B.class, new Definition<B>() {
                                            @Override
                                            public B invoke(Scope scope, DefinitionParameters parameters) {
                                                return new B();
                                            }
                                        });
                                    } catch (DefinitionOverrideException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });

                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });

                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        A a = koin.get(A.class);

        Scope scope = koin.createScope(A.class, a);
        B b1 = scope.<B>get(B.class);
        scope.close();

        scope = koin.createScope(A.class, a);
        B b2 = scope.<B>get(B.class);

        Assert.assertNotEquals(b1, b2);
    }

    @Test
    public void cascade_scope() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, KoinAppAlreadyStartedException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        Koin koin = startKoin(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {

                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.factory(A.class, new Definition<A>() {
                                @Override
                                public A invoke(Scope scope, DefinitionParameters parameters) {
                                    return new A();
                                }
                            });

                            module.scope(A.class, new org.koin.core.module.Module.ScopeSet() {
                                @Override
                                public void invoke(ScopeDSL scopeDSL) {
                                    try {
                                        scopeDSL.scoped(B.class, new Definition<B>() {
                                            @Override
                                            public B invoke(Scope scope, DefinitionParameters parameters) {
                                                return new B();
                                            }
                                        });
                                    } catch (DefinitionOverrideException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });

                            module.scope(B.class, new org.koin.core.module.Module.ScopeSet() {
                                @Override
                                public void invoke(ScopeDSL scopeDSL) {
                                    try {
                                        scopeDSL.scoped(C.class, new Definition<C>() {
                                            @Override
                                            public C invoke(Scope scope, DefinitionParameters parameters) {
                                                return new C();
                                            }
                                        });
                                    } catch (DefinitionOverrideException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });

                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });

                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        A a = koin.<A>get(A.class);
        B b1 = a.getScope().<B>get(B.class);
        C c1 = b1.getScope().<C>get(C.class);

        a.getScope().close();
        b1.getScope().close();

        a = koin.get(A.class);
        B b2 = a.getScope().<B>get(B.class);
        C c2 = b2.getScope().<C>get(C.class);

        Assert.assertNotEquals(b1, b2);
        Assert.assertNotEquals(c1, c2);

        stopKoin();
    }

    @Test
    public void cascade_linked_scope() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, KoinAppAlreadyStartedException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        Koin koin = startKoin(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {

                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.single(A.class, new Definition<A>() {
                                @Override
                                public A invoke(Scope scope, DefinitionParameters parameters) {
                                    return new A();
                                }
                            });

                            module.scope(A.class, new org.koin.core.module.Module.ScopeSet() {
                                @Override
                                public void invoke(ScopeDSL scopeDSL) {
                                    try {
                                        scopeDSL.scoped(B.class, new Definition<B>() {
                                            @Override
                                            public B invoke(Scope scope, DefinitionParameters parameters) {
                                                return new B();
                                            }
                                        });
                                    } catch (DefinitionOverrideException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });

                            module.scope(B.class, new org.koin.core.module.Module.ScopeSet() {
                                @Override
                                public void invoke(ScopeDSL scopeDSL) {
                                    try {
                                        scopeDSL.scoped(C.class, new Definition<C>() {
                                            @Override
                                            public C invoke(Scope scope, DefinitionParameters parameters) {
                                                return new C();
                                            }
                                        });
                                    } catch (DefinitionOverrideException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });

                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });

                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        A a = koin.<A>get(A.class);
        B b = a.getScope().<B>get(B.class);
        a.getScope().linkTo(b.getScope());
        Assert.assertTrue(a.getScope().<C>get(C.class).equals(b.getScope().<C>get(C.class)));
        stopKoin();
    }

    @Test
    public void cascade_unlink_scope() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, KoinAppAlreadyStartedException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        Koin koin = startKoin(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {
                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.single(A.class, new Definition<A>() {
                                @Override
                                public A invoke(Scope scope, DefinitionParameters parameters) {
                                    return new A();
                                }
                            });

                            module.scope(A.class, new org.koin.core.module.Module.ScopeSet() {
                                @Override
                                public void invoke(ScopeDSL scopeDSL) {
                                    try {
                                        scopeDSL.scoped(B.class, new Definition<B>() {
                                            @Override
                                            public B invoke(Scope scope, DefinitionParameters parameters) {
                                                return new B();
                                            }
                                        });
                                    } catch (DefinitionOverrideException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });

                            module.scope(B.class, new org.koin.core.module.Module.ScopeSet() {
                                @Override
                                public void invoke(ScopeDSL scopeDSL) {
                                    try {
                                        scopeDSL.scoped(C.class, new Definition<C>() {
                                            @Override
                                            public C invoke(Scope scope, DefinitionParameters parameters) {
                                                return new C();
                                            }
                                        });
                                    } catch (DefinitionOverrideException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });

                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });

                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        A a = koin.<A>get(A.class);
        B b1 = a.getScope().<B>get(B.class);
        a.getScope().linkTo(b1.getScope());
        C c1 = a.getScope().get(C.class);
        Assert.assertNotNull(c1);

        a.getScope().unlink(b1.getScope());
        Assert.assertNull(a.getScope().getOrNull(C.class));
        stopKoin();
    }

    @Test
    public void shared_linked_scope() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, KoinAppAlreadyStartedException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        Koin koin = startKoin(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {

                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        module.scope(A.class, new org.koin.core.module.Module.ScopeSet() {
                            @Override
                            public void invoke(ScopeDSL scopeDSL) {
                                try {
                                    scopeDSL.scoped(Simple.ComponentB.class, new Definition<Simple.ComponentB>() {
                                        @Override
                                        public Simple.ComponentB invoke(Scope scope, DefinitionParameters parameters) {
                                            try {
                                                return new Simple.ComponentB(scope.get(Simple.ComponentA.class));
                                            } catch (ClosedScopeException e) {
                                                e.printStackTrace();
                                            } catch (DefinitionParameterException e) {
                                                e.printStackTrace();
                                            } catch (NoBeanDefFoundException e) {
                                                e.printStackTrace();
                                            }
                                            return null;
                                        }
                                    });
                                } catch (DefinitionOverrideException e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                        module.scope(B.class, new org.koin.core.module.Module.ScopeSet() {
                            @Override
                            public void invoke(ScopeDSL scopeDSL) {
                                try {
                                    scopeDSL.scoped(Simple.ComponentB.class, new Definition<Simple.ComponentB>() {
                                        @Override
                                        public Simple.ComponentB invoke(Scope scope, DefinitionParameters parameters) {
                                            try {
                                                return new Simple.ComponentB(scope.get(Simple.ComponentA.class));
                                            } catch (ClosedScopeException e) {
                                                e.printStackTrace();
                                            } catch (DefinitionParameterException e) {
                                                e.printStackTrace();
                                            } catch (NoBeanDefFoundException e) {
                                                e.printStackTrace();
                                            }
                                            return null;
                                        }
                                    });
                                } catch (DefinitionOverrideException e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                        module.scope(C.class, new org.koin.core.module.Module.ScopeSet() {
                            @Override
                            public void invoke(ScopeDSL scopeDSL) {
                                try {
                                    scopeDSL.scoped(Simple.ComponentA.class, new Definition<Simple.ComponentA>() {
                                        @Override
                                        public Simple.ComponentA invoke(Scope scope, DefinitionParameters parameters) {
                                            return new Simple.ComponentA();
                                        }
                                    });
                                } catch (DefinitionOverrideException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                });

                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        Scope scopeA = koin.<A>createScope(A.class);
        Scope scopeB = koin.<B>createScope(B.class);
        Scope scopeC = koin.<C>createScope(C.class);

        scopeA.linkTo(scopeC);
        scopeB.linkTo(scopeC);

        Simple.ComponentB compb_scopeA = scopeA.<Simple.ComponentB>get(Simple.ComponentB.class);
        Simple.ComponentB compb_scopeB = scopeB.<Simple.ComponentB>get(Simple.ComponentB.class);

        Assert.assertNotEquals(compb_scopeA, compb_scopeB);
        Assert.assertEquals(compb_scopeA.a, compb_scopeB.a);

        stopKoin();
    }

    @Test
    public void error_for_root_linked_scope() throws ScopeAlreadyCreatedException, NoScopeDefFoundException, KoinAppAlreadyStartedException, DefinitionParameterException, NoBeanDefFoundException, ClosedScopeException {
        Koin koin = startKoin(new KoinAppDeclaration() {
            @Override
            public void invoke(KoinApplication koinApplication) {

                org.koin.core.module.Module module = Module.module(new ModuleDeclaration() {
                    @Override
                    public void invoke(org.koin.core.module.Module module) {
                        try {
                            module.single(A.class, new Definition<A>() {
                                @Override
                                public A invoke(Scope scope, DefinitionParameters parameters) {
                                    return new A();
                                }
                            });

                            module.scope(A.class, new org.koin.core.module.Module.ScopeSet() {
                                @Override
                                public void invoke(ScopeDSL scopeDSL) {
                                    try {
                                        scopeDSL.scoped(B.class, new Definition<B>() {
                                            @Override
                                            public B invoke(Scope scope, DefinitionParameters parameters) {
                                                return new B();
                                            }
                                        });
                                    } catch (DefinitionOverrideException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });

                        } catch (DefinitionOverrideException e) {
                            e.printStackTrace();
                        }
                    }
                });

                try {
                    koinApplication.modules(module);
                } catch (DefinitionOverrideException e) {
                    e.printStackTrace();
                }
            }
        }).getKoin();

        A a = koin.<A>get(A.class);

        try {
            koin.getScopeRegistry().getRootScope().linkTo(a.getScope());
            Assert.fail();
        } catch (Exception e) {
            e.printStackTrace();
        }
        stopKoin();
    }
}
