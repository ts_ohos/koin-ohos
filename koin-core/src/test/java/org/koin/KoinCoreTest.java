package org.koin;

import org.junit.After;

import static org.koin.core.context.DefaultContextExt.stopKoin;

public abstract class KoinCoreTest {

    @After
    public void after() {
        stopKoin();
    }
}
