package org.koin;

public class Simple {

    public interface ComponentInterface1 {
    }

    public interface ComponentInterface2 {
    }

    public static class ComponentA {

    }

    public static class ComponentB {

        public ComponentA a;

        public ComponentB(ComponentA a) {
            this.a = a;
        }
    }

    public static class ComponentC {
        public ComponentC(ComponentB b) {

        }
    }

    public static class Component1 implements ComponentInterface1, ComponentInterface2 {

    }

    public static class Component2 implements ComponentInterface1 {
    }

    public static class UserComponent {
        public UserComponent(ComponentInterface1 c1) {

        }
    }

    public static class MySingle {

        private int id;

        public MySingle(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }
    }

    public static class MySingleWithNull {

        public Integer id;

        public MySingleWithNull(Integer id) {
            this.id = id;
        }
    }

    public static class MyIntFactory {

        public Integer id;

        public MyIntFactory(Integer id) {
            this.id = id;
        }
    }

    public static class MyStringFactory {

        public String s;

        public MyStringFactory(String s) {
            this.s = s;
        }
    }

    public static class AllFactory {

        public MyIntFactory ints;
        public MyStringFactory strings;

        public AllFactory(MyIntFactory ints, MyStringFactory strings) {
            this.ints = ints;
            this.strings = strings;
        }
    }
}
