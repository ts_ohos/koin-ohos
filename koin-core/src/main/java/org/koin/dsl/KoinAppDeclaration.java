package org.koin.dsl;

import org.koin.core.KoinApplication;

public interface KoinAppDeclaration {
    void invoke(KoinApplication koinApplication);
}
