package org.koin.dsl;

import org.koin.core.error.NoScopeDefFoundException;
import org.koin.core.error.ScopeAlreadyCreatedException;

public class KoinApplication {

    /**
     * Create a KoinApplication instance and help configure it
     *
     * @author Arnaud Giuliani
     */
    public static org.koin.core.KoinApplication koinApplication(KoinAppDeclaration appDeclaration)
            throws ScopeAlreadyCreatedException,
            NoScopeDefFoundException {
        org.koin.core.KoinApplication koinApplication = org.koin.core.KoinApplication.init();
        if (appDeclaration != null) {
            appDeclaration.invoke(koinApplication);
        }
        return koinApplication;
    }

    public static org.koin.core.KoinApplication koinApplication()
            throws ScopeAlreadyCreatedException,
            NoScopeDefFoundException {
        return koinApplication(null);
    }
}
