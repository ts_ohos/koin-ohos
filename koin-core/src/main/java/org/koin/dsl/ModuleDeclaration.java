package org.koin.dsl;

import org.koin.core.module.Module;

public interface ModuleDeclaration {
    void invoke(Module module);
}
