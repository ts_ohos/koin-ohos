package org.koin.dsl;

import org.koin.core.definition.BeanDefinition;
import org.koin.core.definition.Callbacks;
import org.koin.core.definition.OnCloseCallback;
import java.util.List;

public class DefinitionBinding {

    /**
     * Add a compatible type to match for definition
     *
     * @param bean
     * @param clazz
     */
    public static <T> BeanDefinition<T> bind(BeanDefinition<T> bean, Class clazz) {
        bean.getSecondaryTypes().add(clazz);
        return bean;
    }

    /**
     * Add compatible types to match for definition
     *
     * @param bean
     * @param classes
     */
    public static BeanDefinition binds(BeanDefinition bean, List<Class> classes) {
        bean.getSecondaryTypes().addAll(classes);
        return bean;
    }

    /**
     * Callback when closing instance
     */
    public static <T> BeanDefinition<T> onClose(BeanDefinition<T> bean,
                                                OnCloseCallback<T> onClose) {
        bean.setCallbacks(new Callbacks<>(onClose));
        return bean;
    }
}
