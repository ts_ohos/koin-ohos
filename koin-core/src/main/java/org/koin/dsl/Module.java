package org.koin.dsl;

public class Module {

    /**
     * Define a Module
     *
     * @param createdAtStart
     * @param override
     * @author Arnaud Giuliani
     */
    public static org.koin.core.module.Module module(boolean createdAtStart,
                                                     boolean override,
                                                     ModuleDeclaration moduleDeclaration) {
        org.koin.core.module.Module module = new org
                .koin
                .core
                .module
                .Module(createdAtStart, override);
        if (moduleDeclaration != null) {
            moduleDeclaration.invoke(module);
        }
        return module;
    }

    /**
     * Define a Module
     *
     * @author Arnaud Giuliani
     */
    public static org.koin.core.module.Module module(ModuleDeclaration moduleDeclaration) {
        return module(false, false, moduleDeclaration);
    }

    /**
     * Define a Module
     *
     * @author Arnaud Giuliani
     */
    public static org.koin.core.module.Module module(boolean override,
                                                     ModuleDeclaration moduleDeclaration) {
        return module(false, override, moduleDeclaration);
    }
}
