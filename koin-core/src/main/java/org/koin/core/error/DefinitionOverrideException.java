package org.koin.core.error;

/**
 * Definition Override Error
 *
 * @author Arnaud Giuliani
 */
public class DefinitionOverrideException extends Exception {
    public DefinitionOverrideException(String msg) {
        super(msg);
    }
}
