package org.koin.core.error;

public class NoParameterFoundException extends Exception {

    public NoParameterFoundException(String message) {
        super(message);
    }
}
