package org.koin.core.error;

public class NoBeanDefFoundException extends Exception {

    public NoBeanDefFoundException(String message) {
        super(message);
    }
}
