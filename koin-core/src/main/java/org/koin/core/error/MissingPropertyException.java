package org.koin.core.error;

public class MissingPropertyException extends Exception {

    public MissingPropertyException(String msg) {
        super(msg);
    }
}
