package org.koin.core.error;

public class ClosedScopeException extends Exception {

    public ClosedScopeException(String message) {
        super(message);
    }
}
