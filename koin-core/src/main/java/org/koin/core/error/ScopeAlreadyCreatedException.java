package org.koin.core.error;

public class ScopeAlreadyCreatedException extends Exception {
    public ScopeAlreadyCreatedException(String message) {
        super(message);
    }
}
