package org.koin.core.error;

public class KoinAppAlreadyStartedException extends Exception {
    public KoinAppAlreadyStartedException(String message) {
        super(message);
    }
}
