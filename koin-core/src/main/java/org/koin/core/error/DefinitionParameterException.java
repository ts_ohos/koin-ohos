package org.koin.core.error;

public class DefinitionParameterException extends Exception {

    public DefinitionParameterException(String message) {
        super(message);
    }
}
