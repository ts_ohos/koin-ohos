package org.koin.core.error;

public class InstanceCreationException extends Exception {

    public InstanceCreationException(String message, Throwable cause) {
        super(message, cause);
    }
}
