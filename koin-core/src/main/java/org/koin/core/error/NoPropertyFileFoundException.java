package org.koin.core.error;

public class NoPropertyFileFoundException extends Exception {
    public NoPropertyFileFoundException(String message) {
        super(message);
    }
}
