package org.koin.core.error;

public class ScopeNotCreatedException extends Exception {
    public ScopeNotCreatedException(String message) {
        super(message);
    }
}
