package org.koin.core.error;

public class NoScopeDefFoundException extends Exception {
    public NoScopeDefFoundException(String message) {
        super(message);
    }
}
