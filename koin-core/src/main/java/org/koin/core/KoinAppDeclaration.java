package org.koin.core;

public interface KoinAppDeclaration {

    public void invoke(KoinApplication koinApplication);
}
