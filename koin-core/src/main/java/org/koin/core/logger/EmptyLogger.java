package org.koin.core.logger;


public class EmptyLogger extends Logger {

    public EmptyLogger() {
        super(Level.NONE);
    }

    @Override
    public void log(Level level, String msg) {

    }
}
