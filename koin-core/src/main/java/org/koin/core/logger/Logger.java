package org.koin.core.logger;

public abstract class Logger {

    public static final String KOIN_TAG = "[Koin]";

    private Level level;

    public Logger() {
        this.level = Level.INFO;
    }

    public Logger(Level level) {
        this.level = level;
    }

    public Level getLevel() {
        return level;
    }

    public abstract void log(Level level, String msg);

    private boolean canLog(Level level) {
        return this.level.ordinal() <= level.ordinal();
    }

    private void doLog(Level level, String msg) {
        if (canLog(level)) {
            log(level, msg);
        }
    }

    public void debug(String msg) {
        doLog(Level.DEBUG, msg);
    }

    public void info(String msg) {
        doLog(Level.INFO, msg);
    }

    public void error(String msg) {
        doLog(Level.ERROR, msg);
    }

    public boolean isAt(Level lvl) {
        return this.level.ordinal() <= lvl.ordinal();
    }
}
