package org.koin.core.logger;

import java.io.PrintStream;

public class PrintLogger extends Logger {

    private Level level;

    public PrintLogger() {
        super(Level.INFO);
        this.level = Level.INFO;
    }

    public PrintLogger(Level level) {
        super(level);
        this.level = level;
    }

    @Override
    public void log(Level level, String msg) {
        if (this.level.ordinal() <= level.ordinal()) {
            PrintStream printer = level.compareTo(Level.ERROR) >= 0 ? System.err : System.out;
            printer.println("" + '[' + level + "] [Koin] " + msg);
        }
    }
}
