package org.koin.core.logger;

public enum Level {

    DEBUG, INFO, ERROR, NONE

}
