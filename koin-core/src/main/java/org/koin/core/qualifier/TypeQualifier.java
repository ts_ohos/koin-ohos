package org.koin.core.qualifier;

import org.koin.ext.ClassUtil;

public class TypeQualifier extends Qualifier {

    public TypeQualifier(Class type) {
        this.value = ClassUtil.getFullName(type);
    }

    @Override
    public String toString() {
        return "q:'" + value + "'";
    }
}
