package org.koin.core.qualifier;

public class StringQualifier extends Qualifier {

    public StringQualifier(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
