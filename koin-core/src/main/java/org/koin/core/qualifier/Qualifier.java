package org.koin.core.qualifier;

public class Qualifier {

    public String value = "";

    public static StringQualifier named(String name) {
        return new StringQualifier(name);
    }

    public static <E extends Enum<E>> Qualifier named(Enum<E> eEnum) {
        return qualifier(eEnum);
    }

    public static StringQualifier qualifier(String name) {
        return new StringQualifier(name);
    }

    public static StringQualifier _q(String name) {
        return new StringQualifier(name);
    }

    public static <T> TypeQualifier named(Class<T> claz) {
        return new TypeQualifier(claz);
    }

    public static <T> TypeQualifier qualifier(Class<T> claz) {
        return new TypeQualifier(claz);
    }

    public static <T> TypeQualifier _q(Class<T> claz) {
        return new TypeQualifier(claz);
    }

    public static <E extends Enum<E>> StringQualifier qualifier(Enum<E> eEnum) {
        return new StringQualifier(eEnum.toString().toLowerCase());
    }
}
