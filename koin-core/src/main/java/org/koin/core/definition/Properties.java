package org.koin.core.definition;

import org.koin.core.error.MissingPropertyException;
import org.koin.mp.KoinPlatformTools;
import java.util.Map;

public class Properties {

    public Map<String, Object> data;

    public Properties() {
        this.data = KoinPlatformTools.INSTANCE().safeHashMap();
    }

    public Properties(Map<String, Object> data) {
        this.data = data;
    }

    public <T> void set(String key, T value) {
        data.put(key, value);
    }

    public <T> T get(String key) throws MissingPropertyException {
        T result = (T) data.get(key);
        if (result == null) {
            throw new MissingPropertyException("missing property for '" + key + "'");
        }
        return result;
    }

    public <T> T getOrNull(String key) {
        return (T) data.get(key);
    }
}
