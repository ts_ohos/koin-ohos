package org.koin.core.definition;

public class Options {

    private boolean isCreatedAtStart = false;
    private boolean override = false;
    private boolean isExtraDefinition = false;

    public boolean isCreatedAtStart() {
        return isCreatedAtStart;
    }

    public Options setCreatedAtStart(boolean createdAtStart) {
        isCreatedAtStart = createdAtStart;
        return this;
    }

    public boolean isOverride() {
        return override;
    }

    public Options setOverride(boolean override) {
        this.override = override;
        return this;
    }

    public boolean isExtraDefinition() {
        return isExtraDefinition;
    }

    public Options setExtraDefinition(boolean extraDefinition) {
        isExtraDefinition = extraDefinition;
        return this;
    }
}
