package org.koin.core.definition;

public interface OnCloseCallback<T> {
    void invoke(T t);
}
