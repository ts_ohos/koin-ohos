package org.koin.core.definition;

public class Callbacks<T> {

    private OnCloseCallback<T> onClose;

    public Callbacks() {
        this.onClose = null;
    }

    public Callbacks(OnCloseCallback<T> onClose) {
        this.onClose = onClose;
    }

    public OnCloseCallback<T> getOnClose() {
        return onClose;
    }

    public void setOnClose(OnCloseCallback<T> onClose) {
        this.onClose = onClose;
    }
}
