package org.koin.core.definition;

import org.koin.core.qualifier.Qualifier;
import java.util.List;

public class Definitions {

    public static volatile Definitions INSTANCE;

    private Definitions() {

    }

    public static Definitions INSTANCE() {
        if (INSTANCE == null) {
            synchronized (Definitions.class) {
                if (INSTANCE == null) {
                    INSTANCE = new Definitions();
                }
            }
        }
        return INSTANCE;
    }

    public <T> BeanDefinition<T> createTSingle(
            Class<T> clazz,
            Qualifier qualifier,
            Definition<T> definition,
            Options options,
            List<Class> secondaryTypes,
            Qualifier scopeQualifier
    ) {

        return new BeanDefinition<>()
                .setScopeQualifier(scopeQualifier)
                .setPrimaryType(clazz)
                .setQualifier(qualifier)
                .setDefinition(definition)
                .setKind(Kind.Single)
                .setOptions(options)
                .setSecondaryTypes(secondaryTypes);
    }

    public BeanDefinition createSingle(
            Class clazz,
            Qualifier qualifier,
            Definition definition,
            Options options,
            List<Class> secondaryTypes,
            Qualifier scopeQualifier
    ) {

        return new BeanDefinition<>()
                .setScopeQualifier(scopeQualifier)
                .setPrimaryType(clazz)
                .setQualifier(qualifier)
                .setDefinition(definition)
                .setKind(Kind.Single)
                .setOptions(options)
                .setSecondaryTypes(secondaryTypes);
    }

    public <T> BeanDefinition createFactory(
            Class<T> clazz,
            Qualifier qualifier,
            Definition<T> definition,
            Options options,
            List<Class> secondaryTypes,
            Qualifier scopeQualifier
    ) {
        return new BeanDefinition()
                .setScopeQualifier(scopeQualifier)
                .setPrimaryType(clazz)
                .setQualifier(qualifier)
                .setDefinition(definition)
                .setKind(Kind.Factory)
                .setOptions(options)
                .setSecondaryTypes(secondaryTypes);
    }
}
