package org.koin.core.definition;

import org.koin.core.parameter.DefinitionParameters;
import org.koin.core.scope.Scope;

public interface Definition<T> {

    T invoke(Scope scope, DefinitionParameters parameters);

}
