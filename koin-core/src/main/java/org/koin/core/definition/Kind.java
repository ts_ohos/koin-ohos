package org.koin.core.definition;

public enum Kind {
    Single, Factory
}
