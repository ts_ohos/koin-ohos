package org.koin.core;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target(
        {
                ElementType.TYPE,
                ElementType.METHOD,
                ElementType.FIELD,
                ElementType.PARAMETER
        }
)
public @interface KoinExperimentalAPI {
}
