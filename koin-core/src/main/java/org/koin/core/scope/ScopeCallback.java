package org.koin.core.scope;

/**
 * Scope Callback
 */
public interface ScopeCallback {

    void onScopeClose(Scope scope);

}
