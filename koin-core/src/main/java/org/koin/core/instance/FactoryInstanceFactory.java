package org.koin.core.instance;

import org.koin.core.Koin;
import org.koin.core.definition.BeanDefinition;
import org.koin.core.definition.OnCloseCallback;

public class FactoryInstanceFactory<T> extends InstanceFactory<T> {

    public FactoryInstanceFactory(Koin koin, BeanDefinition<T> beanDefinition) {
        super(koin, beanDefinition);
    }

    @Override
    public T get(InstanceContext context) {
        return create(context);
    }

    @Override
    public boolean isCreated() {
        return false;
    }

    @Override
    public void drop() {
        OnCloseCallback<T> onClose = getBeanDefinition().getCallbacks().getOnClose();
        if (onClose != null) {
            onClose.invoke(null);
        }
    }
}
