package org.koin.core.instance;

import org.koin.core.Koin;
import org.koin.core.parameter.DefinitionParameters;
import org.koin.core.parameter.ParametersDefinition;
import org.koin.core.scope.Scope;

public class InstanceContext {

    private Koin koin;
    private Scope scope;
    private DefinitionParameters parameters;

    public InstanceContext(Koin koin, Scope scope, ParametersDefinition _parameters) {
        this.koin = koin;
        this.scope = scope;
        if (_parameters != null) {
            parameters = _parameters.invoke();
        } else {
            parameters = new DefinitionParameters();
        }
    }

    public InstanceContext(Koin koin, Scope scope) {
        this.koin = koin;
        this.scope = scope;
        parameters = new DefinitionParameters();
    }

    public Koin getKoin() {
        return koin;
    }

    public void setKoin(Koin koin) {
        this.koin = koin;
    }

    public Scope getScope() {
        return scope;
    }

    public void setScope(Scope scope) {
        this.scope = scope;
    }

    public DefinitionParameters getParameters() {
        return parameters;
    }

    public void setParameters(DefinitionParameters parameters) {
        this.parameters = parameters;
    }
}
