package org.koin.core.parameter;

public interface ParametersDefinition {

    DefinitionParameters invoke();
}
