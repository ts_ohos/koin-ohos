package org.koin.core.registry;

import org.koin.core.Koin;
import org.koin.core.logger.Level;
import org.koin.mp.KoinPlatformTools;
import java.util.Map;

/**
 * Property Registry
 * Save/find all Koin properties
 *
 * @author Arnaud Giuliani
 */
public class PropertyRegistry {

    private Map<String, Object> _values = KoinPlatformTools.INSTANCE().safeHashMap();
    private Koin _koin;

    public PropertyRegistry(Koin _koin) {
        this._koin = _koin;
    }

    /**
     * saveProperty all properties to registry
     *
     * @param properties
     */
    public void saveProperties(Map<String, Object> properties) {
        if (_koin.getLogger().isAt(Level.DEBUG)) {
            _koin.getLogger().debug("load " + properties.size() + " properties");
        }
        _values.putAll(properties);
    }

    /**
     * save a property (key,value)
     */
    public <T> void saveProperty(String key, T value) {
        _values.put(key, value);
    }

    /**
     * Delete a property (key,value)
     */
    public void deleteProperty(String key) {
        _values.remove(key);
    }

    /**
     * Get a property
     *
     * @param key
     */
    public <T> T getProperty(String key) {
        return (T) _values.get(key);
    }

    public void close() {
        _values.clear();
    }

    public Koin getKoin() {
        return _koin;
    }

    @Override
    public String toString() {
        return _values.toString();
    }
}
