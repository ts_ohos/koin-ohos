package org.koin.core;

import org.koin.core.error.NoScopeDefFoundException;
import org.koin.core.error.ScopeAlreadyCreatedException;

public class KoinAppDeclarationExt {

    public static KoinApplication koinApplication(KoinAppDeclaration appDeclaration)
            throws ScopeAlreadyCreatedException,
            NoScopeDefFoundException {
        KoinApplication koinApplication = KoinApplication.init();
        if (appDeclaration != null) {
            appDeclaration.invoke(koinApplication);
        }
        return koinApplication;
    }
}
