package org.koin.core.context;

import org.koin.core.KoinApplication;
import org.koin.core.error.KoinAppAlreadyStartedException;
import org.koin.core.error.NoScopeDefFoundException;
import org.koin.core.error.ScopeAlreadyCreatedException;
import org.koin.core.module.Module;
import org.koin.dsl.KoinAppDeclaration;
import org.koin.mp.KoinPlatformTools;
import java.util.List;

public class DefaultContextExt {

    /**
     * Start a Koin Application as StandAlone
     */
    public static KoinApplication startKoin(KoinApplication koinApplication)
            throws KoinAppAlreadyStartedException {
        return KoinPlatformTools.INSTANCE().defaultContext().startKoin(koinApplication);
    }

    /**
     * Start a Koin Application as StandAlone
     */
    public static KoinApplication startKoin(KoinAppDeclaration appDeclaration)
            throws NoScopeDefFoundException,
            ScopeAlreadyCreatedException,
            KoinAppAlreadyStartedException {
        return KoinPlatformTools.INSTANCE().defaultContext().startKoin(appDeclaration);
    }

    /**
     * Stop current StandAlone Koin application
     */
    public static void stopKoin() {
        KoinPlatformTools.INSTANCE().defaultContext().stopKoin();
    }

    /**
     * load Koin module in global Koin context
     */
    public static void loadKoinModules(Module module) {
        KoinPlatformTools.INSTANCE().defaultContext().loadKoinModules(module);
    }

    /**
     * load Koin modules in global Koin context
     */
    public static void loadKoinModules(List<Module> modules) {
        KoinPlatformTools.INSTANCE().defaultContext().loadKoinModules(modules);
    }

    /**
     * unload Koin modules from global Koin context
     */
    public static void unloadKoinModules(Module module) {
        KoinPlatformTools.INSTANCE().defaultContext().unloadKoinModules(module);
    }

    /**
     * unload Koin modules from global Koin context
     */
    public static void unloadKoinModules(List<Module> modules) {
        KoinPlatformTools.INSTANCE().defaultContext().unloadKoinModules(modules);
    }
}
