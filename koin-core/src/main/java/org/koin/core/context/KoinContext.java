package org.koin.core.context;

import org.koin.core.Koin;
import org.koin.core.KoinApplication;
import org.koin.core.error.KoinAppAlreadyStartedException;
import org.koin.core.error.NoScopeDefFoundException;
import org.koin.core.error.ScopeAlreadyCreatedException;
import org.koin.core.module.Module;
import org.koin.dsl.KoinAppDeclaration;
import java.util.List;

/**
 * Hold Current Koin context
 */
public interface KoinContext {

    /**
     * Get Koin instance
     */
    Koin get();

    /**
     * Get Koin instance or null
     */
    Koin getOrNull();

    /**
     * Stop current Koin instance
     */
    void stopKoin();

    /**
     * Start a Koin Application as StandAlone
     */
    KoinApplication startKoin(KoinApplication koinApplication)
            throws KoinAppAlreadyStartedException;

    /**
     * Start a Koin Application as StandAlone
     */
    KoinApplication startKoin(KoinAppDeclaration appDeclaration)
            throws ScopeAlreadyCreatedException,
            NoScopeDefFoundException,
            KoinAppAlreadyStartedException;

    /**
     * load Koin module in global Koin context
     */
    void loadKoinModules(Module module);

    /**
     * load Koin modules in global Koin context
     */
    void loadKoinModules(List<Module> modules);

    /**
     * unload Koin module from global Koin context
     */
    void unloadKoinModules(Module module);

    /**
     * unload Koin modules from global Koin context
     */
    void unloadKoinModules(List<Module> modules);
}
