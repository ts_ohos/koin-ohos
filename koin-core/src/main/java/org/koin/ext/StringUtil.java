package org.koin.ext;

public class StringUtil {

    public static String clearQuotes(String str) {
        char[] chars = str.toCharArray();
        char quoteChar = '"';
        if (chars.length > 0 && chars[0] == quoteChar && chars[chars.length - 1] == quoteChar) {
            StringBuilder sb = new StringBuilder();
            for (int i = 1; i < chars.length - 1; i++) {
                sb.append(chars[i]);
            }
            return sb.toString();
        } else {
            return str;
        }
    }

}
