package org.koin.ext;

import org.koin.mp.KoinPlatformTools;
import java.util.Map;

public class ClassUtil {

    private static Map<Class, String> classNames = KoinPlatformTools.INSTANCE().safeHashMap();

    public static String getFullName(Class claz) {
        String name = classNames.get(claz);
        if (name == null) {
            name = saveCache(claz);
        }
        return name;
    }

    public static String saveCache(Class claz) {
        String name = KoinPlatformTools.INSTANCE().getClassName(claz);
        classNames.put(claz, name);
        return name;
    }

}
