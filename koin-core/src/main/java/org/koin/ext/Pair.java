package org.koin.ext;

import java.io.Serializable;

public class Pair<A, B> implements Serializable {

    private A first;
    private B second;

    public Pair(A a, B b) {
        this.first = a;
        this.second = b;
    }

    public A getFirst() {
        return first;
    }

    public B getSecond() {
        return second;
    }

    @Override
    public String toString() {
        return "Pair{"
                + "first=" + first
                + ", second=" + second
                + '}';
    }
}
