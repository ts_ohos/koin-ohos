package org.koin.mp;

import org.koin.core.context.GlobalContext;
import org.koin.core.context.KoinContext;
import org.koin.core.instance.InstanceFactory;
import org.koin.core.logger.Level;
import org.koin.core.logger.Logger;
import org.koin.core.logger.PrintLogger;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class KoinPlatformTools {

    private static volatile KoinPlatformTools INSTANCE;

    private KoinPlatformTools() {

    }

    public static KoinPlatformTools INSTANCE() {
        if (INSTANCE == null) {
            synchronized (KoinPlatformTools.class) {
                if (INSTANCE == null) {
                    INSTANCE = new KoinPlatformTools();
                }
            }
        }
        return INSTANCE;
    }

    public String getStackTrace(Exception e) {
        StringBuilder sb = new StringBuilder();
        sb.append(e.toString());
        sb.append(InstanceFactory.ERROR_SEPARATOR);
        StackTraceElement[] stackTrace = e.getStackTrace();
        for (StackTraceElement element : stackTrace) {
            if (element.getClassName().contains("sun.reflect")) {
                sb.append(element.toString());
                sb.append(InstanceFactory.ERROR_SEPARATOR);
            } else {
                break;
            }
        }
        return sb.toString();
    }

    public String getClassName(Class claz) {
        return claz.getName();
    }

    public String generateId() {
        return UUID.randomUUID().toString();
    }

    public Logger defaultLogger(Level level) {
        return new PrintLogger(level);
    }

    public KoinContext defaultContext() {
        return GlobalContext.INSTANCE();
    }

    public <K, V> ConcurrentHashMap<K, V> safeHashMap() {
        return new ConcurrentHashMap<K, V>();
    }
}
