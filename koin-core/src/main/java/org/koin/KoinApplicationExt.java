package org.koin;

import org.koin.core.Koin;
import org.koin.core.KoinApplication;
import org.koin.core.registry.PropertyRegistryExt;

public class KoinApplicationExt {

    /**
     * Load properties from file
     *
     * @param fileName
     */
    public static KoinApplication fileProperties(KoinApplication app, String fileName) {
        Koin koin = app.getKoin();
        PropertyRegistryExt.loadPropertiesFromFile(koin.getPropertyRegistry(), fileName);
        return app;
    }

    /**
     * Load properties from environment
     */
    public static KoinApplication environmentProperties(KoinApplication app) {
        Koin koin = app.getKoin();
        PropertyRegistryExt.loadEnvironmentProperties(koin.getPropertyRegistry());
        return app;
    }
}
